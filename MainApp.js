// Inbuilt
import 'react-native-gesture-handler'
import * as React from 'react'
import {
  Image,
  Platform,
  Text,
  TouchableOpacity,
  DeviceEventEmitter,
  View,
} from 'react-native'

// 3rd Party
import SplashScreen from 'react-native-splash-screen'
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets'
import { isiPhoneX } from './Source/Utility/Utility'
import { createStackNavigator } from '@react-navigation/stack'
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer, useLinkBuilder } from '@react-navigation/native'
// import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'

import { MyColors } from './Source/Theme'
import images_path from '@Images/Images';
import Global from './Source/Utility/Global'
import strings from './Source/Localization/string';


// Auth
import MobileScreen from "./Source/Screens/Auth/MobileScreen";
import OtpScreen from "./Source/Screens/Auth/OtpScreen";
import CompleteProfileScreen from "./Source/Screens/Auth/CompleteProfileScreen";


const StackMain = createStackNavigator()


class MainApp extends React.Component {

  constructor() {
    super()
    console.disableYellowBox = true;
  }

  state = {
    isLoading: true,
    token: "",
    user: {},
  };

  componentDidMount() {
    SplashScreen.hide()

    Global.getCurrentToken().then(token => {
      this.setState({ token: token, isLoading: false, })
    }).catch(err => {
      this.setState({ token: '', isLoading: false, })
    })

    Global.getCurrentUserPromise().then(user => {
      this.setState({ user: user })
    })
  }

  render() {
    return (
      this.state.isLoading ? <View style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: MyColors.themeColor
      }}>
        <Image source={images_path.oms_money} />
      </View> : (
        <View style={{ flex: 1, }}>
          <NavigationContainer>
            {this.state.token == '' ? this.renderLoginStack() : this.renderLoginStack()}
          </NavigationContainer>
        </View>
      )
    )
  }

  renderLoginStack = () => {
    // console.log('renderUserStack is being called .... >>>>>> ')
    return (
      <StackMain.Navigator
        screenOptions={MainApp.customizeNavigationBarWithGrayColor()}
        initialRouteName='MobileScreen'
      >
        <StackMain.Screen
          name='MobileScreen'
          component={MobileScreen}
        />
        <StackMain.Screen
          name='OtpScreen'
          component={OtpScreen}
        />
        <StackMain.Screen
          name='CompleteProfileScreen'
          component={CompleteProfileScreen}
        />
      </StackMain.Navigator>
    )
  }

  // Navigation bar customization
  static customizeNavigationBarWithGrayColor(title) {
    return MainApp.customizeNavigationBarWithHeaderStyle(
      MainApp.setNavigationBarBackgroundColorLightGray(),
      title == null ? "" : title
    )
  }

  static customizeNavigationBarWithWhiteColor(title) {
    return MainApp.customizeNavigationBarWithHeaderStyle(
      MainApp.setNavigationBarBackgroundColorWhite(),
      title == null ? "" : title
    )
  }

  static customizeNavigationBarWithHeaderStyle(headerStyle, navTitle) {
    return {
      title: navTitle,
      headerTintColor: MyColors.blackColor,
      headerBackTitleVisible: false,
      headerBackImage: MyCustomHeaderBackImage,
      headerTitleStyle: {
        fontSize: 17,
        fontWeight: null,
        letterSpacing: 0.3
      },
      headerTitleAlign: 'center',
      headerStyle: {
        backgroundColor: MyColors.whiteColor,
        shadowColor: 'transparent',
        elevation: 0,
        height:
          Platform.OS === 'ios'
            ? StaticSafeAreaInsets.safeAreaInsetsTop + 44
            : 44
      }
    }
  }
  static setNavigationBarBackgroundColorLightGray() {
    return this.setNavigationBarBackgroundColor(MyColors.whiteColor)
  }
  static setNavigationBarBackgroundColorWhite() {
    return this.setNavigationBarBackgroundColor(MyColors.whiteColor)
  }
  static setNavigationBarBackgroundColor(color) {
    return {
      backgroundColor: color,
      shadowColor: 'transparent',
      elevation: 0,
      height:
        Platform.OS === 'ios'
          ? StaticSafeAreaInsets.safeAreaInsetsTop + 44
          : 44
    }
  }
}

export default MainApp

const MyCustomHeaderBackImage = () => (
  <Image
    source={images_path.back}
    style={{
      resizeMode: 'center',
      width: 40,
      height: 40
    }}
  />
)

