import React, { Component } from 'react'
import { SafeAreaView, ActivityIndicator } from 'react-native'
import { WebView } from 'react-native-webview'

import { MyColors } from '../../Theme'
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class WebViewScreen extends Component {
  state = {
    url: '',
  }

  componentDidMount () {
    if (this.props.navigation.state.params != null) {
      this.setState({
        url: this.props.navigation.state.params.url
      })
    }
  }

  render () {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: MyColors.whiteColor }}>
        <CustomStatusBarTheme />
        <WebView
          style={{ flex: 1, backgroundColor: MyColors.whiteColor }}
          source={{ uri: this.state.url }}
          renderLoading={this.renderProgressIndicator}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          startInLoadingState={true}
          onNavigationStateChange={this.onNavigationStateChange}
          // onMessage={this.onMessage}
        />
      </SafeAreaView>
    )
  }

  renderProgressIndicator = () => {
    return (
      <ActivityIndicator size='large' color={MyColors.themeColor} 
      style={{ 
        position: 'absolute',
        alignSelf: 'center',
        top: 0,
        bottom: 0 }} />
    )
  }

  onNavigationStateChange = navState => {
    console.log('onNavigationStateChange--->>>', JSON.stringify(navState))
    // if (
    //   navState.url != 'about:blank' &&
    //   navState.url != this.state.url
    // ) {
    // }
  }

  onMessage = e => {
    let { data } = e.nativeEvent // data you will receive from html
    console.log('onMessage--->>>', e)
  }

}
