import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  SafeAreaView,
  DeviceEventEmitter,
  Text,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { RadioButton } from 'react-native-paper';

import { MyColors } from '../../Theme';
import images_path from '@Images/Images';
import Global from '../../Utility/Global';
import strings from '../../Localization/string';
import { postMethodAPI, putMethodAPI } from '../../API/APIClient';
import { showMessageAlert, isEmpty } from '../../Utility/Utility';

import TitleTxtFld from '../../customComponents/TitleTxtFld'
import CustomButton from '../../customComponents/CustomButton';
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class AddPosScreen extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    token: '',
    user: {},
    merchant: {},
    posObj: {},

    isLoading: false,

    manufacturer: '',
    model: '',
    version: '',
    branch: '',
    selectedBranch: {},
    api_support: '',
    description: '',
  };

  componentDidMount() {
    if (this.props.navigation.state != null &&
      this.props.navigation.state.params != undefined &&
      this.props.navigation.state.params.posObj) {
      console.log('posObj', this.props.navigation.state.params.posObj)
      this.setState({
        posObj: this.props.navigation.state.params.posObj,

        manufacturer: this.props.navigation.state.params.posObj.PosManufacturer,
        model: this.props.navigation.state.params.posObj.PosModel,
        version: this.props.navigation.state.params.posObj.PosVersion,
        branch: this.props.navigation.state.params.posObj.BranchName,
        api_support: this.props.navigation.state.params.posObj.PosApiSupport,
        description: this.props.navigation.state.params.posObj.PosDescription,

        selectedBranch: {
          id: this.props.navigation.state.params.posObj.BranchId,
          storeName: this.props.navigation.state.params.posObj.BranchName
        }
      })
    }

    Global.getCurrentToken().then(token => {
      Global.getCurrentUserPromise().then(user => {
        Global.getCurrentMerchantPromise().then(merchant => {
          this.setState({
            token: token,
            user: user,
            merchant: merchant,
          }, () => {
            console.log('user----------->>>>>>>', this.state.user)
            console.log('merchant------->>>>>>>', this.state.merchant)
          })
        })
      })
    })
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomStatusBarTheme />

        <Image
          source={images_path.top_bg}
          style={{ width: '100%', position: 'absolute' }} />

        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} >
          {/* <View style={{ flex: 1, }}> */}

          {/* {this.renderProfile()} */}

          {this.renderHeader()}

          <View style={Styles.container}>

            {this.renderManufacturerField()}
            {this.renderModelField()}
            {this.renderVersionField()}
            {this.renderBranchField()}
            {this.renderApiSupport()}
            {this.renderDescriptionField()}

            {this.renderButton()}

          </View>
          {/* </View> */}
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }

  renderHeader() {
    return (
      <View style={Styles.cellHeader}>
        <Text style={Styles.txtHeader}>{strings.request_pos_activation}</Text>
        <TouchableOpacity
          onPress={() => { this.props.navigation.toggleDrawer() }}
          style={{ position: 'absolute', right: 0 }}>
          <Image source={images_path.menu} />
        </TouchableOpacity>
      </View>
    );
  }

  renderManufacturerField() {
    return (
      <TitleTxtFld
        value={this.state.manufacturer}
        title={strings.manufacturer}
        placeholder={strings.enter_manufacturer}
        onChangeTxt={txt => this.setState({ manufacturer: txt })}
      />
    );
  }

  renderModelField() {
    return (
      <TitleTxtFld
        value={this.state.model}
        title={strings.model}
        placeholder={strings.enter_model}
        onChangeTxt={txt => this.setState({ model: txt })}
      />
    );
  }

  renderVersionField() {
    return (
      <TitleTxtFld
        value={this.state.version}
        title={strings.version}
        placeholder={strings.enter_version}
        onChangeTxt={txt => this.setState({ version: txt })}
      />
    );
  }

  renderApiSupport() {
    return (
      <View>
        <Text style={Styles.apiSupportContainer}>{strings.api_support}</Text>
        <RadioButton.Group
          onValueChange={newValue => this.setState({ api_support: newValue })}
          value={this.state.api_support}
        >
          <View style={{ flexDirection: 'row', marginTop: 5 }}>
            <TouchableOpacity style={[Styles.apiSupportSubContainer, { marginEnd: 10, }]}
              onPress={() => { this.setState({ api_support: strings.json }) }}>
              <RadioButton value={strings.json} color={MyColors.themeColor} uncheckedColor={MyColors.vwBorder} />
              <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14 }}>{strings.json}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={Styles.apiSupportSubContainer}
              onPress={() => { this.setState({ api_support: strings.xml }) }}>
              <RadioButton value={strings.xml} color={MyColors.themeColor} uncheckedColor={MyColors.vwBorder} />
              <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14 }}>{strings.xml}</Text>
            </TouchableOpacity>
          </View>
        </RadioButton.Group>
      </View >
    )
  }

  renderBranchField() {
    return (
      <TitleTxtFld
        value={this.state.branch}
        title={strings.select_branch}
        placeholder={strings.select_branch}
        onChangeTxt={txt => this.setState({ branch: txt })}
        isSelectable
        rightImageSource={images_path.arrow_down}
        onPress={() => this.onPressSelectBranch()}
      />
    );
  }

  renderDescriptionField() {
    return (
      <TitleTxtFld
        value={this.state.description}
        title={strings.description_of_requirments}
        placeholder={strings.enter_description_of_requirments}
        onChangeTxt={txt => this.setState({ description: txt })}
      />
    );
  }

  renderButton() {
    return (
      <CustomButton
        title={strings.request}
        isLoading={this.state.isLoading}
        buttonCustomStyle={{ marginTop: 50, paddingHorizontal: 20 }}
        onPress={() => this.tappedOnButton()}
      />
    );
  }

  onPressSelectBranch = () => {
    this.props.navigation.navigate("SelectBranchListScreen", {
      returnData: this.refreshSelectedBranch.bind(this)
    })
  }

  refreshSelectedBranch(selectedBranch) {
    console.log("selectedBranch is >>> ", selectedBranch);
    this.setState({
      selectedBranch: selectedBranch,
      branch: selectedBranch.storeName
    })
  }

  tappedOnButton() {
    if (this.state.isLoading) {
      return;
    }

    if (isEmpty(this.state.manufacturer)) {
      showMessageAlert(strings.please_enter_manufacturer)
      return
    } else if (isEmpty(this.state.model)) {
      showMessageAlert(strings.please_enter_model)
      return
    } else if (isEmpty(this.state.version)) {
      showMessageAlert(strings.please_enter_version)
      return
    } else if (isEmpty(this.state.branch)) {
      showMessageAlert(strings.please_select_branch)
      return
    } else if (isEmpty(this.state.api_support)) {
      showMessageAlert(strings.please_select_api_support)
      return
    }

    this.setState({ isLoading: true })

    var params = JSON.stringify({
      posStatusId: '1',
      merchantId: this.state.merchant.id,
      posManufacturer: this.state.manufacturer,
      posModel: this.state.model,
      posVersion: this.state.version,
      merchantBranchId: this.state.selectedBranch.id,
      apiSupport: this.state.api_support,
      description: this.state.description,
    });

    if (this.state.posObj.PosRequestId != undefined) {
      const apifetcherObj = putMethodAPI('posRequest/updatePOSRequest/' + this.state.posObj.PosRequestId, params, this.state.token)
      apifetcherObj
        .then(response => { return Promise.all([response.status, response.json()]) })
        .then(res => {
          let statusCode = res[0]
          let data = res[1]
          console.log("Response ---->>>>\n ", data);

          this.setState({ isLoading: false })

          if (statusCode == 200) {
            this.props.navigation.goBack()
            DeviceEventEmitter.emit('AddEditPOS')
          }

          if (data.error != undefined) {
            showMessageAlert(data.error)
          }
        })
        .catch(error => {
          console.log("Error ---->>>> \n", error);
          this.setState({ isLoading: false });
          showMessageAlert(error)
        });
    } else {
      const apifetcherObj = postMethodAPI('posRequest/createPOSRequest', params, this.state.token);
      apifetcherObj
        .then(response => { return Promise.all([response.status, response.json()]) })
        .then(res => {
          let statusCode = res[0]
          let data = res[1]
          console.log("Response ---->>>>\n ", data);

          this.setState({ isLoading: false })

          if (statusCode == 201) {
            this.props.navigation.goBack()
            DeviceEventEmitter.emit('AddEditPOS')
          }

          if (data.error != undefined) {
            showMessageAlert(data.error)
          }
        })
        .catch(error => {
          console.log("Error ---->>>> \n", error);
          this.setState({ isLoading: false });
          showMessageAlert(error)
        });
    }
    return;
  }

}

const Styles = StyleSheet.create({
  container: {
    padding: 25,
    backgroundColor: MyColors.whiteColor,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25
  },

  // ---------------------------Top
  cellHeader: {
    margin: 25,
  },

  txtHeader: {
    fontFamily: 'Sansation-Bold',
    fontSize: 26,
    marginTop: 80,
    color: MyColors.whiteColor,
  },

  apiSupportContainer: {
    fontFamily: 'Roboto-Regular',
    fontSize: 14,
    color: '#5A607B',
    marginTop: 20,
  },
  apiSupportSubContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginStart: 10,
    justifyContent: 'center',
    backgroundColor: '#F1F1F4',
    borderRadius: 10,
    padding: 10,
  }


});
