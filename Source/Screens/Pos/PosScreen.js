import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  Text,
  FlatList,
  ActivityIndicator,
  DeviceEventEmitter,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import moment from "moment";

import { MyColors } from '../../Theme';
import images_path from '@Images/Images';
import Global from '../../Utility/Global';
import strings from '../../Localization/string';
import { getMethodAPI, } from '../../API/APIClient';
import { showMessageAlert, isEmpty } from '../../Utility/Utility';
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class PosScreen extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    isLoading: false,
    token: '',
    user: {},
    merchant: {},

    list: [],
  };

  componentDidMount() {
    Global.getCurrentToken().then(token => {
      Global.getCurrentUserPromise().then(user => {
        Global.getCurrentMerchantPromise().then(merchant => {
          this.setState({
            token: token,
            user: user,
            merchant: merchant,
          }, () => {
            console.log('user----------->>>>>>>', this.state.user)
            console.log('merchant------->>>>>>>', this.state.merchant)
            this.callPOSList()
          })
        })
      })
    })

    DeviceEventEmitter.addListener('AddEditPOS', () => {
      this.callPOSList()
    })
  }

  callPOSList = () => {
    this.setState({ isLoading: true })

    const apifetcherObj = getMethodAPI(
      'posRequest/getAllPOSRequests?merchantId=' + this.state.merchant.id, null, this.state.token
    )
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log('Success in API is >>> ', data)

        this.setState({ isLoading: false })

        if (statusCode == 200 && data.records != undefined) {
          this.setState({ list: data.records })
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log('Error in API is >>> ', error)
        this.setState({ isLoading: false })
      })
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: MyColors.whiteColor, }}>
          <CustomStatusBarTheme />

          <Image source={images_path.top_bg}
            style={{ width: '100%', position: 'absolute' }} />

          <ScrollView contentContainerStyle={{ flexGrow: 1, }} showsVerticalScrollIndicator={false} >
            <View style={{ flex: 1, }}>

              {this.renderHeader()}

              <View style={Styles.container}>

                {this.state.isLoading ? (
                  <ActivityIndicator
                    style={{ alignSelf: 'center' }}
                    color={MyColors.themeColor}
                    size='large'
                  />
                ) : (
                  <View style={{ }}>
                    {this.renderFilter()}

                    {this.state.list.length == 0 ?
                      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={images_path.pos_placeholder} />
                        <Text style={Styles.txtNoRecord}>{strings.send_and_check_status_of_branch_pos_requests}</Text>
                      </View> :
                      <View>
                        {this.renderList()}
                      </View>
                    }

                  </View>
                )}

              </View>

            </View>
          </ScrollView>
      </SafeAreaView >
    );
  }

  renderHeader() {
    return (
      <View style={Styles.cellHeader}>
        <Text style={Styles.txtHeader}>{strings.pos_requests}</Text>
        <TouchableOpacity
          onPress={() => { this.props.navigation.toggleDrawer() }}
          style={{ position: 'absolute', right: 0 }}>
          <Image source={images_path.menu} />
        </TouchableOpacity>
      </View>
    );
  }

  renderFilter() {
    return (
      <TouchableOpacity style={Styles.filterContainer}
        onPress={() => { this.props.navigation.navigate("AddPosScreen") }}>
        <LinearGradient
          colors={['#19CB3F', '#83DA48']}
          style={{ borderRadius: 10, }}
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 1 }}>
          <View style={{ padding: 10, flexDirection: 'row', alignItems: 'center' }}>
            <Text style={Styles.newTitle}>{strings.new_request}</Text>
            <Image source={images_path.user_plus} />
          </View>
        </LinearGradient>
      </TouchableOpacity>
    )
  }

  renderList() {
    return (
      <FlatList
        style={{ marginTop: 20 }}
        data={this.state.list}
        renderItem={this.renderItem}
      />
    )
  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => { }}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Image style={{ width : 50, height: 50, borderRadius: 10, resizeMode: 'contain' }} source={{uri: item.MerchantLogo}} />
            <View style={{ flex: 1, marginStart: 5 }}>
              <Text style={Styles.txtBranchName}>{item.BranchName}</Text>
              <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 12 }}>{
                moment(item.PosRequestedDate).format('DD/MM/YYYY')
              }</Text>
            </View>
          </View>

          <View style={{ alignItems: 'center', marginStart: 5 }}>
            <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12 }}>{item.PosStatus}</Text>
            {/* <TouchableOpacity onPress={() => { this.props.navigation.navigate("AddPosScreen", { posObj: item }) }}>
              <LinearGradient
                colors={['#19CB3F', '#83DA48']}
                style={{ borderRadius: 10, marginTop: 20 }}
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}>
                <Text style={Styles.itemEditTxt}>{strings.view_edit}</Text>
              </LinearGradient>
            </TouchableOpacity> */}
          </View>
        </View>

        <View style={Styles.itemSubContainer}>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12 }}>{item.PosManufacturer}</Text>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12, marginStart: 5 }}>{item.BID}</Text>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12, marginStart: 5 }}>{item.PosApiSupport}</Text>
        </View>

        <View style={Styles.vwSeparator} />
      </TouchableOpacity>
    )
  }

}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    backgroundColor: MyColors.whiteColor,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25
  },

  // ---------------------------Top
  cellHeader: {
    margin: 25,
  },
  txtHeader: {
    fontFamily: 'Sansation-Bold',
    fontSize: 26,
    marginTop: 80,
    color: MyColors.whiteColor,
  },

  txtNoRecord: {
    textAlign: 'center',
    fontFamily: 'Roboto-Medium',
    fontSize: 12,
    marginTop: 20,
  },

  newTitle: {
    color: MyColors.whiteColor,
    fontFamily: 'Roboto-Medium',
    marginEnd: 5,
    fontSize: 12
  },

  filterContainer: {
    flex: 1,
    flexDirection: 'row-reverse',
    alignItems: 'center',
    marginTop: 30,
  },

  itemEditTxt: {
    color: MyColors.whiteColor,
    fontFamily: 'Roboto-Medium',
    fontSize: 14,
    padding: 10,
  },

  txtBranchName: {
    fontFamily: 'Roboto-Medium',
    fontSize: 14,
    color: '#07321A',
  },
  vwSeparator: {
    flex: 1,
    height: 1,
    backgroundColor: MyColors.seperatorGreyColor,
    marginVertical: 15
  },

  itemSubContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    justifyContent: 'space-between',
  },

});
