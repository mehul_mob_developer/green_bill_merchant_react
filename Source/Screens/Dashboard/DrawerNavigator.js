import React, { ReactElement } from 'react';
import { DrawerItem, createDrawerNavigator } from '@react-navigation/drawer';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  DeviceEventEmitter,
  Alert,
  ImageBackground,
} from 'react-native';

const Drawer = createDrawerNavigator();

import {
  HomeStack,
  BranchStack,
  InvoiceStack,
  SubscriptionsStack,
  PosStack,
  PromotionsStack,
} from '../../../MainAppWithSwitchNavigator';

import { MyColors } from '../../Theme'
import Global from '../../Utility/Global'
import images_path from '@Images/Images';
import strings from '../../Localization/string';


class DrawerNavigator extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      token: '',
      user: {},
      merchant: {},
      subscription: {}
    };
  }

  componentDidMount() {
    Global.getCurrentToken().then(token => {
      Global.getCurrentUserPromise().then(user => {
        Global.getCurrentMerchantPromise().then(merchant => {
          this.setState({
            token: token,
            user: user,
            merchant: merchant,
          }, () => {
            console.log('user----------->>>>>>>', this.state.user)
            console.log('merchant------->>>>>>>', this.state.merchant)
          })
        })
      })
    })

    Global.getCurrentSubscriptionPromise().then(subscription => {
      this.setState({subscription: subscription}, () => {
        console.log('subscription----------->>>>>>>', this.state.subscription)
      })
    })

    DeviceEventEmitter.addListener('refreshUserMerchant', () => {
      Global.getCurrentUserPromise().then(user => {
        Global.getCurrentMerchantPromise().then(merchant => {
          this.setState({
            user: user,
            merchant: merchant
          })
        })
      })
    })
  }

  render() {
    return (
      <Drawer.Navigator
        drawerContent={(props) => <DrawerContent {...props} />}
        // initialRouteName="Home"
        // drawerStyle={{backgroundColor:'green'}}
        drawerContent={(props): ReactElement =>
          // <CustomDrawerContent {...props} />
          this.CustomDrawerContent(props.navigation)
        }>

        <Drawer.Screen
          // options={{ icon:<Image source={images_path.dashboard} style={{height:10,width:10}}/> }}
          name="HomeScreen"
          component={HomeStack}
        />

        <Drawer.Screen
          name="BranchScreen"
          component={BranchStack}
        />

        <Drawer.Screen
          name="InvoiceScreen"
          component={InvoiceStack}
        />

        <Drawer.Screen
          name="SubscriptionsScreen"
          component={SubscriptionsStack}
        />

        <Drawer.Screen
          name="PosScreen"
          component={PosStack}
        />

        <Drawer.Screen
          name="PromotionsScreen"
          component={PromotionsStack}
        />

      </Drawer.Navigator>
    );
  }

  CustomDrawerContent(navigation) {
    return (
      <View style={{ flex: 1 }}>

        <Image
          source={images_path.menu_footer}
          style={Styles.imgFooter}
        />

        <ScrollView>
          <View style={Styles.subContainer}>
            <TouchableOpacity
              style={Styles.profileContainer}
              onPress={() => { 
                  navigation.toggleDrawer()
                  navigation.navigate('CompleteProfileScreen') 
                }}>
                  
              <Image style={{height : 70, width : 70, borderRadius : 10}} source={{uri : this.state.merchant.businessLogo}} />

              <Text style={Styles.txtName}>{this.state.user.name}</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => { navigation.toggleDrawer() }}>
              <Image source={images_path.close} style={{ margin: 15 }} />
            </TouchableOpacity>
          </View>

          {this.state.subscription.name == undefined ? null :
            <TouchableOpacity
              style={{ alignSelf: 'center', marginTop: 15 }}
              onPress={() => { navigation.toggleDrawer(); navigation.navigate('SubscriptionsScreen'); }}>
              <ImageBackground source={images_path.ticket_yellow} style={{ width: 100, height: 45, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontFamily: 'Sansation-Bold', fontSize: 9, color: MyColors.whiteColor }}>Gold Plan</Text>
              </ImageBackground>
              <ImageBackground source={images_path.ticket_grey} style={{ width: 100, height: 100, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 15, color: MyColors.whiteColor }}>Active</Text>
                <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 7, color: MyColors.whiteColor, marginTop: 10 }}>{strings.renews_in}</Text>
                <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 9, color: MyColors.themeColor }}>37 Days</Text>
              </ImageBackground>
            </TouchableOpacity>
          }

          <View style={{ height: 20 }} />

          <DrawerItem
            label={strings.dashboard}
            labelStyle={Styles.menuLabel}
            icon={({ focused, color, size }) => (
              <Image source={images_path.dashboard} style={Styles.menuIcon} />
            )}
            onPress={() => {
              navigation.navigate('HomeScreen')
            }}
          />
          <DrawerItem
            label={strings.manange_branches}
            labelStyle={Styles.menuLabel}
            icon={({ focused, color, size }) => (
              <Image source={images_path.branches} style={Styles.menuIcon} />
            )}
            onPress={() => {
              navigation.navigate('BranchScreen');
              DeviceEventEmitter.emit('refreshOrderHistory')
            }}
          />
          <DrawerItem
            label={strings.manange_invoices}
            labelStyle={Styles.menuLabel}
            icon={({ focused, color, size }) => (
              <Image source={images_path.invoice} style={Styles.menuIcon} />
            )}
            onPress={() => {
              navigation.navigate('InvoiceScreen', { isDashboard: false });
            }}
          />
          <DrawerItem
            label={strings.subscription_plans}
            labelStyle={Styles.menuLabel}
            icon={({ focused, color, size }) => (
              <Image source={images_path.subscription} style={Styles.menuIcon} />
            )}
            onPress={() => {
              navigation.navigate('SubscriptionsScreen');
              DeviceEventEmitter.emit('refreshNotification')
            }}
          />
          <DrawerItem
            label={strings.request_for_pos_api}
            labelStyle={Styles.menuLabel}
            icon={({ focused, color, size }) => (
              <Image source={images_path.pos_request} style={Styles.menuIcon} />
            )}
            onPress={() => {
              navigation.navigate('PosScreen');
              DeviceEventEmitter.emit('refreshNotification')
            }}
          />
          <DrawerItem
            label={strings.promotions_and_offers}
            labelStyle={Styles.menuLabel}
            icon={({ focused, color, size }) => (
              <Image source={images_path.promotions_offers} style={Styles.menuIcon} />
            )}
            onPress={() => {
              navigation.navigate('PromotionsScreen');
              DeviceEventEmitter.emit('refreshNotification')
            }}
          />
          <DrawerItem
            label={strings.log_out}
            labelStyle={Styles.menuLabel}
            icon={({ focused, color, size }) => (
              <Image source={images_path.log_out} style={Styles.menuIcon} />
            )}
            onPress={() => {
              console.log('navigation', navigation);
              Alert.alert(
                strings.app_name,
                strings.are_you_sure_you_want_to_logout,
                [
                  { text: strings.no },
                  {
                    text: strings.yes,
                    onPress: () => {
                      navigation.toggleDrawer()
                      Global.clearUserDataFromDefaults()
                      navigation.navigate('Auth')
                    },
                  },
                ],
                { cancelable: false },
              );
            }}
          />
        </ScrollView>

      </View >
    );
  }

}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  subContainer: {
    marginTop: 50,
    marginStart: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },

  profileContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  txtName: {
    color: MyColors.black,
    marginHorizontal: 10,
    flex: 1,
    fontSize: 15,
    fontFamily: 'Sansation-Bold',
  },

  menuLabel: {
    marginLeft: -20,
    fontSize: 15,
    fontFamily: 'Roboto-Medium',
  },
  menuIcon: {
    width: 20,
    height: 20,
    resizeMode: 'center',
    marginStart: 15,
  },

  imgFooter: {
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center',
  }

});

export default DrawerNavigator;