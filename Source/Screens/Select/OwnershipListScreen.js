import React, { Component } from 'react'
import {
  View,
  TouchableOpacity,
  FlatList,
  Text,
  StyleSheet,
  TextInput,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native'
var filter = require('lodash.filter')

import { MyColors } from '../../Theme'
import Global from '../../Utility/Global';
import strings from '../../Localization/string'

import { isEmpty, showMessageAlert } from '../../Utility/Utility'
import { getMethodAPI } from '../../API/APIClient';
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class OwnershipListScreen extends Component {

  constructor() {
    super()
    this.state = {
      isLoading: true,
      token: '',

      keyword: '',
      list: [],
      filteredList: [],
      currentPage: 0,
      totalPages: 0,
    }
  }

  componentDidMount() {
    Global.getCurrentToken().then(token => {
      this.setState({ token: token }, () => {
        this.callOwnershipList()
      })
    })
  }

  callOwnershipList = () => {
    const apifetcherObj = getMethodAPI(
      'ownership/getAllOwnerships?page=' + this.state.currentPage + '&size=10', null, this.state.token
    )
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log('Success in API is >>> ', data)

        this.setState({ isLoading: false })

        if (statusCode == 200) {
          if (this.state.currentPage == 0) {
            this.setState({
              list: data.records,
              filteredList: data.records,
              currentPage: data.currentPage + 1,
              totalPages: data.totalPages,
            })
          } else {
            var array = this.state.list
            for (let symptom of data.records) {
              array.push(symptom)
            }
            this.setState({
              list: array,
              filteredList: array,
              currentPage: data.currentPage + 1,
              totalPages: data.totalPages,
            })
          }
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log('Error in API is >>> ', error)
        this.setState({ isLoading: false })
      })
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: MyColors.whiteColor, }}>
        <CustomStatusBarTheme />
        <View style={Styles.container}>
          {this.state.isLoading ? (
            <ActivityIndicator
              style={{ alignSelf: 'center' }}
              color={MyColors.themeColor}
              size='large'
            />
          ) : (
            this.state.list.length == 0 ?
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>{strings.no_record_found}</Text>
              </View> :
              <View style={{ flex: 1 }}>
                {this.renderSearch()}
                {this.renderList()}
              </View>
          )}
        </View>
      </SafeAreaView>
    )
  }

  renderSearch() {
    return (
      <View style={Styles.searchContainer}>
        <TextInput
          style={{ padding: 10 }}
          clearButtonMode='always'
          returnKeyType='search'
          placeholder={strings.search}
          value={this.state.keyword}
          onChangeText={txt => { this.setState({ keyword: txt }, () => { this.executeSearchoperation() }) }}
        />
      </View>
    )
  }

  renderList() {
    return (
      <FlatList
        style={{ marginTop: 20 }}
        data={this.state.filteredList}
        renderItem={this.renderItem}

        initialNumToRender={10}
        maxToRenderPerBatch={1}
        onEndReachedThreshold={0.5}
        onEndReached={({ distanceFromEnd }) => {
          console.log(' ***************** ' + distanceFromEnd)
          this.handleLoadMore()
        }}
      />
    )
  }

  handleLoadMore = () => {
    if ((this.state.currentPage != this.state.totalPages) && !this.state.isLoading) {
      this.setState({
        isLoading : true
      },() => {
        this.callOwnershipList()
      })  
    }
  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => this.onPressItem(item)}>
        <View>
          <Text style={Styles.txtTitle}>{item.ownership}</Text>
          <View style={Styles.vwSeparator} />
        </View>
      </TouchableOpacity>
    )
  }

  onPressItem = item => {
    console.log('navigation is >>> ', this.props)

    this.props.navigation.state.params.returnData(item)
    this.props.navigation.goBack()
  }

  executeSearchoperation = () => {
    const data = filter(this.state.list, item => {
      return this.contains(item, this.state.keyword)
    })
    this.setState({
      filteredList: data
    })
  }

  contains = ({ ownership }, query) => {
    if (ownership.includes(query)) {
      return true
    }
    return false
  }

}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: MyColors.whiteColor,
    padding: 15
  },
  txtTitle: {
    fontFamily: 'Roboto-Regular',
    fontSize: 16,
    marginHorizontal: 15
  },
  vwSeparator: {
    flex: 1,
    height: 1,
    backgroundColor: MyColors.seperatorGreyColor,
    marginVertical: 15
  },
  searchContainer: {
    justifyContent: 'center',
    borderRadius: 10,
    marginVertical: 10,
    backgroundColor: '#F1F1F4'
  }
})
