import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  Text,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import RazorpayCheckout from 'react-native-razorpay';

import { MyColors } from '../../Theme';
import images_path from '@Images/Images';
import Global from '../../Utility/Global';
import strings from '../../Localization/string';
import { showMessageAlert, isEmpty } from '../../Utility/Utility';
import { getMethodAPI, postMethodAPI, postMethodUploadImageAPI } from '../../API/APIClient';

import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp(percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;


export default class SubscriptionsScreen extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    isLoading: false,
    token: '',
    user: {},

    list: [],
    currentPage: 0,
    totalPages: 0,
  };

  componentDidMount() {
    Global.getCurrentToken().then(token => {
      this.setState({ token: token }, () => {
        this.callSubscriptionList()
      })
    })

    Global.getCurrentUserPromise().then(user => {
      this.setState({user: user})
    })
  }

  callSubscriptionList = () => {
    this.setState({ isLoading: true })

    const apifetcherObj = getMethodAPI(
      'masterSubscription/getAllMasterSubscriptions?page=' + this.state.currentPage + '&size=10',
      null, this.state.token
    )
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log('Success in API is >>> ', data)

        this.setState({ isLoading: false })

        if (statusCode == 200) {
          if (this.state.currentPage == 0) {
            this.setState({
              list: data.records,
              currentPage: data.currentPage + 1,
              totalPages: data.totalPages,
            })
          } else {
            var array = this.state.list
            for (let symptom of data.records) {
              array.push(symptom)
            }
            this.setState({
              list: array,
              currentPage: data.currentPage + 1,
              totalPages: data.totalPages,
            })
          }
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log('Error in API is >>> ', error)
        this.setState({ isLoading: false })
      })
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: MyColors.whiteColor }}>
        <CustomStatusBarTheme />

          <Image
            source={images_path.top_bg}
            style={{ width: '100%', position: 'absolute' }} />

          <ScrollView contentContainerStyle={{ flexGrow: 1, }} showsVerticalScrollIndicator={false} >
            <View style={{ flex: 1, }}>

              {this.renderHeader()}

              <View style={Styles.container}>

                {this.state.isLoading ? (
                  <ActivityIndicator
                    style={{ alignSelf: 'center' }}
                    color={MyColors.themeColor}
                    size='large'
                  />
                ) : (

                  this.state.list.length == 0 ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Image source={images_path.subscription_placeholder} />
                      <Text style={Styles.txtNoRecord}>{strings.create_and_manage_subscription_plans_here}</Text>
                    </View> :
                    <View>
                      {this.renderList()}
                      {this.renderFooter()}
                    </View>

                )}

              </View>

            </View>
          </ScrollView>
      </SafeAreaView >
    );
  }

  renderHeader() {
    return (
      <View style={Styles.cellHeader}>
        <Text style={Styles.txtHeader}>{strings.manage_subscriptions}</Text>
        <TouchableOpacity
          onPress={() => { this.props.navigation.toggleDrawer() }}
          style={{ position: 'absolute', right: 0 }}>
          <Image source={images_path.menu} />
        </TouchableOpacity>
      </View>
    );
  }

  renderList() {
    return (
      <Carousel
        containerCustomStyle={{ marginTop: 40, }}
        ref={(c) => { this._carousel = c; }}
        data={this.state.list}
        extraData={[this.state.list]}
        renderItem={this._renderItem}
        sliderWidth={sliderWidth}
        itemWidth={itemWidth}
        initialNumToRender={10}
        maxToRenderPerBatch={1}
        onEndReachedThreshold={0.5}
        onEndReached={({ distanceFromEnd }) => {
          console.log(' ***************** ' + distanceFromEnd)
          this.handleLoadMore()
        }}
      />
    )
  }

  handleLoadMore = () => {
    if ((this.state.currentPage != this.state.totalPages) && !this.state.isLoading) {
      this.setState({
        isLoading : true
      },() => {
        this.callSubscriptionList()
      })  
    }
  }

  _renderItem = ({ item, index }) => {
    return (
      <View style={Styles.itemContainer}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <Text style={{ flex: 1, fontFamily: 'Roboto-Bold', fontSize: 34, }}>{item.name}</Text>
          <Image style={{ width : 60, height: 40, resizeMode: 'contain' }} source={{ uri: item.image }} />
        </View>
        <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 14, marginTop: 5 }}>{item.description}</Text>
        <View style={{ flex: 1, flexDirection: 'row', marginTop: 20 }}>
          <View style={{ flex: 1, }}>
            <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 18, }}>{item.subscriptionPeriod + ' Days'}</Text>
            <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14, }}>{strings.validity}</Text>
          </View>
          <View style={{ flex: 1, }}>
            <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 18, }}>
              {Global.getFormatePriceWithCurrencySymbol(item.price, 2, true)}</Text>
            <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14, }}>{strings.price}</Text>
          </View>
        </View>
        <View style={{ flex: 1, marginTop: 15 }}>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 18, }}>{item.billCount + ' Bills'}</Text>
          <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14, }}>{strings.balance}</Text>
        </View>

        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginTop: 5, alignItems: 'center' }}>
          <Image style={{ width : 80, height: 100, resizeMode: 'contain' }} source={{ uri: item.assetImage }} />
          <TouchableOpacity style={Styles.itemBtnContainer} activeOpacity={0.9}
            onPress={() => { this.callSDKPayment(item) }}>
              {item.isBtnLoading ?
                <ActivityIndicator
                  style={{ alignSelf: 'center', paddingHorizontal: 25, paddingVertical: 10, }}
                  color={MyColors.whiteColor}
                /> :
                <Text style={Styles.itemBtnText}>{strings.buy_now}</Text>
              }
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  renderFooter() {
    return (
      <View style={{ marginTop: 20, marginHorizontal: 20 }}>
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
          <Image source={images_path.lock} />
          <Text style={Styles.txtSecureMsg}>{strings.hundred_prcent_secure_payments}</Text>
        </View>
        <TouchableOpacity style={Styles.termsContainer}
        onPress={()=>{ this.props.navigation.navigate('WebViewScreen', { url: 'https://www.google.com/' }) }}>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 15, }}>{strings.by_continuing_you_agree_to}</Text>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 15, color: '#2780DB' }}>{strings.terms_and_conditions}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  callSDKPayment(item) {
    console.log("this.state.user >>> ",this.state.user);
    console.log("item is  >>> ",item);
    
    if (item.isBtnLoading) {
      return;
    }

    var amount = parseInt(Global.getFormatePriceWithCurrencySymbol(item.price)) * 100
    console.log("amount is  >>> ",amount);
    if (amount == 0) {
      alert("Price cannot be zero")
      return
    }

    item.isBtnLoading = true
    this.setState({})

    var options = {
        description: item.name,
        image: item.image,
        currency: 'INR',
        key: 'rzp_test_Opzyqkn5LwdwOI',
        amount: amount,
        // recurring: true,
        name: this.state.user.name,
        // plan_id: this.state.result.razorpay_yearly_id, //Replace this with an order_id created using Orders API.
        // subscription_id: subscription, //Replace this with an order_id created using Orders API.
        // plan_id: 'plan_Hjl6AMiEG8YI8B',
        // subscription_id: subscription,
        prefill: {
          email: this.state.user.email,
          contact: this.state.user.mobile,
          name: this.state.user.name,
        },
        theme: {color: MyColors.themeColor}
      }
      RazorpayCheckout.open(options).then((data) => {
        console.log('data success inside razorpay---------------------------------------', JSON.stringify(data))
        this.callVerifyBuy(item, data.razorpay_payment_id)
      }).catch((error) => {
        console.log('error innside razorpay---------------------------------------', JSON.stringify(error))
        item.isBtnLoading = false
        this.setState({})
        // showMessageAlert(JSON.stringify(error))
      });


    // var options = {
    //   description: 'Credits towards consultation',
    //   image: 'https://i.imgur.com/3g7nmJC.png',
    //   currency: 'INR',
    //   key: 'rzp_test_Opzyqkn5LwdwOI', // Your api key
    //   amount: '5000',
    //   name: 'foo',
    //   prefill: {
    //     email: 'void@razorpay.com',
    //     contact: '9191919191',
    //     name: 'Razorpay Software'
    //   },
    //   theme: {color: '#F37254'}
    // }
    // RazorpayCheckout.open(options).then((data) => {
    //   // handle success
    //   alert(`Success: ${data.razorpay_payment_id}`);
    // }).catch((error) => {
    //   // handle failure
    //   alert(`Error: ${error.code} | ${error.description}`);
    // });
    
    return;
  }

  callVerifyBuy(item, razorpay_payment_id) {

    var params = JSON.stringify({
      razorpayOrderId: razorpay_payment_id
    });

    const apifetcherObj = postMethodAPI('razorpay/verify', params, this.state.token);
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log("Response ---->>>>\n ", data);

        item.isBtnLoading = false
        this.setState({})

        if (statusCode == 200) {
          
        }

        if (data.msg != undefined) {
          showMessageAlert(data.msg)
        }
      })
      .catch(error => {
        console.log("Error ---->>>> \n", error);
        item.isBtnLoading = false
        this.setState({})
        showMessageAlert(error)
      });
    return
  }

}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
    backgroundColor: MyColors.whiteColor
  },

  // ---------------------------Top
  cellHeader: {
    margin: 25,
  },
  txtHeader: {
    fontFamily: 'Sansation-Bold',
    fontSize: 26,
    marginTop: 80,
    color: MyColors.whiteColor,
  },
  txtNoRecord: {
    textAlign: 'center',
    fontFamily: 'Roboto-Medium',
    fontSize: 12,
    marginTop: 20,
  },

  itemContainer: {
    borderRadius: 25,
    paddingTop: 15,
    paddingHorizontal: 15,
    marginVertical: 10,
    backgroundColor: MyColors.whiteColor,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,  
    zIndex:999,  
  },
  itemBtnContainer: {
    borderRadius: 10,
    backgroundColor: MyColors.themeColor,
    justifyContent: 'center',

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,

    elevation: 11,
  },

  itemBtnText: {
    color: MyColors.whiteColor,
    fontFamily: 'Roboto-Bold',
    fontSize: 14,
    padding: 10,
  },

  termsContainer: {
    flexDirection: 'row', 
    justifyContent: 'center', 
    alignItems: 'center', 
    overflow: 'hidden',
  },
  txtSecureMsg: {
    fontFamily: 'Roboto-Medium', 
    fontSize: 15, 
    padding: 10, 
    color: MyColors.themeColor
  }

});
