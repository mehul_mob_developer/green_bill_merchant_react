import React, { Component } from 'react'
import {
  View,
  TouchableOpacity,
  FlatList,
  Text,
  StyleSheet,
  TextInput,
  ActivityIndicator,
  Image,
  // Switch,
  DeviceEventEmitter,
  SafeAreaView,
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient';

import { MyColors } from '../../Theme'
import Global from '../../Utility/Global';
import strings from '../../Localization/string'
import { getMethodAPI } from '../../API/APIClient';
import { isEmpty, showMessageAlert } from '../../Utility/Utility'
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class SearchBranchScreen extends Component {

  constructor() {
    super()
    this.state = {
      isLoading: true,
      token: '',
      user: {},
      merchant: {},

      isBranchId: false,
      isBranchName: false,
      isStoreName: false,

      keyword: '',
      list: [],
    }
  }

  componentDidMount() {
    Global.getCurrentToken().then(token => {
      Global.getCurrentUserPromise().then(user => {
        Global.getCurrentMerchantPromise().then(merchant => {
          this.setState({
            token: token,
            user: user,
            merchant: merchant,
          }, () => {
            console.log('user----------->>>>>>>', this.state.user)
            console.log('merchant------->>>>>>>', this.state.merchant)
            this.callBranchList()
          })
        })
      })
    })

    DeviceEventEmitter.addListener('AddEditBranch', () => {
      this.callBranchList()
    })
  }

  callBranchList = () => {
    this.setState({ isLoading: true })

    // var searchKey =
    //   this.state.branchId ? '&branchId=true' :
    //     this.state.isBranchName ? '&branchName=true' :
    //       this.state.isStoreName ? '&storeName=true' :
    //         ''

    const apifetcherObj = getMethodAPI(
      // 'merchantBranch/getAllMerchantBranchsForApp/' + this.state.merchant.id + '?' + searchKey +  '&searchedValue='+ this.state.keyword
      'merchantBranch/getAllMerchantBranchsForApp/' + this.state.merchant.id + '?' + 
      '&branchId=' + this.state.isBranchId +
      '&branchName=' + this.state.isBranchName  +
      '&storeName=' + this.state.isStoreName +
      '&searchedValue=' + this.state.keyword
      ,null,
      this.state.token
    )
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log('Success in API is >>> ', data)

        this.setState({ isLoading: false })

        if (statusCode == 200) {
          this.setState({ list: data.records })
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log('Error in API is >>> ', error)
        this.setState({ isLoading: false })
      })
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: MyColors.whiteColor, }}>
        <CustomStatusBarTheme />
        <View style={Styles.container}>
          {this.renderSearch()}
          {this.renderFilter()}

          <View style={{ flex: 1 }} >
            {this.state.isLoading ? (
              <ActivityIndicator
                style={{ alignSelf: 'center' }}
                color={MyColors.themeColor}
                size='large'
              />
            ) : (
              this.state.list == undefined || this.state.list.length == 0 ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text>{strings.no_record_found}</Text>
                </View> :
                <View style={{ flex: 1 }}>
                  {this.renderList()}
                </View>
            )}
          </View>
        </View>
      </SafeAreaView>
    )
  }

  renderSearch() {
    return (
      <View style={Styles.searchContainer}>
        <TextInput
          style={{ padding: 10 }}
          clearButtonMode='always'
          returnKeyType='search'
          placeholder={strings.search}
          value={this.state.keyword}
          onChangeText={txt => { this.setState({ keyword: txt }) }}
          onSubmitEditing={(event) => { this.onChangeTextSearch() }}
        />
      </View>
    )
  }

  onChangeTextSearch = (key, value) => {
    console.log('onChangeText', key, value);
    this.setState({
      [key]: value,
      next_offset: 0,
      list: [],
    }, () => { this.callBranchList() });
  };

  renderFilter() {
    return (
      <View style={{ marginVertical: 10 }}>
        <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14 }}>{strings.search_by}</Text>
        <View style={{ flexDirection: 'row', marginTop: 10, }}>
          <TouchableOpacity style={[Styles.txtTagContainer,
          { backgroundColor: this.state.isBranchId ? MyColors.themeColor : null }]}
            onPress={() => {this.setState({isBranchId: !this.state.isBranchId})}}>
            <Text style={[Styles.txtTag, { color: this.state.isBranchId ? MyColors.whiteColor : '#07321A' }]}>{strings.branch_id}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[Styles.txtTagContainer,
          { backgroundColor: this.state.isBranchName ? MyColors.themeColor : null, marginHorizontal: 5 }]}
            onPress={() => {this.setState({isBranchName: !this.state.isBranchName})}}>
            <Text style={[Styles.txtTag, { color: this.state.isBranchName ? MyColors.whiteColor : '#07321A' }]}>{strings.branch_name}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[Styles.txtTagContainer,
          { backgroundColor: this.state.isStoreName ? MyColors.themeColor : null }]}
            onPress={() => {this.setState({isStoreName: !this.state.isStoreName})}}>
            <Text style={[Styles.txtTag, { color: this.state.isStoreName ? MyColors.whiteColor : '#07321A' }]}>{strings.store_name}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderList() {
    return (
      <FlatList
        style={{ marginTop: 20 }}
        data={this.state.list}
        renderItem={this.renderItem}
        showsVerticalScrollIndicator={false}
      />
    )
  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => { }}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
          <Image style={{ width : 50, height: 50, borderRadius: 10, resizeMode: 'contain' }} source={{uri: this.state.merchant.businessLogo}} />
            <View style={{ flex: 1, marginStart: 5 }}>
              <Text style={Styles.txtBranchName}>{item.branchName}</Text>
              <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 12 }}>{item.managerName}</Text>
            </View>
          </View>

          <View style={{ alignItems: 'center', marginStart: 5 }}>
            <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12 }}>{item.branchBID}</Text>
            <LinearGradient
              colors={['#19CB3F', '#83DA48']}
              style={{ borderRadius: 10, marginTop: 20 }}
              start={{ x: 0, y: 1 }}
              end={{ x: 1, y: 1 }}>
              <TouchableOpacity onPress={() => { this.onPressEditItem(item) }}>
                <Text style={Styles.editTitleItem}>{strings.view_edit}</Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>
        </View>

        <View style={Styles.contactContainerItem}>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12 }}>{item.contactNumber}</Text>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12, marginStart: 20 }}>{item.streetAddress1}</Text>
        </View>

        <View style={Styles.vwSeparator} />
      </TouchableOpacity>
    )
  }

  onPressEditItem(item) {
    var branchObj = {}

    var generalInfo = {
      storeName: item.storeName,
      branchName: item.branchName,
      // ownershipId: item.ownershipId,
      websiteURL: item.websiteURL,
      websiteText: item.websiteText,
      // start_time: item.,
      // end_time: item.,
    }

    var taxInfo = {
      gstNumber: item.gstNumber,
      panNumber: item.panNumber,
      tinNumber: item.tinNumber,
      cinNumber: item.cinNumber,
    }

    var contactInfo = {
      feedbackEmail: item.feedbackEmail,
      customerCareEmail: item.customerCareEmail,
      whatsappOrderNumber: item.whatsappOrderNumber,
      instructionOnBill: item.instructionOnBill,
      contactNumber: item.contactNumber,
    }

    var storeAddress = {
      streetAddress1: item.streetAddress1,
      streetAddress2: item.streetAddress2,
      // countryId: this.state.selectedCountry.id,
      // stateId: this.state.selectedState.id,
      // city: this.state.selectedCity.id,
      pinCode: item.pinCode,
    }

    branchObj.generalInfo = generalInfo
    branchObj.taxInfo = taxInfo
    branchObj.contactInfo = contactInfo
    branchObj.storeAddress = storeAddress

    this.props.navigation.navigate('AddGeneralInfoScreen', {
      branchObj: branchObj,
      isEdit: true,
      branchId: item.id,
    })
  }

}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: MyColors.whiteColor,
    padding: 15
  },
  searchContainer: {
    justifyContent: 'center',
    borderRadius: 10,
    marginVertical: 10,
    backgroundColor: '#F1F1F4'
  },

  txtTagContainer: {
    borderWidth: 1,
    borderRadius: 10,
    borderColor: MyColors.themeColor,
  },
  txtTag: {
    fontFamily: 'Roboto-Regular',
    fontSize: 16,
    padding: 8,
  },

  editTitleItem: {
    color: MyColors.whiteColor,
    fontFamily: 'Roboto-Medium',
    fontSize: 14,
    padding: 10,
  },

  txtBranchName: {
    fontFamily: 'Roboto-Medium',
    fontSize: 14,
    color: '#2F80ED',
  },
  vwSeparator: {
    flex: 1,
    height: 1,
    backgroundColor: MyColors.seperatorGreyColor,
    marginVertical: 15
  },

  contactContainerItem: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  }

})
