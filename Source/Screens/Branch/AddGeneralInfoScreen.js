import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Platform,
  Image,
  TouchableOpacity,
  SafeAreaView,
  Text,
  BackHandler,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ImagePicker from "react-native-image-crop-picker";
import ImageResizer from "react-native-image-resizer";
import ActionSheet from 'react-native-actionsheet';
import { TimePickerModal } from 'react-native-paper-dates'
import moment from "moment";

import { MyColors } from '../../Theme';
import images_path from '@Images/Images';
import Global from '../../Utility/Global';
import strings from '../../Localization/string';
import { showMessageAlert, isEmpty } from '../../Utility/Utility';
import { putMethodAPI, postMethodAPI, postMethodUploadImageAPI } from '../../API/APIClient';

import TitleTxtFld from '../../customComponents/TitleTxtFld'
import CustomButton from '../../customComponents/CustomButton';
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class AddGeneralInfoScreen extends Component {

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  state = {
    isLoading: false,
    token: '',
    user: {},
    merchant: {},
    branchObj: {},
    isEdit: false,
    branchId: '',

    profileImage: '',

    store_name: '',
    branch_name: '',
    manager_name: '',
    ownership_type: '',
    selectedType: {},
    website_url: '',
    website_text: '',
    start_time: '',
    end_time: '',

    isPickTime: false,
    isStart: true,

    optionArray: [strings.camera, strings.photos, strings.cancel],
  };

  componentDidMount() {
    if (this.props.navigation.state != null && this.props.navigation.state.params != undefined) {
      this.setState(
        {
          branchObj: this.props.navigation.state.params.branchObj,
          isEdit: this.props.navigation.state.params.isEdit,
          branchId: this.props.navigation.state.params.branchId
        },
        () => {
          if(this.state.branchObj != undefined){
            console.log('branchObj ========== > ', this.state.branchObj)
            var generalInfo = this.state.branchObj.generalInfo
            if (generalInfo != undefined) {
              this.setState(
                {
                  store_name: generalInfo.storeName,
                  branch_name: generalInfo.branchName,
                  manager_name: generalInfo.managerName,
                  ownership_type: generalInfo.OwnerShipType,
                  website_url: generalInfo.websiteURL,
                  website_text: generalInfo.websiteText,
                  start_time: generalInfo.start_time,
                  end_time: generalInfo.end_time,
                  selectedType: generalInfo.selectedType,
                })
            }
          }
        }
      )
    }

    Global.getCurrentToken().then(token => {
      Global.getCurrentUserPromise().then(user => {
        Global.getCurrentMerchantPromise().then(merchant => {
          this.setState({
            token: token,
            user: user,
            merchant: merchant,
          }, () => {
            console.log('user----------->>>>>>>', this.state.user)
            console.log('merchant------->>>>>>>', this.state.merchant)
          })
        })
      })
    })

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  handleBackButtonClick() {
    this.props.navigation.goBack()
    return true;
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomStatusBarTheme />
        <Image
          source={images_path.top_bg}
          style={{ width: '100%', position: 'absolute' }} />

        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} >
          {/* <View style={{ flex: 1, }}> */}

          {/* {this.renderProfile()} */}

          <View style={Styles.container}>

            {this.renderStoreNameField()}
            {this.renderBranchNameField()}
            {this.renderManagerNameField()}
            {this.renderTypeField()}
            {this.renderWebsiteField()}
            {this.renderWebsiteTextField()}
            {this.renderTime()}

            {this.renderButton()}

            {this.renderActionSheet()}
            {this.renderCalendar()}
          </View>
          {/* </View> */}
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }

  renderProfile() {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <TouchableOpacity style={{ position: 'absolute' }}
          onPress={() => this.handleBackButtonClick()}>
          <Image source={images_path.back_white}
            style={Styles.imgBack}/>
        </TouchableOpacity>
        <View style={{ flex: 1, }}>
          <TouchableOpacity style={Styles.cellImage}
            onPress={() => this.ActionSheet.show()}
          >
            <Image
              // style={{
              //   width: isEmpty(this.state.profileImage) ? 33 : '100%',
              //   height: isEmpty(this.state.profileImage) ? 28 : '100%',
              //   borderRadius: isEmpty(this.state.profileImage) ? 0 : 15,
              //   resizeMode: 'cover',
              // }}
              style={{
                height: '100%', width: '100%',
                resizeMode: isEmpty(this.state.profileImage) ? 'center' : 'cover',
                borderRadius: isEmpty(this.state.profileImage) ? 0 : 15
              }}
              source={isEmpty(this.state.profileImage) ? images_path.camera : { uri: this.state.profileImage }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  renderStoreNameField() {
    return (
      <TitleTxtFld
        value={this.state.store_name}
        title={strings.store_name}
        placeholder={strings.enter_store_name}
        onChangeTxt={txt => this.setState({ store_name: txt })}
      />
    );
  }

  renderBranchNameField() {
    return (
      <TitleTxtFld
        value={this.state.branch_name}
        title={strings.branch_name}
        placeholder={strings.enter_branch_name}
        onChangeTxt={txt => this.setState({ branch_name: txt })}
      />
    );
  }

  renderManagerNameField() {
    return (
      <TitleTxtFld
        value={this.state.manager_name}
        title={strings.manager_name}
        placeholder={strings.enter_manager_name}
        onChangeTxt={txt => this.setState({ manager_name: txt })}
      />
    );
  }

  renderTypeField() {
    return (
      <TitleTxtFld
        value={this.state.ownership_type}
        title={strings.ownership_type}
        placeholder={strings.select_ownership_type}
        onChangeTxt={txt => this.setState({ ownership_type: txt })}
        isSelectable
        rightImageSource={images_path.arrow_down}
        onPress={() => this.onPressSelectType()}
      />
    );
  }

  renderWebsiteField() {
    return (
      <TitleTxtFld
        value={this.state.website_url}
        title={strings.website_url}
        placeholder={strings.enter_website_url}
        onChangeTxt={txt => this.setState({ website_url: txt })}
      />
    );
  }

  renderWebsiteTextField() {
    return (
      <TitleTxtFld
        value={this.state.website_text}
        title={strings.website_text}
        placeholder={strings.enter_website_text}
        onChangeTxt={txt => this.setState({ website_text: txt })}
      />
    );
  }

  renderTime() {
    return (
      <View>
        <Text style={Styles.timeTitle}>{strings.working_hours}</Text>
        <View style={Styles.timeSubConainer}>
          <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14 }}>{strings.timings}</Text>

          <TouchableOpacity style={{ flex: 1, }} onPress={() => { this.setState({ isPickTime: true, isStart: true }) }}>
            <Text style={Styles.textInput} >{this.state.start_time}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, }} onPress={() => { this.setState({ isPickTime: true, isStart: false }) }}>
            <Text style={Styles.textInput} >{this.state.end_time}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderButton() {
    return (
      <View style={{ flexDirection: 'row', marginTop: 50, }}>
        {/* <CustomButton
          title={strings.done}
          isLoading={this.state.isLoading}
          buttonCustomStyle={Styles.btnDone}
          onPress={() => this.tappedOnButton()}
        /> */}
        <CustomButton
          title={strings.continue}
          isLoading={this.state.isLoading}
          buttonCustomStyle={Styles.btnContinue}
          onPress={() => this.tappedOnButton()}
        />
      </View>
    );
  }

  renderActionSheet = () => {
    return (
      <ActionSheet
        ref={o => (this.ActionSheet = o)}
        //Title of the Bottom Sheet
        title={strings.which_one_do_you_like}
        //Options Array to show in bottom sheet
        options={this.state.optionArray}
        //Define cancel button index in the option array
        //this will take the cancel option in bottom and will highlight it
        cancelButtonIndex={2}
        //If you want to highlight any specific option you can use below prop
        destructiveButtonIndex={2}
        onPress={index => {
          if (index === 0) {
            this.openCamera();
          } else if (index === 1) {
            this.openPhotos();
          }
        }}
      />
    );
  }

  openCamera() {
    ImagePicker.openCamera({
      width: 400,
      height: 400,
      quality: 0.4,
      cropping: true,
      // showCropFrame: true,
      // multiple: true,
      mediaType: 'photo',
      maxFiles: 10000,
    }).then(image => {
      ImageResizer.createResizedImage(image.path, 400, 400, "JPEG", 80, 0).then(
        uri => {
          var path = uri.uri
          if (Platform.OS == "ios") {
            path = uri.path
          }
          this.setState({ profileImage: path })
        }
      );
    });
  }

  openPhotos() {
    ImagePicker.openPicker({
      width: 400,
      height: 400,
      quality: 0.4,
      cropping: true,
      // showCropFrame: true,
      mediaType: 'photo',
      maxFiles: 10000,
    }).then(image => {
      ImageResizer.createResizedImage(image.path, 400, 400, "JPEG", 80, 0).then(
        uri => {
          var path = uri.uri
          if (Platform.OS == "ios") {
            path = uri.path
          }
          this.setState({ profileImage: path })
        }
      );
    });
  }

  onPressSelectType = () => {
    this.props.navigation.navigate('OwnershipListScreen', {
      returnData: this.refreshSelectedType.bind(this)
    })
  }

  refreshSelectedType(selectedType) {
    console.log("selectedType is >>> ", selectedType);
    this.setState({
      selectedType: selectedType,
      ownership_type: selectedType.ownership,
    })
  }

  renderCalendar() {
    return (
      <TimePickerModal
        visible={this.state.isPickTime}
        onDismiss={this._hideDateCancel}
        onConfirm={this._handleDateConfirm}
      // hours={12} // default: current hours
      // minutes={14} // default: current minutes
      // label="Select time" // optional, default 'Select time'
      // cancelLabel="Cancel" // optional, default: 'Cancel'
      // confirmLabel="Ok" // optional, default: 'Ok'
      // animationType="slide" // optional, default is 'none'
      // locale={'en'} // optional, default is automically detected by your system
      />
    )
  }

  _hideDateCancel = () => this.setState({ isPickTime: false });

  _handleDateConfirm = ({ hours, minutes }) => {
    console.log('A date has been picked: ', hours + '' + minutes);
    this._hideDateCancel();

    var start_time = this.state.start_time
    var end_time = this.state.end_time

    if (this.state.isStart) {
      var startTime = moment(hours + ':' + minutes, 'HH:mm').format("hh:mm a")
      if(end_time != undefined && end_time == startTime) {
        showMessageAlert(strings.please_select_diffrent_working_hours)
      } else {
        this.setState({ start_time: startTime })
      }
    } else {
      var endTime = moment(hours + ':' + minutes, 'HH:mm').format("hh:mm a")
      if(start_time != undefined && start_time == endTime) {
        showMessageAlert(strings.please_select_diffrent_working_hours)
      } else {
        this.setState({ end_time: endTime })
      }
    }
  }

  tappedOnButton() {

    if (this.state.isLoading) {
      return;
    }

    if (isEmpty(this.state.store_name)) {
      showMessageAlert(strings.please_enter_store_name)
      return
    } else if (isEmpty(this.state.branch_name)) {
      showMessageAlert(strings.please_enter_branch_name)
      return
    } else if (isEmpty(this.state.manager_name)) {
      showMessageAlert(strings.please_enter_manager_name)
      return
    } else if (isEmpty(this.state.ownership_type)) {
      showMessageAlert(strings.please_select_ownership_type)
      return
    } else if (isEmpty(this.state.website_url)) {
      showMessageAlert(strings.please_enter_website_url)
      return
    } else if (!this.isValidURL(this.state.website_url)) {
      showMessageAlert(strings.please_enter_valid_website_url)
      return
    }  else if (isEmpty(this.state.website_text)) {
      showMessageAlert(strings.please_enter_website_text)
      return
    } else if (isEmpty(this.state.start_time) || isEmpty(this.state.end_time)) {
      showMessageAlert(strings.please_select_working_hours)
      return
    }

    var generalInfo = {
      merchantId: this.state.merchant.id,
      storeName: this.state.store_name,
      branchName: this.state.branch_name,
      managerName: this.state.manager_name,
      ownershipId: this.state.selectedType.id,
      websiteURL: this.state.website_url,
      websiteText: this.state.website_text,
      shopTiming: this.state.start_time + '-' + this.state.end_time,
      status: '1',
      BID : this.state.merchant.BID
    }

    var branchObj = {}
    if(this.state.branchObj != undefined){
      branchObj = this.state.branchObj
    }
    branchObj.generalInfo = generalInfo
    this.setState({ branchObj: branchObj }, () => {
      console.log('this.state.branchObj---------------------------', this.state.branchObj)

      this.props.navigation.navigate('AddTaxInfoScreen', {
        branchObj: this.state.branchObj,
        isEdit: this.state.isEdit,
        branchId: this.state.branchId,
      })
    })

    // this.setState({ isLoading: true })

    // if (this.state.profileImage == '') {
    //   this.callMerchantDetails('')
    // } else {
    //   this.callUploadProfilePic()
    // }

    return;
  }

  // callUploadProfilePic = () => {
  //   var formdata = new FormData();
  //   let photo = {
  //     uri: this.state.profileImage,
  //     type: "image/jpeg",
  //     name: "image.jpg"
  //   };

  //   formdata.append("profileImage", photo);

  //   const apifetcherObj = postMethodUploadImageAPI('profileImage/uploadProfileImage', formdata, this.state.token)
  //   apifetcherObj
  //     .then(response => { return Promise.all([response.status, response.json()]) })
  //     .then(res => {
  //       let statusCode = res[0]
  //       let data = res[1]
  //       console.log("Response >>>", data);

  //       if (statusCode == 200) {
  //         this.callMerchantDetails(data.url)
  //       }
  //       if (data.error != undefined) {
  //         showMessageAlert(data.error)
  //       }
  //     })
  //     .catch(error => {
  //       console.log("Error >>>", error);
  //       this.setState({ isLoading: false });
  //       showMessageAlert(error)
  //     });
  //   return;
  // }

  // callMerchantDetails(imageUrl) {

  //   var params = JSON.stringify({
  //     merchantId: this.state.merchant.id,
  //     title: this.state.store_name,
  //     subTitle: this.state.branch_name,
  //     description: this.state.website_url,
  //     terms: this.state.website_text,
  //     templateName: this.state.template_name,
  //     image: imageUrl,
  //   });

  //   const apifetcherObj = postMethodAPI('merchantDetails/createMerchantDetails', params, this.state.token)
  //   apifetcherObj
  //     .then(response => { return Promise.all([response.status, response.json()]) })
  //     .then(res => {
  //       let statusCode = res[0]
  //       let data = res[1]
  //       console.log("Response ---->>>>\n ", data);

  //       if (statusCode == 200) {
  //         this.setState({ isLoading: false }, () => {
  //           this.props.navigation.goBack()
  //         })
  //       }

  //       if (data.error != undefined) {
  //         showMessageAlert(data.error)
  //       }
  //     })
  //     .catch(error => {
  //       console.log("Error ---->>>> \n", error);
  //       this.setState({ isLoading: false });
  //       showMessageAlert(error)
  //     });
  //   return;
  // }

  isValidURL(userInput) {
    var res = userInput.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    if(res == null)
      return false
    else
      return true
}

}

const Styles = StyleSheet.create({
  container: {
    padding: 25,
    backgroundColor: MyColors.whiteColor,
    // borderTopRightRadius: 25,
    // borderTopLeftRadius: 25
  },

  // ---------------------------Profile
  cellImage: {
    width: 110,
    height: 110,
    alignSelf: 'center',
    margin: 20,
    borderWidth: 5,
    borderRadius: 20,
    borderColor: MyColors.whiteColor,
    alignItems: 'center',
    justifyContent: 'center',
  },

  imgBack: {
    resizeMode: 'center',
    margin: 25
  },

  timeTitle: {
    fontFamily: 'Sansation-Bold',
    fontSize: 18,
    color: '#5A607B',
    marginTop: 25,
  },
  timeSubConainer: {
    flex: 1,
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },

  btnDone: {
    flex: 1,
    minWidth: 0,
    paddingHorizontal: 20,
    marginEnd: 5,
  },
  btnContinue: {
    flex: 1.5,
    minWidth: 0,
    paddingHorizontal: 20,
    // marginStart: 5,
  },

  textInput: {
    paddingStart: 20,
    padding: 15,
    fontSize: 16,
    color: MyColors.blackColor,
    fontFamily: 'Roboto-Regular',
    backgroundColor: '#F1F1F4',
    borderRadius: 10,
    marginHorizontal: 5,
  }

});
