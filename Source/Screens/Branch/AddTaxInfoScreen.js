import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { MyColors } from '../../Theme';
import strings from '../../Localization/string';
import { showMessageAlert, isEmpty } from '../../Utility/Utility';

import TitleTxtFld from '../../customComponents/TitleTxtFld'
import CustomButton from '../../customComponents/CustomButton';
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class AddTaxInfoScreen extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    isLoading: false,
    branchObj: {},
    isEdit: false,
    branchId: '',

    gst_number: '',
    pan_number: '',
    cin_number: '',
    tin_number: '',
  };

  componentDidMount() {
    if (this.props.navigation.state != null && this.props.navigation.state.params != undefined) {
      this.setState(
        {
          branchObj: this.props.navigation.state.params.branchObj,
          isEdit: this.props.navigation.state.params.isEdit,
          branchId: this.props.navigation.state.params.branchId
        },
        () => {
          if(this.state.branchObj != undefined){
            console.log('branchObj ========== > ', this.state.branchObj)
            var taxInfo = this.state.branchObj.taxInfo
            if (taxInfo != undefined) {
              this.setState(
                {
                  gst_number: taxInfo.gstNumber,
                  pan_number: taxInfo.panNumber,
                  cin_number: taxInfo.tinNumber,
                  tin_number: taxInfo.cinNumber,
                })
            }
          }
        }
      )
    }

  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: MyColors.whiteColor, }}>
        <CustomStatusBarTheme />
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} >
          <View style={Styles.container}>

            {this.renderGstField()}
            {this.renderPanField()}
            {this.renderTinField()}
            {this.renderCinField()}

            {this.renderButton()}
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }

  renderGstField() {
    return (
      <TitleTxtFld
        value={this.state.gst_number}
        title={strings.gst_number}
        placeholder={strings.enter_gst_number}
        onChangeTxt={txt => this.setState({ gst_number: txt })}
      />
    );
  }

  renderPanField() {
    return (
      <TitleTxtFld
        value={this.state.pan_number}
        title={strings.pan_number}
        placeholder={strings.enter_pan_number}
        onChangeTxt={txt => this.setState({ pan_number: txt })}
      />
    );
  }

  renderTinField() {
    return (
      <TitleTxtFld
        value={this.state.tin_number}
        title={strings.tin_number}
        placeholder={strings.enter_tin_number}
        onChangeTxt={txt => this.setState({ tin_number: txt })}
      />
    );
  }

  renderCinField() {
    return (
      <TitleTxtFld
        value={this.state.cin_number}
        title={strings.cin_number}
        placeholder={strings.enter_cin_number}
        onChangeTxt={txt => this.setState({ cin_number: txt })}
      />
    );
  }

  renderButton() {
    return (
      <View style={{ flexDirection: 'row', marginTop: 50, }}>
        {/* <CustomButton
          title={strings.done}
          isLoading={this.state.isLoading}
          buttonCustomStyle={Styles.btnDone}
          onPress={() => this.tappedOnButton()}
        /> */}
        <CustomButton
          title={strings.continue}
          isLoading={this.state.isLoading}
          buttonCustomStyle={Styles.btnContinue}
          onPress={() => this.tappedOnButton()}
        />
      </View>
    );
  }

  tappedOnButton() {

    if (this.state.isLoading) {
      return;
    }

    if (isEmpty(this.state.gst_number)) {
      showMessageAlert(strings.please_enter_gst_number)
      return
    } else if (isEmpty(this.state.pan_number)) {
      showMessageAlert(strings.please_enter_pan_number)
      return
    } else if (isEmpty(this.state.tin_number)) {
      showMessageAlert(strings.please_enter_tin_number)
      return
    } else if (isEmpty(this.state.cin_number)) {
      showMessageAlert(strings.please_enter_cin_number)
      return
    }

    var taxInfo = {
      gstNumber: this.state.gst_number,
      panNumber: this.state.pan_number,
      tinNumber: this.state.tin_number,
      cinNumber: this.state.cin_number,
    }

    var branchObj = this.state.branchObj
    branchObj.taxInfo = taxInfo
    this.setState({ branchObj: branchObj }, () => {
      console.log('this.state.branchObj---------------------------', this.state.branchObj)

      this.props.navigation.navigate('AddContactInfoScreen', {
        branchObj: this.state.branchObj,
        isEdit: this.state.isEdit,
        branchId: this.state.branchId,
      })
    })

    return;
  }

}

const Styles = StyleSheet.create({
  container: {
    padding: 25,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25
  },
  btnDone: {
    flex: 1,
    minWidth: 0,
    paddingHorizontal: 20,
    marginEnd: 5,
  },
  btnContinue: {
    flex: 1.5,
    minWidth: 0,
    paddingHorizontal: 20,
    // marginStart: 5,
  },

});
