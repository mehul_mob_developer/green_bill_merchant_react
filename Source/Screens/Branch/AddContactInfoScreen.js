import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { MyColors } from '../../Theme';
import strings from '../../Localization/string';
import { showMessageAlert, isEmpty, isValidEmail } from '../../Utility/Utility';

import TitleTxtFld from '../../customComponents/TitleTxtFld'
import CustomButton from '../../customComponents/CustomButton';
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class AddContactInfoScreen extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    isLoading: false,
    branchObj: {},
    isEdit: false,
    branchId: '',

    feedback_email: '',
    care_email: '',
    whatsapp_number: '',
    instructions: '',
    contact_number: '',
  };

  componentDidMount() {
    if (this.props.navigation.state != null && this.props.navigation.state.params != undefined) {
      this.setState(
        {
          branchObj: this.props.navigation.state.params.branchObj,
          isEdit: this.props.navigation.state.params.isEdit,
          branchId: this.props.navigation.state.params.branchId
        },
        () => {
          if(this.state.branchObj != undefined){
            console.log('branchObj ========== > ', this.state.branchObj)
            var contactInfo = this.state.branchObj.contactInfo
            if (contactInfo != undefined) {
              this.setState(
                {
                  feedback_email: contactInfo.feedbackEmail,
                  care_email: contactInfo.customerCareEmail,
                  whatsapp_number: contactInfo.whatsappOrderNumber.toString(),
                  instructions: contactInfo.instructionOnBill,
                  contact_number: contactInfo.contactNumber.toString(),
                })
            }
          }
        }
      )
    }

  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: MyColors.whiteColor, }}>
        <CustomStatusBarTheme />
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} >
          <View style={Styles.container}>

            {this.renderEmailField()}
            {this.renderCareEmailField()}
            {this.renderWhatsAppNumberField()}
            {this.renderInstructionsField()}
            {this.renderContactField()}

            {this.renderButton()}
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }

  renderEmailField() {
    return (
      <TitleTxtFld
        keyboardType='email-address'
        value={this.state.feedback_email}
        title={strings.feedback_email}
        placeholder={strings.enter_feedback_email}
        onChangeTxt={txt => this.setState({ feedback_email: txt })}
      />
    );
  }

  renderCareEmailField() {
    return (
      <TitleTxtFld
        keyboardType='email-address'
        value={this.state.care_email}
        title={strings.customer_care_email}
        placeholder={strings.enter_customer_care_email}
        onChangeTxt={txt => this.setState({ care_email: txt })}
      />
    );
  }

  renderWhatsAppNumberField() {
    return (
      <TitleTxtFld
        value={this.state.whatsapp_number}
        title={strings.whatsapp_order_number}
        placeholder={strings.enter_whatsapp_order_number}
        onChangeTxt={txt => this.setState({ whatsapp_number: txt })}
        keyboardType='number-pad'
      />
    );
  }

  renderInstructionsField() {
    return (
      <TitleTxtFld
        value={this.state.instructions}
        title={strings.instructions_on_bill}
        placeholder={strings.enter_instructions_on_bill}
        onChangeTxt={txt => this.setState({ instructions: txt })}
      />
    );
  }

  renderContactField() {
    return (
      <TitleTxtFld
        value={this.state.contact_number}
        title={strings.contact_number}
        placeholder={strings.enter_contact_number}
        onChangeTxt={txt => this.setState({ contact_number: txt })}
        keyboardType='number-pad'
      />
    );
  }

  renderButton() {
    return (
      <View style={{ flexDirection: 'row', marginTop: 50, }}>
        {/* <CustomButton
          title={strings.done}
          isLoading={this.state.isLoading}
          buttonCustomStyle={Styles.btnDone}
          onPress={() => this.tappedOnButton()}
        /> */}
        <CustomButton
          title={strings.continue}
          isLoading={this.state.isLoading}
          buttonCustomStyle={Styles.btnContinue}
          onPress={() => this.tappedOnButton()}
        />
      </View>
    );
  }

  tappedOnButton() {

    if (this.state.isLoading) {
      return;
    }

    if (isEmpty(this.state.feedback_email)) {
      showMessageAlert(strings.please_enter_feedback_email)
      return
    } else if (!isValidEmail(this.state.feedback_email)) {
      showMessageAlert(strings.please_enter_valid_email_id)
      return
    } else if (isEmpty(this.state.care_email)) {
      showMessageAlert(strings.please_enter_customer_care_email)
      return
    } else if (!isValidEmail(this.state.care_email)) {
      showMessageAlert(strings.please_enter_valid_email_id)
      return
    } else if (isEmpty(this.state.contact_number.toString())) {
      showMessageAlert(strings.please_enter_contact_number)
      return
    } else if (!this.isValidMobileNumber(this.state.contact_number)) {
      showMessageAlert(strings.please_enter_valid_contact_number)
      return
    }

    var contactInfo = {
      feedbackEmail: this.state.feedback_email,
      customerCareEmail: this.state.care_email,
      whatsappOrderNumber: this.state.whatsapp_number,
      instructionOnBill: this.state.instructions,
      contactNumber: this.state.contact_number,
    }

    var branchObj = this.state.branchObj
    branchObj.contactInfo = contactInfo
    this.setState({ branchObj: branchObj }, () => {
      console.log('this.state.branchObj---------------------------', this.state.branchObj)

      this.props.navigation.navigate('AddBranchAddressScreen', {
        branchObj: this.state.branchObj,
        isEdit: this.state.isEdit,
        branchId: this.state.branchId,
      })
    })

    return;
  }

  isValidMobileNumber = value => {
    let reg = /^([0-9]){10}$/;
    if (reg.test(value) === false) {
      return false;
    } else {
      return true;
    }
  };

}

const Styles = StyleSheet.create({
  container: {
    padding: 25,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25
  },
  btnDone: {
    flex: 1,
    minWidth: 0,
    paddingHorizontal: 20,
    marginEnd: 5,
  },
  btnContinue: {
    flex: 1.5,
    minWidth: 0,
    paddingHorizontal: 20,
    // marginStart: 5,
  },

});
