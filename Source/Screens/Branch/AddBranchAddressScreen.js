import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  DeviceEventEmitter,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { putMethodAPI, postMethodAPI } from '../../API/APIClient';

import { MyColors } from '../../Theme';
import images_path from '@Images/Images';
import Global from '../../Utility/Global';
import strings from '../../Localization/string';
import { showMessageAlert, isEmpty } from '../../Utility/Utility';

import TitleTxtFld from '../../customComponents/TitleTxtFld'
import CustomButton from '../../customComponents/CustomButton';
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class AddBranchAddressScreen extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    isLoading: false,
    token: '',
    branchObj: {},
    isEdit: false,
    branchId: '',

    address_one: '',
    address_two: '',
    country: '',
    selectedCountry: {},
    region: '',
    selectedRegion: {},
    state: '',
    selectedState: {},
    city: '',
    selectedCity: {},
    pin_code: '',
    merchant : {}
  };

  componentDidMount() {
    if (this.props.navigation.state != null && this.props.navigation.state.params != undefined) {
      this.setState(
        {
          branchObj: this.props.navigation.state.params.branchObj,
          isEdit: this.props.navigation.state.params.isEdit,
          branchId: this.props.navigation.state.params.branchId
        },
        () => {
          if(this.state.branchObj != undefined){
            console.log('branchObj ========== > ', this.state.branchObj)
            var storeAddress = this.state.branchObj.storeAddress
            if (storeAddress != undefined) {
              this.setState(
                {
                  address_one: storeAddress.streetAddress1,
                  address_two: storeAddress.streetAddress2,
                  country: storeAddress.country,
                  region: storeAddress.region,
                  state: storeAddress.state,
                  city: storeAddress.city,
                  pin_code: storeAddress.pinCode.toString(),
                  selectedCountry: storeAddress.selectedCountry,
                  selectedRegion: storeAddress.selectedRegion,
                  selectedState: storeAddress.selectedState,
                  selectedCity: storeAddress.selectedCity,
                })
            }
          }
        }
      )
    }
    Global.getCurrentMerchantPromise().then(merchant => {
      this.setState({
        merchant : merchant
      })
    })
    Global.getCurrentToken().then(token => {
      this.setState({ token: token }, () => { })
    })
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: MyColors.whiteColor, }}>
        <CustomStatusBarTheme />
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} >
          <View style={Styles.container}>

            {this.renderAddressOneField()}
            {this.renderAddressTwoField()}
            {this.renderCountryField()}
            {this.renderRegionField()}
            {this.renderStateField()}
            {this.renderCityField()}
            {this.renderPinCodeField()}

            {this.renderButton()}
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }

  renderAddressOneField() {
    return (
      <TitleTxtFld
        value={this.state.address_one}
        title={strings.street_address_one}
        placeholder={strings.enter_address}
        onChangeTxt={txt => this.setState({ address_one: txt })}
      />
    );
  }

  renderAddressTwoField() {
    return (
      <TitleTxtFld
        value={this.state.address_two}
        title={strings.street_address_two}
        placeholder={strings.enter_address}
        onChangeTxt={txt => this.setState({ address_two: txt })}
      />
    );
  }

  renderCountryField() {
    return (
      <TitleTxtFld
        value={this.state.country}
        title={strings.country}
        placeholder={strings.select_country}
        onChangeTxt={txt => this.setState({ country: txt })}
        isSelectable
        rightImageSource={images_path.arrow_down}
        onPress={() => this.onPressSelectCountry()}
      />
    );
  }

  renderRegionField() {
    return (
      <TitleTxtFld
        value={this.state.region}
        title={strings.region}
        placeholder={strings.select_region}
        onChangeTxt={txt => this.setState({ region: txt })}
        isSelectable
        rightImageSource={images_path.arrow_down}
        onPress={() => this.onPressSelectRegion()}
      />
    );
  }


  renderStateField() {
    return (
      <TitleTxtFld
        value={this.state.state}
        title={strings.state}
        placeholder={strings.select_state}
        onChangeTxt={txt => this.setState({ state: txt })}
        isSelectable
        rightImageSource={images_path.arrow_down}
        onPress={() => this.onPressSelectState()}
      />
    );
  }

  renderCityField() {
    return (
      <TitleTxtFld
        value={this.state.city}
        title={strings.city}
        placeholder={strings.select_city}
        onChangeTxt={txt => this.setState({ city: txt })}
        isSelectable
        rightImageSource={images_path.arrow_down}
        onPress={() => this.onPressSelectCity()}
      />
    );
  }

  renderPinCodeField() {
    return (
      <TitleTxtFld
        value={this.state.pin_code}
        title={strings.pin_code}
        placeholder={strings.enter_pin_code}
        onChangeTxt={txt => this.setState({ pin_code: txt })}
        keyboardType='number-pad'
      />
    );
  }

  renderButton() {
    return (
      <CustomButton
        title={strings.done}
        isLoading={this.state.isLoading}
        buttonCustomStyle={{ paddingHorizontal: 20, marginTop: 50, }}
        onPress={() => this.tappedOnButton()}
      />
    );
  }

  onPressSelectCountry = () => {
    this.props.navigation.navigate("CountryListScreen", {
      returnData: this.refreshSelectedCountry.bind(this)
    })
  }

  refreshSelectedCountry(selectedCountry) {
    console.log("selectedCountry is >>> ", selectedCountry);
    this.setState({
      selectedCountry: selectedCountry,
      country: selectedCountry.country,
      selectedRegion: {},
      region: '',
      selectedState: {},
      state: '',
      selectedCity: {},
      city: ''
    })
  }

  onPressSelectRegion = () => {
    var selectedCountry = this.state.selectedCountry
    if (selectedCountry.id == undefined) {
      showMessageAlert(strings.please_select_country)
    } else {
      this.props.navigation.navigate("RegionListScreen", {
        countryId: selectedCountry.id,
        returnData: this.refreshSelectedRegion.bind(this)
      })
    }
  }

  refreshSelectedRegion(selectedRegion) {
    console.log("selectedRegion is >>> ", selectedRegion);
    this.setState({
      selectedRegion: selectedRegion,
      region: selectedRegion.region,
      selectedState: {},
      state: '',
      selectedCity: {},
      city: ''
    })
  }

  onPressSelectState = () => {
    var selectedRegion = this.state.selectedRegion
    if (selectedRegion.id == undefined) {
      showMessageAlert(strings.please_select_region)
    } else {
      this.props.navigation.navigate("StateListScreen", {
        regionId: selectedRegion.id,
        returnData: this.refreshSelectedState.bind(this)
      })
    }
  }

  refreshSelectedState(selectedState) {
    console.log("selectedState is >>> ", selectedState);
    this.setState({
      selectedState: selectedState,
      state: selectedState.state,
      selectedCity: {},
      city: ''
    })
  }

  onPressSelectCity = () => {
    var selectedState = this.state.selectedState
    if (selectedState.id == undefined) {
      showMessageAlert(strings.please_select_state)
    } else {
      this.props.navigation.navigate("CityListScreen", {
        stateId: selectedState.id,
        returnData: this.refreshSelectedCity.bind(this)
      })
    }
  }

  refreshSelectedCity(selectedCity) {
    console.log("selectedCity is >>> ", selectedCity);
    this.setState({
      selectedCity: selectedCity,
      city: selectedCity.city
    })
  }

  tappedOnButton() {

    if (this.state.isLoading) {
      return;
    }

    if (isEmpty(this.state.address_one) && isEmpty(this.state.address_two)) {
      showMessageAlert(strings.please_enter_address)
      return
    } else if (isEmpty(this.state.country)) {
      showMessageAlert(strings.please_select_country)
      return
    } else if (isEmpty(this.state.region)) {
      showMessageAlert(strings.please_select_region)
      return
    } else if (isEmpty(this.state.state)) {
      showMessageAlert(strings.please_select_state)
      return
    } else if (isEmpty(this.state.city)) {
      showMessageAlert(strings.please_select_city)
      return
    } else if (isEmpty(this.state.pin_code.toString())) {
      showMessageAlert(strings.please_enter_pin_code)
      return
    }

    this.setState({ isLoading: true })

    var storeAddress = {
      streetAddress1: this.state.address_one,
      streetAddress2: this.state.address_two,
      countryId: this.state.selectedCountry.id,
      regionId: this.state.selectedRegion.id,
      stateId: this.state.selectedState.id,
      city: this.state.selectedCity.id,
      pinCode: this.state.pin_code,
    }

    var branchObj = this.state.branchObj
    branchObj.storeAddress = storeAddress
    this.setState({ branchObj: branchObj }, () => {
      console.log('this.state.branchObj---------------------------', this.state.branchObj)
      if (this.state.isEdit) {
        this.callEditMerchant()
      } else {
        this.callCreateMerchant()
      }
    })
    return;
  }

  callCreateMerchant() {
    var params = JSON.stringify(this.state.branchObj);

    const apifetcherObj = postMethodAPI('merchantBranch/createMerchantBranch', params, this.state.token)
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log("Response ---->>>>\n ", data);

        if (statusCode == 201) {
          this.setState({ isLoading: false }, () => {
            this.props.navigation.goBack()
            this.props.navigation.goBack()
            this.props.navigation.goBack()
            this.props.navigation.goBack()
            DeviceEventEmitter.emit('AddEditBranch')
          })
        }
        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log("Error ---->>>> \n", error);
        this.setState({ isLoading: false });
        showMessageAlert(error)
      });
    return;
  }

  callEditMerchant() {
    var params = JSON.stringify(this.state.branchObj);

    const apifetcherObj = putMethodAPI('merchantBranch/updateMerchantBranch/' + this.state.branchId, params, this.state.token)
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log("Response ---->>>>\n ", data);

        if (statusCode == 200) {
          this.setState({ isLoading: false }, () => {
            this.props.navigation.goBack()
            this.props.navigation.goBack()
            this.props.navigation.goBack()
            this.props.navigation.goBack()
            DeviceEventEmitter.emit('AddEditBranch')
          })
        }
        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log("Error ---->>>> \n", error);
        this.setState({ isLoading: false });
        showMessageAlert(error)
      });
    return;
  }

}

const Styles = StyleSheet.create({
  container: {
    padding: 25,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25
  },


});
