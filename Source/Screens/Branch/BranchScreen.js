import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  Text,
  FlatList,
  ActivityIndicator,
  DeviceEventEmitter,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { MyColors } from '../../Theme';
import images_path from '@Images/Images';
import Global from '../../Utility/Global';
import strings from '../../Localization/string';
import { getMethodAPI, } from '../../API/APIClient';
import { showMessageAlert, isEmpty } from '../../Utility/Utility';
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class BranchScreen extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    isLoading: false,
    token: '',
    user: {},
    merchant: {},

    list: [],
  };

  componentDidMount() {
    Global.getCurrentToken().then(token => {
      Global.getCurrentUserPromise().then(user => {
        Global.getCurrentMerchantPromise().then(merchant => {
          this.setState({
            token: token,
            user: user,
            merchant: merchant,
          }, () => {
            console.log('user----------->>>>>>>', this.state.user)
            console.log('merchant------->>>>>>>', this.state.merchant)
            this.callBranchList()
          })
        })
      })
    })

    DeviceEventEmitter.addListener('AddEditBranch', () => {
      this.callBranchList()
    })

  }

  callBranchList = () => {
    this.setState({ isLoading: true })

    const apifetcherObj = getMethodAPI(
      'merchantBranch/getAllMerchantBranchsForApp/' + this.state.merchant.id, null, this.state.token
    )
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log('Success in API is >>> ', data)

        this.setState({ isLoading: false })

        if (statusCode == 200 && data.records != undefined) {
          this.setState({ list: data.records })
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log('Error in API is >>> ', error)
        this.setState({ isLoading: false })
      })
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: MyColors.whiteColor, }}>
          <CustomStatusBarTheme />

          <Image
            source={images_path.top_bg}
            style={{ width: '100%', position: 'absolute' }} />

          <ScrollView contentContainerStyle={{ flexGrow: 1, }} showsVerticalScrollIndicator={false} >
            <View style={{ flex: 1, }}>

              {this.renderHeader()}

              <View style={Styles.container}>

                {this.state.isLoading ? (
                  <ActivityIndicator
                    style={{ alignSelf: 'center' }}
                    color={MyColors.themeColor}
                    size='large'
                  />
                ) : (
                  <View>
                    {this.renderFilter()}

                    {this.state.list.length == 0 ?
                      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={images_path.branch_placeholder} />
                        <Text style={Styles.txtNoRecord}>{strings.you_can_manage_your_branches_here}</Text>
                      </View> :
                      <View>
                        {this.renderList()}
                      </View>
                    }
                  </View>
                )}

              </View>

            </View>
          </ScrollView>
      </SafeAreaView >
    );
  }

  renderHeader() {
    return (
      <View style={Styles.cellHeader}>
        <Text style={Styles.headerTitle}>{strings.manange_branches}</Text>
        <TouchableOpacity
          onPress={() => { this.props.navigation.toggleDrawer() }}
          style={{ position: 'absolute', right: 0 }}>
          <Image source={images_path.menu} />
        </TouchableOpacity>
      </View>
    );
  }

  renderFilter() {
    return (
      <View style={Styles.filterContainer}>
        <LinearGradient
          colors={['#19CB3F', '#83DA48']}
          style={{ borderRadius: 10, }}
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 1 }}>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('AddGeneralInfoScreen') }}
            style={{ padding: 10, flexDirection: 'row', alignItems: 'center' }}>
            <Text style={Styles.addTitle}>{strings.add_new}</Text>
            <Image source={images_path.user_plus} />
          </TouchableOpacity>
        </LinearGradient>

        <View style={{ flex: 1 }} />

        <TouchableOpacity style={Styles.searchContainer}
          onPress={() => { this.props.navigation.navigate('SearchBranchScreen') }}>
          <Image source={images_path.search} />
        </TouchableOpacity>

      </View>
    )
  }

  renderList() {
    return (
      <FlatList
        style={{ marginTop: 20 }}
        data={this.state.list}
        renderItem={this.renderItem}
      />
    )
  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => { }}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
          <Image style={{ width : 50, height: 50, borderRadius: 10, resizeMode: 'contain' }} source={{uri: this.state.merchant.businessLogo}} />
            <View style={{ flex: 1, marginStart: 5 }}>
              <Text style={Styles.txtBranchName}>{item.branchName}</Text>
              <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 12 }}>{item.managerName}</Text>
            </View>
          </View>

          <View style={{ alignItems: 'center', marginStart: 5 }}>
            <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12 }}>{item.branchBID}</Text>
            <LinearGradient
              colors={['#19CB3F', '#83DA48']}
              style={{ borderRadius: 10, marginTop: 20 }}
              start={{ x: 0, y: 1 }}
              end={{ x: 1, y: 1 }}>
              <TouchableOpacity onPress={() => { this.onPressEditItem(item) }}>
                <Text style={Styles.editTitleItem}>{strings.view_edit}</Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>
        </View>

        <View style={Styles.contactContainerItem}>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12 }}>{item.contactNumber}</Text>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12, marginStart: 20 }}>{item.streetAddress1}</Text>
        </View>

        <View style={Styles.vwSeparator} />
      </TouchableOpacity>
    )
  }

  onPressEditItem(item) {
    var branchObj = {}

    var shopTimingArray = item.shopTiming.split('-')
    var selectedType = { id: item.ownershipId, ownership: item.OwnerShipType  }

    var generalInfo = {
      storeName: item.storeName,
      branchName: item.branchName,
      OwnerShipType: item.OwnerShipType,
      websiteURL: item.websiteURL,
      websiteText: item.websiteText,
      start_time: shopTimingArray[0],
      end_time: shopTimingArray[1],
      selectedType: selectedType,
    }

    var taxInfo = {
      gstNumber: item.gstNumber,
      panNumber: item.panNumber,
      tinNumber: item.tinNumber,
      cinNumber: item.cinNumber,
    }

    var contactInfo = {
      feedbackEmail: item.feedbackEmail,
      customerCareEmail: item.customerCareEmail,
      whatsappOrderNumber: item.whatsappOrderNumber,
      instructionOnBill: item.instructionOnBill,
      contactNumber: item.contactNumber,
    }

    var selectedCountry = { id: item.countryId, country: item.Country  }
    var selectedRegion = { id: item.regionId, region: item.Region  }
    var selectedState = { id: item.stateId, state: item.State  }
    var selectedCity = { id: item.cityId, city: item.City  }

    var storeAddress = {
      streetAddress1: item.streetAddress1,
      streetAddress2: item.streetAddress2,
      country: item.Country,
      region: item.Region,
      state: item.State,
      city: item.City,
      pinCode: item.pinCode,
      selectedCountry: selectedCountry,
      selectedRegion: selectedRegion,
      selectedState: selectedState,
      selectedCity: selectedCity,
    }

    branchObj.generalInfo = generalInfo
    branchObj.taxInfo = taxInfo
    branchObj.contactInfo = contactInfo
    branchObj.storeAddress = storeAddress

    this.props.navigation.navigate('AddGeneralInfoScreen', {
      branchObj: branchObj,
      isEdit: true,
      branchId: item.id,
    })
  }

}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    backgroundColor: MyColors.whiteColor,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25
  },

  // ---------------------------Top
  cellHeader: {
    margin: 25,
  },
  headerTitle: {
    fontFamily: 'Sansation-Bold',
    fontSize: 26,
    marginTop: 80,
    color: MyColors.whiteColor,
  },

  filterContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 30,
  },
  addTitle: {
    color: MyColors.whiteColor,
    fontFamily: 'Roboto-Medium',
    marginEnd: 5,
    fontSize: 12
  },
  searchContainer: {
    borderRadius: 10,
    backgroundColor: '#F1F1F4',
    padding: 10,
    marginStart: 5,
  },

  txtNoRecord: {
    textAlign: 'center',
    fontFamily: 'Roboto-Medium',
    fontSize: 12,
    marginTop: 20,
  },

  editTitleItem: {
    color: MyColors.whiteColor,
    fontFamily: 'Roboto-Medium',
    fontSize: 14,
    padding: 10,
  },

  txtBranchName: {
    fontFamily: 'Roboto-Medium',
    fontSize: 14,
    color: '#2F80ED',
  },
  vwSeparator: {
    flex: 1,
    height: 1,
    backgroundColor: MyColors.seperatorGreyColor,
    marginVertical: 15
  },

  contactContainerItem: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  }

});
