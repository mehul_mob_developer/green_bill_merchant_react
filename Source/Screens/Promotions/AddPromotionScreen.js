import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Platform,
  Image,
  TouchableOpacity,
  SafeAreaView,
  DeviceEventEmitter,
  Text,
  Switch,
  BackHandler,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ImagePicker from "react-native-image-crop-picker";
import ImageResizer from "react-native-image-resizer";
import ActionSheet from 'react-native-actionsheet';
import { DatePickerModal } from 'react-native-paper-dates';
import moment from "moment";

import { MyColors } from '../../Theme';
import images_path from '@Images/Images';
import Global from '../../Utility/Global';
import strings from '../../Localization/string';
import { showMessageAlert, isEmpty } from '../../Utility/Utility';
import { postMethodAPI, putMethodAPI, postMethodUploadImageAPI } from '../../API/APIClient';

import TitleTxtFld from '../../customComponents/TitleTxtFld'
import CustomButton from '../../customComponents/CustomButton';
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class AddPromotionScreen extends Component {

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  state = {
    token: '',
    user: {},
    merchant: {},
    promotionObj: {},

    isLoading: false,

    profileImage: '',

    offer_title: '',
    sub_title: '',
    description: '',
    terms: '',
    start_date: '',
    end_date: '',
    date_duration: '',
    branch: '',
    selectedBranch: {},
    discount_type: '',
    discount: '',
    isTemplate: false,
    template_name: '',

    isPickDate: false,

    optionArray: [strings.camera, strings.photos, strings.cancel],
    optionDiscountTypeArray: [strings.flat, strings.percentage, strings.cancel],
  };

  componentDidMount() {
    if (this.props.navigation.state != null &&
      this.props.navigation.state.params != undefined &&
      this.props.navigation.state.params.promotionObj) {
      console.log('promotionObj', this.props.navigation.state.params.promotionObj)

      var start_date = moment(this.props.navigation.state.params.promotionObj.StartDate, 'YYYY-MM-DD')
      var end_date = moment(this.props.navigation.state.params.promotionObj.EndDate, 'YYYY-MM-DD')

      this.setState({
        promotionObj: this.props.navigation.state.params.promotionObj,

        offer_title: this.props.navigation.state.params.promotionObj.Title,
        sub_title: this.props.navigation.state.params.promotionObj.SubTitle,
        description: this.props.navigation.state.params.promotionObj.Description,
        terms: this.props.navigation.state.params.promotionObj.Terms,
        start_date: start_date,
        end_date: end_date,
        branch: this.props.navigation.state.params.promotionObj.BranchName,
        discount_type: this.props.navigation.state.params.promotionObj.DiscountType == strings.percentage ? strings.percentage : strings.flat,
        discount: Global.getFormatePriceWithCurrencySymbol(this.props.navigation.state.params.promotionObj.Discount, 2),
        isTemplate: this.props.navigation.state.params.promotionObj.IsTemplate == 1 ? true : false,
        template_name: this.props.navigation.state.params.promotionObj.TemplateName,

        selectedBranch: {
          id: this.props.navigation.state.params.promotionObj.BranchId,
          storeName: this.props.navigation.state.params.promotionObj.BranchName
        },
        date_duration: moment(start_date).format('DD-MM-YYYY') + ' - ' + moment(end_date).format('DD-MM-YYYY')
      })
    }

    Global.getCurrentToken().then(token => {
      Global.getCurrentUserPromise().then(user => {
        Global.getCurrentMerchantPromise().then(merchant => {
          this.setState({
            token: token,
            user: user,
            merchant: merchant,
          }, () => {
            console.log('user----------->>>>>>>', this.state.user)
            console.log('merchant------->>>>>>>', this.state.merchant)
          })
        })
      })
    })

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

    if (this.props.navigation.state != null && this.props.navigation.state.params != undefined) {
      this.setState(
        {
          data: this.props.navigation.state.params.data
        },
        () => console.log('data == > ', this.state.data)
      )
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  handleBackButtonClick() {
    this.props.navigation.goBack()
    return true;
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomStatusBarTheme />

        <Image
          source={images_path.top_bg}
          style={{ width: '100%', position: 'absolute' }} />

        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} >
          {/* <View style={{ flex: 1, }}> */}

          {this.renderProfile()}

          <View style={Styles.container}>

            {this.renderOfferTitleField()}
            {this.renderSubTitleField()}
            {this.renderDescriptionField()}
            {this.renderTermsField()}
            {this.renderDurationField()}
            {this.renderBranchField()}
            {this.renderDiscountTypeField()}
            {this.renderDiscountField()}
            {this.renderTemplet()}
            {this.renderTemplateNameField()}

            {this.renderButton()}

            {this.renderCalendar()}
            {this.renderActionSheet()}
            {this.renderDiscountTypeActionSheet()}
          </View>
          {/* </View> */}
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }

  renderProfile() {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <TouchableOpacity style={{ position: 'absolute' }}
          onPress={() => this.handleBackButtonClick()}>
          <Image source={images_path.back_white}
            style={Styles.imgBack}/>
        </TouchableOpacity>
        <View style={{ flex: 1, }}>
          <TouchableOpacity style={Styles.cellImage}
            onPress={() => this.ActionSheet.show()}>
            <Image
              // style={{
              //   width: isEmpty(this.state.profileImage) ? 33 : '100%',
              //   height: isEmpty(this.state.profileImage) ? 28 : '100%',
              //   borderRadius: isEmpty(this.state.profileImage) ? 0 : 15,
              //   resizeMode: 'cover',
              // }}
              style={{
                height: '100%', width: '100%',
                resizeMode: isEmpty(this.state.profileImage) ? 'center' : 'cover',
                borderRadius: isEmpty(this.state.profileImage) ? 0 : 15
              }}
              source={isEmpty(this.state.profileImage) ? images_path.camera : { uri: this.state.profileImage }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  renderOfferTitleField() {
    return (
      <TitleTxtFld
        value={this.state.offer_title}
        title={strings.offer_title}
        placeholder={strings.enter_offer_title}
        onChangeTxt={txt => this.setState({ offer_title: txt })}
      />
    );
  }

  renderSubTitleField() {
    return (
      <TitleTxtFld
        value={this.state.sub_title}
        title={strings.sub_title}
        placeholder={strings.enter_sub_title}
        onChangeTxt={txt => this.setState({ sub_title: txt })}
      />
    );
  }

  renderDescriptionField() {
    return (
      <TitleTxtFld
        value={this.state.description}
        title={strings.description}
        placeholder={strings.enter_description}
        onChangeTxt={txt => this.setState({ description: txt })}
      />
    );
  }

  renderTermsField() {
    return (
      <TitleTxtFld
        value={this.state.terms}
        title={strings.terms_and_conditions}
        placeholder={strings.enter_terms_and_conditions}
        onChangeTxt={txt => this.setState({ terms: txt })}
      />
    );
  }

  renderDurationField() {
    return (
      <TitleTxtFld
        value={this.state.date_duration}
        title={strings.select_date_duration}
        placeholder={strings.select_date_duration}
        onChangeTxt={txt => this.setState({ date_duration: txt })}
        isSelectable
        rightImageSource={images_path.calendar}
        onPress={() => { this.setState({ isPickDate: true }) }}
      />
    );
  }

  renderBranchField() {
    return (
      <TitleTxtFld
        value={this.state.branch}
        title={strings.select_branch}
        placeholder={strings.select_branch}
        onChangeTxt={txt => this.setState({ branch: txt })}
        isSelectable
        rightImageSource={images_path.arrow_down}
        onPress={() => this.onPressSelectBranch()}
      />
    );
  }

  renderDiscountTypeField() {
    return (
      <TitleTxtFld
        value={this.state.discount_type}
        title={strings.discount_type}
        placeholder={strings.select_discount_type}
        onChangeTxt={txt => this.setState({ discount_type: txt })}
        isSelectable
        rightImageSource={images_path.arrow_down}
        onPress={() => this.DiscountTypeActionSheet.show()}
      />
    );
  }

  renderDiscountField() {
    return (
      <TitleTxtFld
        value={this.state.discount}
        title={strings.discount}
        keyboardType="numeric"
        placeholder={strings.enter_discount}
        onChangeTxt={txt => this.setState({ discount: txt })}
      />
    );
  }

  renderTemplet() {
    return (
      <View>
        <Text style={Styles.txtTemplateTitle}>{strings.do_you_want_to_save_details_to_use_later}</Text>
        <View style={Styles.templateSubContainer}>
          <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14 }}>{strings.save_template}</Text>
          <Switch
            trackColor={{ false: "#9B9B9B", true: MyColors.themeColor }}
            thumbColor={this.state.isPush ? MyColors.whiteColor : "#D1CFD1"}
            ios_backgroundColor="#3e3e3e"
            onValueChange={() => { this.setState({ isTemplate: !this.state.isTemplate, }) }}
            value={this.state.isTemplate}
          />
        </View>
      </View>
    )
  }

  renderTemplateNameField() {
    return (
      <TitleTxtFld
        value={this.state.template_name}
        title={strings.template_name}
        placeholder={strings.enter_template_name}
        onChangeTxt={txt => this.setState({ template_name: txt })}
      />
    );
  }

  renderButton() {
    return (
      <CustomButton
        title={strings.done}
        isLoading={this.state.isLoading}
        buttonCustomStyle={{ marginTop: 20, paddingHorizontal: 20 }}
        onPress={() => this.tappedOnButton()}
      />
    );
  }

  renderActionSheet = () => {
    return (
      <ActionSheet
        ref={o => (this.ActionSheet = o)}
        //Title of the Bottom Sheet
        title={strings.which_one_do_you_like}
        //Options Array to show in bottom sheet
        options={this.state.optionArray}
        //Define cancel button index in the option array
        //this will take the cancel option in bottom and will highlight it
        cancelButtonIndex={2}
        //If you want to highlight any specific option you can use below prop
        destructiveButtonIndex={2}
        onPress={index => {
          if (index === 0) {
            this.openCamera();
          } else if (index === 1) {
            this.openPhotos();
          }
        }}
      />
    );
  }

  openCamera() {
    ImagePicker.openCamera({
      width: 400,
      height: 400,
      quality: 0.4,
      cropping: true,
      // showCropFrame: true,
      // multiple: true,
      mediaType: 'photo',
      maxFiles: 10000,
    }).then(image => {
      ImageResizer.createResizedImage(image.path, 400, 400, "JPEG", 80, 0).then(
        uri => {
          var path = uri.uri
          if (Platform.OS == "ios") {
            path = uri.path
          }
          this.setState({ profileImage: path })
        }
      );
    });
  }

  openPhotos() {
    ImagePicker.openPicker({
      width: 400,
      height: 400,
      quality: 0.4,
      cropping: true,
      // showCropFrame: true,
      mediaType: 'photo',
      maxFiles: 10000,
    }).then(image => {
      ImageResizer.createResizedImage(image.path, 400, 400, "JPEG", 80, 0).then(
        uri => {
          var path = uri.uri
          if (Platform.OS == "ios") {
            path = uri.path
          }
          this.setState({ profileImage: path })
        }
      );
    });
  }

  renderCalendar() {
    return (
      <DatePickerModal
        // locale={'en'} optional, default: automatic
        mode="range"
        visible={this.state.isPickDate}
        date={new Date()}
        onDismiss={this._hideDateCancel}
        onConfirm={this._handleDateConfirm}
        validRange={{
          startDate: new Date(),  // optional
          // endDate: new Date(), // optional
        }}
      // onChange={} // same props as onConfirm but triggered without confirmed by user
      // saveLabel="Save" // optional
      // label="Select date" // optional
      // animationType="slide" // optional, default is 'slide' on ios/android and 'none' on web
      />
    )
  }

  _hideDateCancel = () => this.setState({ isPickDate: false });

  _handleDateConfirm = ({ startDate, endDate }) => {
    console.log('A date has been picked: ', startDate + '' + endDate);
    this._hideDateCancel();

    var date_duration = moment(startDate).format('DD-MM-YYYY') + ' - ' + moment(endDate).format('DD-MM-YYYY')

    this.setState({
      start_date: moment(startDate),
      end_date: moment(endDate),
      date_duration: date_duration,
    });
  }

  renderDiscountTypeActionSheet = () => {
    return (
      <ActionSheet
        ref={o => (this.DiscountTypeActionSheet = o)}
        //Title of the Bottom Sheet
        title={strings.which_one_do_you_like}
        //Options Array to show in bottom sheet
        options={this.state.optionDiscountTypeArray}
        //Define cancel button index in the option array
        //this will take the cancel option in bottom and will highlight it
        cancelButtonIndex={2}
        //If you want to highlight any specific option you can use below prop
        destructiveButtonIndex={2}
        onPress={index => {
          if (index === 0) {
            this.setState({ discount_type: strings.flat })
          } else if (index === 1) {
            this.setState({ discount_type: strings.percentage })
          }
        }}
      />
    );
  }

  onPressSelectBranch = () => {
    this.props.navigation.navigate("SelectBranchListScreen", {
      returnData: this.refreshSelectedBranch.bind(this)
    })
  }

  refreshSelectedBranch(selectedBranch) {
    console.log("selectedBranch is >>> ", selectedBranch);
    this.setState({
      selectedBranch: selectedBranch,
      branch: selectedBranch.storeName
    })
  }

  tappedOnButton() {

    if (this.state.isLoading) {
      return;
    }

    if (isEmpty(this.state.offer_title)) {
      showMessageAlert(strings.please_enter_offer_title)
      return
    } else if (isEmpty(this.state.sub_title)) {
      showMessageAlert(strings.please_enter_sub_title)
      return
    } else if (isEmpty(this.state.terms)) {
      showMessageAlert(strings.please_enter_terms_and_conditions)
      return
    } else if (isEmpty(this.state.date_duration)) {
      showMessageAlert(strings.please_select_date_duration)
      return
    } else if (isEmpty(this.state.branch)) {
      showMessageAlert(strings.please_select_branch)
      return
      // } else if (isEmpty(this.state.discount_type)) {
      //   showMessageAlert(strings.please_select_discount_type)
      //   return
    } else if (isEmpty(this.state.discount)) {
      showMessageAlert(strings.please_enter_discount)
      return
    }

    this.setState({ isLoading: true })

    if (this.state.profileImage == '') {
      this.callCreatePromotionOffer('')
    } else {
      this.callUploadPic()
    }

    return;
  }

  callUploadPic = () => {
    var formdata = new FormData();
    let photo = {
      uri: this.state.profileImage,
      type: "image/jpeg",
      name: "image.jpg"
    };

    formdata.append("promotionImage", photo);

    const apifetcherObj = postMethodUploadImageAPI('promotionOffer/uploadPromotionImage', formdata, this.state.token)
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log("Response >>>", data);

        this.callCreatePromotionOffer(data.PromotionImage.url)
        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log("Error >>>", error);
        this.setState({ isLoading: false });
        showMessageAlert(error)
      });
    return;
  }

  callCreatePromotionOffer(imageUrl) {

    var params = JSON.stringify({
      status: '1',
      merchantId: this.state.merchant.id,
      title: this.state.offer_title,
      subTitle: this.state.sub_title,
      description: this.state.description,
      startDate: moment(this.state.start_date).format('YYYY-MM-DD'),
      endDate: moment(this.state.end_date).format('YYYY-MM-DD'),
      merchantBranchId: this.state.selectedBranch.id,
      discountTypeId: this.state.discount_type == strings.percentage ? 2 : 1,
      discount: this.state.discount,
      terms: this.state.terms,
      isTemplate: this.state.isTemplate ? 1 : 0,
      templateName: this.state.template_name,
      image: imageUrl,
    });

    if (this.state.promotionObj.OfferId != undefined) {
      const apifetcherObj = putMethodAPI('promotionOffer/updatePromotionOffer/' + this.state.promotionObj.OfferId, params, this.state.token)
      apifetcherObj
        .then(response => { return Promise.all([response.status, response.json()]) })
        .then(res => {
          let statusCode = res[0]
          let data = res[1]
          console.log("Response ---->>>>\n ", data);

          this.setState({ isLoading: false })

          if (statusCode == 200) {
            this.props.navigation.goBack()
            DeviceEventEmitter.emit('AddEditPromotion')
          }

          if (data.error != undefined) {
            showMessageAlert(data.error)
          }
        })
        .catch(error => {
          console.log("Error ---->>>> \n", error);
          this.setState({ isLoading: false });
          showMessageAlert(error)
        });
    } else {
      const apifetcherObj = postMethodAPI('promotionOffer/createPromotionOffer', params, this.state.token)
      apifetcherObj
        .then(response => { return Promise.all([response.status, response.json()]) })
        .then(res => {
          let statusCode = res[0]
          let data = res[1]
          console.log("Response ---->>>>\n ", data);

          this.setState({ isLoading: false })

          if (statusCode == 201) {
            this.props.navigation.goBack()
            DeviceEventEmitter.emit('AddEditPromotion')
          }

          if (data.error != undefined) {
            showMessageAlert(data.error)
          }
        })
        .catch(error => {
          console.log("Error ---->>>> \n", error);
          this.setState({ isLoading: false });
          showMessageAlert(error)
        });
    }
    return;
  }

}

const Styles = StyleSheet.create({
  container: {
    padding: 25,
    backgroundColor: MyColors.whiteColor,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25
  },

  // ---------------------------Profile
  cellImage: {
    width: 110,
    height: 110,
    margin: 20,
    borderWidth: 5,
    borderRadius: 20,
    borderColor: MyColors.whiteColor,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },

  imgBack: {
    resizeMode: 'center',
    margin: 25
  },

  txtTemplateTitle: {
    fontFamily: 'Roboto-Regular',
    fontSize: 14,
    color: '#5A607B',
    marginTop: 20,
  },

  templateSubContainer: {
    flex: 1,
    marginTop: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  }

});
