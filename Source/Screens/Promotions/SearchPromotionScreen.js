import React, { Component } from 'react'
import {
  View,
  TouchableOpacity,
  FlatList,
  Text,
  StyleSheet,
  TextInput,
  ActivityIndicator,
  Image,
  DeviceEventEmitter,
  SafeAreaView,
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient';

import { MyColors } from '../../Theme'
import Global from '../../Utility/Global';
import strings from '../../Localization/string'
import { getMethodAPI } from '../../API/APIClient';
import { isEmpty, showMessageAlert } from '../../Utility/Utility'
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class SearchPromotionScreen extends Component {

  constructor() {
    super()
    this.state = {
      isLoading: true,
      token: '',
      user: {},
      merchant: {},
      branch: {},

      isOfferName: false,
      isBranchName: false,
      isStatus: false,

      keyword: '',
      list: [],
    }
  }

  componentDidMount() {
    Global.getCurrentToken().then(token => {
      Global.getCurrentUserPromise().then(user => {
        Global.getCurrentMerchantPromise().then(merchant => {
          this.setState({
            token: token,
            user: user,
            merchant: merchant,
          }, () => {
            console.log('user----------->>>>>>>', this.state.user)
            console.log('merchant------->>>>>>>', this.state.merchant)
            this.callPromotionList()
          })
        })
      })
    })

    Global.getCurrentBranchPromise().then(branch => {
      this.setState({branch: branch}, () => {
        console.log('branch----------->>>>>>>', this.state.branch)
      })
    })

    DeviceEventEmitter.addListener('AddEditPromotion', () => {
      this.callPromotionList()
    })
  }

  callPromotionList = () => {
    this.setState({ isLoading: true })

    // var searchKey =
    //   this.state.isOfferName ? '&offerName=' :
    //     this.state.isBranchName ? '&merchantBranchId=' :
    //       this.state.isStatus ? '&status=' :
    //       '&searchedValue='

    var branchId = this.state.branch.id
    if(!this.state.isBranchName || branchId == undefined){
      branchId = 0
    }

    var status = this.state.isStatus ? 1 : 0

    const apifetcherObj = getMethodAPI(
      // 'promotionOffer/getPromotionOffer/' + this.state.merchant.id + '?' + searchKey + this.state.keyword,
      'promotionOffer/getPromotionOffer/' + this.state.merchant.id + '?' + 
      '&offerName=' + this.state.isOfferName +
      '&merchantBranchId=' + branchId  +
      '&status=' + status +
      '&searchedValue=' + this.state.keyword,
      null,
      this.state.token
    )
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log('Success in API is >>> ', data)

        this.setState({ isLoading: false })

        if (statusCode == 200) {
          this.setState({ list: data })
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log('Error in API is >>> ', error)
        this.setState({ isLoading: false })
      })
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: MyColors.whiteColor, }}>
        <CustomStatusBarTheme />

        <View style={Styles.container}>
          {this.renderSearch()}
          {this.renderFilter()}

          <View style={{ flex: 1 }} >
            {this.state.isLoading ? (
              <ActivityIndicator
                style={{ alignSelf: 'center' }}
                color={MyColors.themeColor}
                size='large'
              />
            ) : (
              this.state.list.length == 0 || this.state.list.length == undefined ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text>{strings.no_record_found}</Text>
                </View> :
                <View style={{ flex: 1 }}>
                  {this.renderList()}
                </View>
            )}
          </View>
        </View>
      </SafeAreaView>
    )
  }

  renderSearch() {
    return (
      <View style={Styles.searchContainer}>
        <TextInput
          style={{ padding: 10 }}
          clearButtonMode='always'
          returnKeyType='search'
          placeholder={strings.search}
          value={this.state.keyword}
          onChangeText={txt => { this.setState({ keyword: txt }) }}
          onSubmitEditing={(event) => { this.onChangeTextSearch() }}
        />
      </View>
    )
  }

  onChangeTextSearch = (key, value) => {
    console.log('onChangeText', key, value);
    this.setState({
      [key]: value,
      next_offset: 0,
      list: [],
    }, () => { this.callPromotionList() });
  };

  renderFilter() {
    return (
      <View style={{ marginVertical: 10 }}>
        <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14 }}>{strings.search_by}</Text>
        <View style={{ flexDirection: 'row', marginTop: 10, }}>
          <TouchableOpacity style={[Styles.txtTagContainer,
          { backgroundColor: this.state.isOfferName ? MyColors.themeColor : null }]}
            onPress={() => {this.setState({isOfferName: !this.state.isOfferName})}}>
            <Text style={[Styles.txtTag, { color: this.state.isOfferName ? MyColors.whiteColor : '#07321A' }]}>{strings.offer_name}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[Styles.txtTagContainer,
          { backgroundColor: this.state.isBranchName ? MyColors.themeColor : null, marginHorizontal: 5 }]}
            onPress={() => {this.setState({isBranchName: !this.state.isBranchName})}}>
            <Text style={[Styles.txtTag, { color: this.state.isBranchName ? MyColors.whiteColor : '#07321A' }]}>{strings.branch_name}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[Styles.txtTagContainer,
          { backgroundColor: this.state.isStatus ? MyColors.themeColor : null }]}
            onPress={() => {this.setState({isStatus: !this.state.isStatus})}}>
            <Text style={[Styles.txtTag, { color: this.state.isStatus ? MyColors.whiteColor : '#07321A' }]}>{strings.status}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderList() {
    return (
      <FlatList
        style={{ marginTop: 20 }}
        data={this.state.list}
        renderItem={this.renderItem}
        showsVerticalScrollIndicator={false}
      />
    )
  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => { }}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
          <Image style={{ width : 50, height: 50, borderRadius: 10, resizeMode: 'contain' }} source={{uri: item.Image}} />
            <View style={{ flex: 1, marginStart: 5 }}>
              <Text style={Styles.txtName}>{item.Title}</Text>
              <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 12 }}>{item.StoreName}</Text>
            </View>
          </View>

          <View style={{ alignItems: 'center', marginStart: 5 }}>
            <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12 }}>{item.Status == 1 ? strings.active : strings.inactive}</Text>
            <TouchableOpacity
              onPress={() => { this.props.navigation.navigate('AddPromotionScreen', { promotionObj: item }) }}>
              <LinearGradient
                colors={['#19CB3F', '#83DA48']}
                style={{ borderRadius: 10, marginTop: 20 }}
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}>
                <Text style={{ color: MyColors.whiteColor, fontFamily: 'Roboto-Medium', fontSize: 14, padding: 10 }}>{strings.view_edit}</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>

        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginTop: 5, justifyContent: 'space-between' }}>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12 }}>{item.DiscountType}</Text>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12, marginStart: 5 }}>{Global.getFormatePriceWithCurrencySymbol(item.Discount, 2)}</Text>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12, marginStart: 5 }}>{'Validity: ' + item.Validity + ' Days'}</Text>
        </View>

        <View style={Styles.vwSeparator} />
      </TouchableOpacity>
    )
  }

}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: MyColors.whiteColor,
    padding: 15
  },
  searchContainer: {
    justifyContent: 'center',
    borderRadius: 10,
    marginVertical: 10,
    backgroundColor: '#F1F1F4'
  },

  txtTagContainer: {
    borderWidth: 1,
    borderRadius: 10,
    borderColor: MyColors.themeColor,
  },
  txtTag: {
    fontFamily: 'Roboto-Regular',
    fontSize: 16,
    padding: 8,
  },

  txtName: {
    fontFamily: 'Roboto-Medium',
    fontSize: 14,
    color: '#07321A',
  },
  vwSeparator: {
    flex: 1,
    height: 1,
    backgroundColor: MyColors.seperatorGreyColor,
    marginVertical: 15
  },

})
