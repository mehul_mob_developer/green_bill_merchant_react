import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  Text,
  FlatList,
  ActivityIndicator,
  DeviceEventEmitter,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { MyColors } from '../../Theme';
import images_path from '@Images/Images';
import Global from '../../Utility/Global';
import strings from '../../Localization/string';
import { getMethodAPI } from '../../API/APIClient';
import { showMessageAlert, isEmpty } from '../../Utility/Utility';
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class PromotionsScreen extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    isLoading: false,
    token: '',
    user: {},
    merchant: {},

    list: [],
  };

  componentDidMount() {
    Global.getCurrentToken().then(token => {
      Global.getCurrentUserPromise().then(user => {
        Global.getCurrentMerchantPromise().then(merchant => {
          this.setState({
            token: token,
            user: user,
            merchant: merchant,
          }, () => {
            console.log('user----------->>>>>>>', this.state.user)
            console.log('merchant------->>>>>>>', this.state.merchant)
            this.callPromotionList()
          })
        })
      })
    })

    DeviceEventEmitter.addListener('AddEditPromotion', () => {
      this.callPromotionList()
    })

  }

  callPromotionList = () => {
    this.setState({ isLoading: true })

    const apifetcherObj = getMethodAPI(
      'promotionOffer/getPromotionOffer/' + this.state.merchant.id, null, this.state.token
    )
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log('Success in API is >>> ', data)

        this.setState({ isLoading: false })

        if (statusCode == 200) {
          this.setState({ list: data })
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log('Error in API is >>> ', error)
        this.setState({ isLoading: false })
      })
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: MyColors.whiteColor, }}>
          <CustomStatusBarTheme />

          <Image source={images_path.top_bg}
            style={{ width: '100%', position: 'absolute' }} />

          <ScrollView contentContainerStyle={{ flexGrow: 1, }} showsVerticalScrollIndicator={false} >
            <View style={{ flex: 1, }}>

              {this.renderHeader()}

              <View style={Styles.container}>

                {this.state.isLoading ? (
                  <ActivityIndicator
                    style={{ alignSelf: 'center' }}
                    color={MyColors.themeColor}
                    size='large'
                  />
                ) : (
                  <View>
                    {this.renderFilter()}

                    {this.state.list.length == undefined || this.state.list.length == 0 ?
                      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={images_path.promotions_placeholder} />
                        <Text style={Styles.txtNoRecord}>{strings.create_and_manage_promotions_and_offers_for_customers}</Text>
                      </View> :
                      <View>
                        {this.renderList()}
                      </View>
                    }

                  </View>
                )}

              </View>

            </View>
          </ScrollView>
      </SafeAreaView >
    );
  }

  renderHeader() {
    return (
      <View style={Styles.cellHeader}>
        <Text style={{ fontFamily: 'Sansation-Bold', fontSize: 26, marginTop: 80, color: MyColors.whiteColor }}>{strings.manage_promotions}</Text>
        <TouchableOpacity
          onPress={() => { this.props.navigation.toggleDrawer() }}
          style={{ position: 'absolute', right: 0 }}>
          <Image source={images_path.menu} />
        </TouchableOpacity>
      </View>
    );
  }

  renderFilter() {
    return (
      <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginTop: 30, }}>
        <LinearGradient
          colors={['#19CB3F', '#83DA48']}
          style={{ borderRadius: 10, }}
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 1 }}>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('AddPromotionScreen') }}
            style={{ padding: 10, flexDirection: 'row', alignItems: 'center' }}>
            <Text style={{ color: MyColors.whiteColor, fontFamily: 'Roboto-Medium', marginEnd: 5, fontSize: 12 }}>{strings.add_new}</Text>
            <Image source={images_path.gift} />
          </TouchableOpacity>
        </LinearGradient>

        <View style={{ flex: 1 }} />

        <TouchableOpacity style={{ borderRadius: 10, backgroundColor: '#F1F1F4', padding: 10, marginStart: 5 }}
          onPress={() => { this.props.navigation.navigate('SearchPromotionScreen') }}>
          <Image source={images_path.search} />
        </TouchableOpacity>

      </View>
    )
  }

  renderList() {
    return (
      <FlatList
        style={{ marginTop: 20 }}
        data={this.state.list}
        renderItem={this.renderItem}
      />
    )
  }

  renderItem = ({ item }) => {
    console.log('item.isTemplate---', item.IsTemplate)
    return (
      <TouchableOpacity onPress={() => { }}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Image style={{ width : 50, height: 50, borderRadius: 10, resizeMode: 'contain' }} source={{uri: item.Image}} />
            <View style={{ flex: 1, marginStart: 5 }}>
              <Text style={Styles.txtName}>{item.Title}</Text>
              <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 12 }}>{item.StoreName}</Text>
            </View>
          </View>

          <View style={{ alignItems: 'center', marginStart: 5 }}>
            <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12 }}>{item.Status == 1 ? strings.active : strings.inactive}</Text>
            <TouchableOpacity
              onPress={() => { this.props.navigation.navigate('AddPromotionScreen', { promotionObj: item }) }}>
              <LinearGradient
                colors={['#19CB3F', '#83DA48']}
                style={{ borderRadius: 10, marginTop: 20 }}
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}>
                <Text style={{ color: MyColors.whiteColor, fontFamily: 'Roboto-Medium', fontSize: 14, padding: 10 }}>{strings.view_edit}</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>

        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginTop: 5, justifyContent: 'space-between' }}>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12 }}>{item.DiscountType}</Text>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12, marginStart: 5 }}>{Global.getFormatePriceWithCurrencySymbol(item.Discount, 2)}</Text>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12, marginStart: 5 }}>{'Validity: ' + item.Validity + ' Days'}</Text>
        </View>

        <View style={Styles.vwSeparator} />
      </TouchableOpacity>
    )
  }

}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    backgroundColor: MyColors.whiteColor,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25
  },

  // ---------------------------Top
  cellHeader: {
    margin: 25,
  },

  txtNoRecord: {
    textAlign: 'center',
    fontFamily: 'Roboto-Medium',
    fontSize: 12,
    marginTop: 20
  },

  txtName: {
    fontFamily: 'Roboto-Medium',
    fontSize: 14,
    color: '#07321A',
  },
  vwSeparator: {
    flex: 1,
    height: 1,
    backgroundColor: MyColors.seperatorGreyColor,
    marginVertical: 15
  },

});
