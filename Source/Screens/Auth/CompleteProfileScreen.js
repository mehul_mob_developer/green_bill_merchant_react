import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Platform,
  Image,
  TouchableOpacity,
  SafeAreaView,
  TextInput,
  Text,
  Dimensions,
  BackHandler,
  DeviceEventEmitter,
} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import ImagePicker from "react-native-image-crop-picker";
import ImageResizer from "react-native-image-resizer";
import ActionSheet from 'react-native-actionsheet';
import CryptoJS from "react-native-crypto-js"

import { MyColors } from '../../Theme';
import images_path from '@Images/Images';
import Global from '../../Utility/Global';
import strings from '../../Localization/string';

import TitleTxtFld from '../../customComponents/TitleTxtFld'
import CustomButton from '../../customComponents/CustomButton'
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'
import { showMessageAlert, isEmpty, isValidEmail } from '../../Utility/Utility'
import { putMethodAPI, postMethodAPI, postMethodUploadImageAPI } from '../../API/APIClient'


export default class CompleteProfileScreen extends Component {

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  state = {
    token: '',
    user: {},
    merchant: {},

    isLoading: false,
    isEdit: false,

    profileImage: '',

    // mobile: '9922277713',
    // adminName: 'Mehul',
    // organizationName: 'Organization',
    // email: 'mehul@yopmail.com',
    mobile: '',
    adminName: '',
    organizationName: '',
    email: '',
    category: '',
    selectedCategory: {},
    subCategory: '',
    selectedSubCategory: {},
    country: '',
    selectedCountry: {},
    region: '',
    selectedRegion: {},
    state: '',
    selectedState: {},
    city: '',
    selectedCity: {},
    location: '',
    pin: '',

    optionArray: [strings.camera, strings.photos, strings.cancel],
  };

  componentDidMount() {
    if (this.props.navigation.state != null && this.props.navigation.state.params != undefined) {
      this.setState({ isEdit: this.props.navigation.state.params.isEdit }, () => {
        console.log('isEdit--------------------------------------', this.state.isEdit)
        if (this.state.isEdit) {
          Global.getCurrentMerchantPromise().then(merchant => {
            this.setState({
              merchant: merchant,
              organizationName: merchant.organizationName,
              selectedCategory: merchant.selectedCategory,
              selectedSubCategory: merchant.selectedSubCategory,
              selectedCountry: merchant.selectedCountry,
              selectedRegion: merchant.selectedRegion,
              selectedState: merchant.selectedState,
              selectedCity: merchant.selectedCity,
              pin: merchant.pin,
              category: merchant.category,
              subCategory: merchant.subCategory,
              country: merchant.country,
              region: merchant.region,
              state: merchant.state,
              city: merchant.city,
            }, () => {
              console.log('merchant------->>>>>>>', this.state.merchant)
            })
          })
        }
      })
    }

    Global.getCurrentToken().then(token => {
      this.setState({ token: token })
    })

    Global.getCurrentUserPromise().then(user => {
      this.setState({
        user: user,
        mobile: user.mobile.toString()
      }, () => {
        console.log('user------->>>>>>>', this.state.user)
        if (this.state.isEdit) {
          var user = this.state.user
          this.setState({
            adminName: user.name,
            email: user.email,
            location: user.moduleAccess,
          })
        }
      })
    })

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  handleBackButtonClick() {
    this.props.navigation.goBack()
    return true;
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomStatusBarTheme />
        <Image
          source={images_path.top_bg}
          style={{ width: '100%', position: 'absolute' }} />
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: MyColors.white }}>
          {this.renderProfile()}
          <View style={Styles.container}>
            {this.renderMobileField()}
            {this.renderAdminNameField()}
            {this.renderOrganizationNameField()}
            {this.renderEmailField()}
            {this.renderCategoryField()}
            {this.renderSubCategoryField()}
            {this.renderCountryField()}
            {this.renderRegionField()}
            {this.renderStateField()}
            {this.renderCityField()}
            {this.renderLocationField()}
            {!this.state.isEdit ? this.renderPinField() : null}
            {this.renderButton()}
            {this.state.isEdit ? this.renderChangePin() : null}
            {this.renderActionSheet()}
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }

  renderProfile() {
    return (
      <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>

        <TouchableOpacity style={{ position: 'absolute' }}
          onPress={() => this.handleBackButtonClick()}>
          <Image source={images_path.back_white}
            style={Styles.imgBack}/>
        </TouchableOpacity>

        <View style={{ flex: 1 }}>
          <TouchableOpacity style={Styles.cellImage}
            onPress={() => this.ActionSheet.show()}>
            <Image
              // style={{
              //   width: isEmpty(this.state.profileImage) ? 33 : '100%',
              //   height: isEmpty(this.state.profileImage) ? 28 : '100%',
              //   borderRadius: isEmpty(this.state.profileImage) ? 0 : 15,
              //   resizeMode: 'cover',
              // }}
              style={{
                height: '100%', width: '100%',
                resizeMode: isEmpty(this.state.profileImage) ? 'center' : 'cover',
                borderRadius: isEmpty(this.state.profileImage) ? 0 : 15
              }}
              source={this.state.profileImage != '' ? { uri: this.state.profileImage } :
               !isEmpty(this.state.user.Profile_url) ? { uri: this.state.user.Profile_url } : images_path.camera }
            />
            {this.state.isEdit ?
              <View style={Styles.imgEditIcon}>
                <Image source={images_path.camera_green} />
              </View> : null
            }
          </TouchableOpacity>
        </View>

      </View>
    );
  }

  renderMobileField() {
    return (
      <View>
        <Text style={Styles.txtLabel}>{strings.mobile_number}</Text>

        <View style={Styles.mobileContainer}>
          <Image
            source={images_path.india}
            style={Styles.flagImage}
          />

          <Text style={Styles.txtCountryNumber} >+91</Text>

          <View style={{ width: 1, height: '70%', backgroundColor: '#5A607B' }} />

          <TextInput
            autoCompleteType="tel"
            autoCorrect={false}
            keyboardType="numeric"
            placeholder={strings.enter_mobile_number}
            value={this.state.mobile}
            numberOfLines={1}
            autoFocus={true}
            onChangeTxt={txt => this.setState({ mobile: txt })}
            style={Styles.textInput}
            editable={false}
          />
        </View>
      </View>
    );
  }

  renderAdminNameField() {
    return (
      <TitleTxtFld
        value={this.state.adminName}
        title={strings.merchant_admin_name}
        placeholder={strings.enter_merchant_admin_name}
        onChangeTxt={txt => this.setState({ adminName: txt })}
      />
    );
  }

  renderOrganizationNameField() {
    return (
      <TitleTxtFld
        value={this.state.organizationName}
        title={strings.organisation_name}
        placeholder={strings.enter_organisation_name}
        onChangeTxt={txt => this.setState({ organizationName: txt })}
      />
    );
  }

  renderEmailField() {
    return (
      <TitleTxtFld
        keyboardType='email-address'
        value={this.state.email}
        title={strings.email_id}
        placeholder={strings.enter_email_id}
        onChangeTxt={txt => this.setState({ email: txt })}
      />
    );
  }

  renderCategoryField() {
    return (
      <TitleTxtFld
        value={this.state.category}
        title={strings.business_category}
        placeholder={strings.select_business_category}
        onChangeTxt={txt => this.setState({ category: txt })}
        isSelectable
        rightImageSource={images_path.arrow_down}
        onPress={() => this.onPressSelectCategory()}
      />
    );
  }

  renderSubCategoryField() {
    return (
      <TitleTxtFld
        value={this.state.subCategory}
        title={strings.business_sub_category}
        placeholder={strings.select_business_sub_category}
        onChangeTxt={txt => this.setState({ subCategory: txt })}
        isSelectable
        rightImageSource={images_path.arrow_down}
        onPress={() => this.onPressSelectSubCategory()}
      />
    );
  }

  renderCountryField() {
    return (
      <TitleTxtFld
        value={this.state.country}
        title={strings.country}
        placeholder={strings.select_country}
        onChangeTxt={txt => this.setState({ country: txt })}
        isSelectable
        rightImageSource={images_path.arrow_down}
        onPress={() => this.onPressSelectCountry()}
      />
    );
  }

  renderRegionField() {
    return (
      <TitleTxtFld
        value={this.state.region}
        title={strings.region}
        placeholder={strings.select_region}
        onChangeTxt={txt => this.setState({ region: txt })}
        isSelectable
        rightImageSource={images_path.arrow_down}
        onPress={() => this.onPressSelectRegion()}
      />
    );
  }

  renderStateField() {
    return (
      <TitleTxtFld
        value={this.state.state}
        title={strings.state}
        placeholder={strings.select_state}
        onChangeTxt={txt => this.setState({ state: txt })}
        isSelectable
        rightImageSource={images_path.arrow_down}
        onPress={() => this.onPressSelectState()}
      />
    );
  }

  renderCityField() {
    return (
      <TitleTxtFld
        value={this.state.city}
        title={strings.city}
        placeholder={strings.select_city}
        onChangeTxt={txt => this.setState({ city: txt })}
        isSelectable
        rightImageSource={images_path.arrow_down}
        onPress={() => this.onPressSelectCity()}
      />
    );
  }

  renderLocationField() {
    return (
      <TitleTxtFld
        value={this.state.location}
        title={strings.location}
        placeholder={strings.enter_location}
        onChangeTxt={txt => this.setState({ location: txt })}
      />
    );
  }

  renderPinField() {
    return (
      <View>
        <Text style={Styles.txtLabel}>{strings.pin}</Text>

        <OTPInputView
          style={{ width: '100%', height: 50, marginTop: 15 }}
          pinCount={6}
          code={this.state.pin} // You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
          onCodeChanged={pin => { this.setState({ pin }) }}
          autoFocusOnLoad={false}
          codeInputFieldStyle={Styles.inputItem}
          // codeInputHighlightStyle={{
          //   borderColor: "#03DAC6",
          // }}
          onCodeFilled={code => {
            if (code.length == 6) {
            }
            // console.log(`Code is ${code}, you are good to go!`)
          }}
        />
      </View>
    )
  }

  renderButton() {
    return (
      <CustomButton
        title={strings.continue}
        isLoading={this.state.isLoading}
        buttonCustomStyle={{ marginTop: 20, paddingHorizontal: 20 }}
        onPress={() => this.tappedOnButton()}
      />
    );
  }

  renderChangePin() {
    return (
      <TouchableOpacity
        style={{ alignSelf: 'center', margin: 20 }}
        onPress={() => { this.props.navigation.navigate('ChangePinScreen') }}
      >
        <Text style={{
          fontFamily: 'Roboto-Medium',
          fontSize: 14,
          color: MyColors.themeColor,
          textDecorationLine: 'underline',
        }} >{strings.change_pin}</Text>
      </TouchableOpacity>
    )
  }

  renderActionSheet = () => {
    return (
      <ActionSheet
        ref={o => (this.ActionSheet = o)}
        //Title of the Bottom Sheet
        title={strings.which_one_do_you_like}
        //Options Array to show in bottom sheet
        options={this.state.optionArray}
        //Define cancel button index in the option array
        //this will take the cancel option in bottom and will highlight it
        cancelButtonIndex={2}
        //If you want to highlight any specific option you can use below prop
        destructiveButtonIndex={2}
        onPress={index => {
          if (index === 0) {
            this.openCamera();
          } else if (index === 1) {
            this.openPhotos();
          }
        }}
      />
    );
  }

  openCamera() {
    ImagePicker.openCamera({
      width: 400,
      height: 400,
      quality: 0.4,
      cropping: true,
      // showCropFrame: true,
      // multiple: true,
      mediaType: 'photo',
      maxFiles: 10000,
    }).then(image => {
      ImageResizer.createResizedImage(image.path, 400, 400, "JPEG", 80, 0).then(
        uri => {
          var path = uri.uri
          if (Platform.OS == "ios") {
            path = uri.path
          }
          this.setState({ profileImage: path })
        }
      );
    });
  }

  openPhotos() {
    ImagePicker.openPicker({
      width: 400,
      height: 400,
      quality: 0.4,
      cropping: true,
      // showCropFrame: true,
      mediaType: 'photo',
      maxFiles: 10000,
    }).then(image => {
      ImageResizer.createResizedImage(image.path, 400, 400, "JPEG", 80, 0).then(
        uri => {
          var path = uri.uri
          if (Platform.OS == "ios") {
            path = uri.path
          }
          this.setState({ profileImage: path })
        }
      );
    });
  }

  onPressSelectCategory = () => {
    this.props.navigation.navigate("CategoryListScreen", {
      returnData: this.refreshSelectedCategory.bind(this)
    })
  }

  refreshSelectedCategory(selectedCategory) {
    console.log("selectedCategory is >>> ", selectedCategory);
    this.setState({
      selectedCategory: selectedCategory,
      category: selectedCategory.name,
      selectedSubCategory: {},
      subCategory: ''
    })
  }

  onPressSelectSubCategory = () => {
    var selectedCategory = this.state.selectedCategory
    if (selectedCategory.id == undefined) {
      showMessageAlert(strings.please_select_business_category)
    } else {
      this.props.navigation.navigate("SubCategoryListScreen", {
        categoryId: selectedCategory.id,
        returnData: this.refreshSelectedSubCategory.bind(this)
      })
    }
  }

  refreshSelectedSubCategory(selectedSubCategory) {
    console.log("selectedSubCategory is >>> ", selectedSubCategory);
    this.setState({
      selectedSubCategory: selectedSubCategory,
      subCategory: selectedSubCategory.name
    })
  }

  onPressSelectCountry = () => {
    this.props.navigation.navigate("CountryListScreen", {
      returnData: this.refreshSelectedCountry.bind(this)
    })
  }

  refreshSelectedCountry(selectedCountry) {
    console.log("selectedCountry is >>> ", selectedCountry);
    this.setState({
      selectedCountry: selectedCountry,
      country: selectedCountry.country,
      selectedRegion: {},
      region: '',
      selectedState: {},
      state: '',
      selectedCity: {},
      city: ''
    })
  }

  onPressSelectRegion = () => {
    var selectedCountry = this.state.selectedCountry
    if (selectedCountry.id == undefined) {
      showMessageAlert(strings.please_select_country)
    } else {
      this.props.navigation.navigate("RegionListScreen", {
        countryId: selectedCountry.id,
        returnData: this.refreshSelectedRegion.bind(this)
      })
    }
  }

  refreshSelectedRegion(selectedRegion) {
    console.log("selectedRegion is >>> ", selectedRegion);
    this.setState({
      selectedRegion: selectedRegion,
      region: selectedRegion.region,
      selectedState: {},
      state: '',
      selectedCity: {},
      city: ''
    })
  }

  onPressSelectState = () => {
    var selectedRegion = this.state.selectedRegion
    if (selectedRegion.id == undefined) {
      showMessageAlert(strings.please_select_region)
    } else {
      this.props.navigation.navigate("StateListScreen", {
        regionId: selectedRegion.id,
        returnData: this.refreshSelectedState.bind(this)
      })
    }
  }

  refreshSelectedState(selectedState) {
    console.log("selectedState is >>> ", selectedState);
    this.setState({
      selectedState: selectedState,
      state: selectedState.state,
      selectedCity: {},
      city: ''
    })
  }

  onPressSelectCity = () => {
    var selectedState = this.state.selectedState
    if (selectedState.id == undefined) {
      showMessageAlert(strings.please_select_state)
    } else {
      this.props.navigation.navigate("CityListScreen", {
        stateId: selectedState.id,
        returnData: this.refreshSelectedCity.bind(this)
      })
    }
  }

  refreshSelectedCity(selectedCity) {
    console.log("selectedCity is >>> ", selectedCity);
    this.setState({
      selectedCity: selectedCity,
      city: selectedCity.city
    })
  }

  tappedOnButton() {
    if (this.state.isLoading) {
      return;
    }

    if (isEmpty(this.state.mobile)) {
      showMessageAlert(strings.please_enter_mobile_number)
      return
    } else if (!this.isValidMobileNumber(this.state.mobile)) {
      showMessageAlert(strings.please_enter_valid_mobile_number)
      return
    } else if (isEmpty(this.state.adminName)) {
      showMessageAlert(strings.please_enter_merchant_admin_name)
      return
    } else if (isEmpty(this.state.organizationName)) {
      showMessageAlert(strings.please_enter_organisation_name)
      return
    } else if (this.state.organizationName.length < 4) {
      showMessageAlert(strings.organisation_name_min_length_error)
      return
    } else if (isEmpty(this.state.email)) {
      showMessageAlert(strings.please_enter_email_id)
      return
    } else if (!isValidEmail(this.state.email)) {
      showMessageAlert(strings.please_enter_valid_email_id)
      return
    } else if (isEmpty(this.state.category)) {
      showMessageAlert(strings.please_select_business_category)
      return
    } else if (isEmpty(this.state.subCategory)) {
      showMessageAlert(strings.please_select_business_sub_category)
      return
    } else if (isEmpty(this.state.country)) {
      showMessageAlert(strings.please_select_country)
      return
    } else if (isEmpty(this.state.region)) {
      showMessageAlert(strings.please_select_region)
      return
    } else if (isEmpty(this.state.state)) {
      showMessageAlert(strings.please_select_state)
      return
    } else if (isEmpty(this.state.city)) {
      showMessageAlert(strings.please_select_city)
      return
    } else if (isEmpty(this.state.location)) {
      showMessageAlert(strings.please_enter_location)
      return
    } else if (!this.state.isEdit && isEmpty(this.state.pin)) {
      showMessageAlert(strings.please_enter_pin)
      return
    }

    this.setState({ isLoading: true })
    
    if (this.state.profileImage == '') {
      this.callUserDetail('')
    } else {
      this.callUploadProfilePic()
    }
    return;
  }

  callUploadProfilePic = () => {
    var formdata = new FormData();
    let photo = {
      uri: this.state.profileImage,
      type: "image/jpeg",
      name: "image.jpg"
    };

    formdata.append("profileImage", photo);

    const apifetcherObj = postMethodUploadImageAPI('profileImage/uploadProfileImage', formdata, this.state.token)
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log("Response >>>", data);

        if (statusCode == 201 && data.ProfileImage != undefined && data.ProfileImage.url != undefined) {
          this.callUserDetail(data.ProfileImage.url)
        } else {
          this.callUserDetail('')
        }
      })
      .catch(error => {
        console.log("Error >>>", error);
        this.setState({ error, isLoading: false });
        showMessageAlert(error)
      });
    return;
  }

  callUserDetail(profileImageUrl) {
    let ciphertext = this.state.pin
    if(this.state.pin.length == 6) {
      ciphertext = CryptoJS.AES.encrypt(this.state.pin, 'GREENBILL').toString();
    }

    var params = JSON.stringify({
      userType: '2',
      Profile_url: profileImageUrl,
      name: this.state.adminName,
      mobile: this.state.mobile,
      email: this.state.email,
      moduleAccess: this.state.location,
      billingAddress: this.state.location,
      shippingAddress: this.state.location,
      pin: ciphertext,
    });

    const apifetcherObj = putMethodAPI('userDetails/updateUserDetails/' + this.state.user.id, params, this.state.token);
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log("Response ---->>>>\n ", data);

        this.callMerchantDetails(profileImageUrl)

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log("Error ---->>>> \n", error);
        this.setState({ isLoading: false });
        showMessageAlert(error)
      });
    return
  }

  callMerchantDetails(profileImageUrl) {
    let ciphertext = this.state.pin
    if(this.state.pin.length == 6) {
      ciphertext = CryptoJS.AES.encrypt(this.state.pin, 'GREENBILL').toString();
    }

    let bid = "B" + this.state.selectedCountry.countryCode + this.state.organizationName.substring(0,4) + this.state.mobile.substring(this.state.mobile.length - 8, 10)

    var params = JSON.stringify({
      userId: this.state.user.id,
      organizationName: this.state.organizationName,
      categoryId: this.state.selectedCategory.id,
      subCategoryId: this.state.selectedSubCategory.id,
      countryId: this.state.selectedCountry.id,
      regionId: this.state.selectedRegion.id,
      stateId: this.state.selectedState.id,
      cityId: this.state.selectedCity.id,
      billingAddress: this.state.location,
      businessLogo: profileImageUrl,
      pin: ciphertext,
      BID : bid
    });

    if (this.state.isEdit) {
      this.callEditMerchant(params)
    } else {
      this.callCreateMerchant(params)
    }
    return;
  }

  callCreateMerchant(params) {
    const apifetcherObj = postMethodAPI('merchantDetails/createMerchantDetailsFromMobile', params, this.state.token)
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log("Response ---->>>>\n ", data);

        if (statusCode == 201) {
          if (data.user != undefined && data.user != undefined) {
            Global.saveCurrentUser(data.user)
          }
          if (data.merchant != undefined && data.merchant != undefined) {
            Global.saveCurrentMerchant(data.merchant)
          }

          this.setState({ isLoading: false }, () => {
            this.props.navigation.navigate('Dashboard')
          })
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log("Error ---->>>> \n", error);
        this.setState({ isLoading: false });
        showMessageAlert(error)
      });
    return;
  }

  callEditMerchant(params) {
    const apifetcherObj = putMethodAPI('merchantDetails/updateMerchantDetails/' + this.state.merchant.id, params, this.state.token)
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log("Response ---->>>>\n ", data);

        if (statusCode == 200) {
          DeviceEventEmitter.emit('updateUserMerchant')
          this.setState({ isLoading: false }, () => {
            this.props.navigation.goBack()
          })
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log("Error ---->>>> \n", error);
        this.setState({ isLoading: false });
        showMessageAlert(error)
      });
    return;
  }

  isValidMobileNumber = value => {
    let reg = /^([0-9]){10}$/;
    if (reg.test(value) === false) {
      return false;
    } else {
      return true;
    }
  };

}

const Styles = StyleSheet.create({
  container: {
    padding: 25,
    backgroundColor: MyColors.whiteColor,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25
  },

  imgBack: {
    resizeMode: 'center',
    margin: 25,
  },

  // ---------------------------Profile
  cellImage: {
    width: 110,
    height: 110,
    alignSelf: 'center',
    margin: 20,
    borderWidth: 5,
    borderRadius: 20,
    borderColor: MyColors.whiteColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgEditIcon: {
    padding: 7, 
    right: -10, 
    bottom: -10, 
    position: 'absolute', 
    borderRadius: 25, 
    backgroundColor: MyColors.whiteColor
  },

  txtLabel: {
    fontFamily: 'Roboto-Regular',
    fontSize: 14,
    color: '#5A607B',
    marginTop: 20,
  },
  mobileContainer: {
    marginTop: 10,
    backgroundColor: '#F1F1F4',
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  txtCountryNumber: {
    marginEnd: 10,
    fontSize: 14,
    fontFamily: 'Roboto-Regular'
  },
  flagImage: {
    width: 25,
    height: 15,
    resizeMode: 'center',
    marginStart: 10,
    marginTop: 15,
    marginBottom: 15,
    marginEnd: 5
  },
  textInput: {
    paddingStart: 20,
    padding: 10,
    flex: 1,
    height: '100%',
    fontSize: 16,
    color: MyColors.blackColor,
    fontFamily: 'Roboto-Regular'
  },

  inputItem: {
    backgroundColor: MyColors.whiteColor,
    borderRadius: 10,
    color: MyColors.blackColor,
    width: (Dimensions.get('window').width - 40) / 6 - 10
  }
});
