import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Image,
  SafeAreaView,
  Text,
  ImageBackground,
  BackHandler,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { MyColors } from '../../Theme';
import images_path from '@Images/Images';
import strings from '../../Localization/string';
import { postMethodAPI } from '../../API/APIClient';
import { isEmpty, showMessageAlert } from '../../Utility/Utility';

import CustomButton from '../../customComponents/CustomButton';
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class MobileScreen extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    isLoading: false,
    mobile: ''
  };

  componentDidMount() {

  }

  componentWillMount() {
    console.log("###### inside componentWillMount");
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    console.log("###### inside componentWillUnmount");
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log("#### handleBackButtonClick ", this.props.navigation);
    if (!this.props.navigation.isFocused()) {
      // The screen is not focused, so don't do anything
      return false;
    }
    BackHandler.exitApp()
    return true;
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, }}>
        <CustomStatusBarTheme />

        <ImageBackground
          source={images_path.login_background}
          style={{ flex: 1 }}>
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: MyColors.white }}>
            <View style={Styles.container}>
              <Image style={{ marginTop: 80 }} source={images_path.logo_bg} />
              <Text style={Styles.txtAppName}>{strings.app_name}</Text>
              <Text style={Styles.txtMobileTitle}>{strings.mobile_number}</Text>
              {this.renderMobileField()}
              {this.renderButton()}
            </View>
          </KeyboardAwareScrollView>
        </ImageBackground>
      </SafeAreaView>
    );
  }

  renderMobileField() {
    return (
      <View style={Styles.mobileContainer}>
        <Image
          source={images_path.india}
          style={Styles.flagImage}
        />

        <Text style={Styles.txtCountryNumber} >+91</Text>

        <View style={{ width: 1, height: '70%', backgroundColor: '#5A607B' }} />

        <TextInput
          autoCompleteType="tel"
          autoCorrect={false}
          keyboardType="number-pad"
          returnKeyType="done"
          placeholder={strings.enter_mobile_number}
          value={this.state.mobile}
          numberOfLines={1}
          autoFocus={true}
          maxLength={12}
          onChangeText={(text) => this.setState({mobile:text})}
          style={Styles.textInput}
        />
      </View>
    );
  }

  renderButton() {
    return (
      <CustomButton
        title={strings.get_otp}
        isLoading={this.state.isLoading}
        buttonCustomStyle={{ marginTop: 20, paddingHorizontal: 20 }}
        onPress={() => this.tappedOnButton()}
      />
    );
  }

  tappedOnButton() {

    if (this.state.isLoading) {
      return;
    }

    if (isEmpty(this.state.mobile)) {
      showMessageAlert(strings.please_enter_mobile_number)
      return
    } else if (!this.isValidMobileNumber(this.state.mobile)) {
      showMessageAlert(strings.please_enter_valid_mobile_number)
      return
    }

    this.setState({ isLoading: true })

    var params = JSON.stringify({
      countryCode: 'IN',
      mobile: this.state.mobile
    });

    const apifetcherObj = postMethodAPI("register", params, null);
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log("Response ---->>>>\n ", data);

        this.setState({ isLoading: false })

        if (statusCode == 200) {
          if (data.merchantFirstTime != null &&
            data.merchantFirstTime != undefined &&
            !data.merchantFirstTime) {
            this.props.navigation.navigate('VerifyPinScreen', { mobile: data.mobile })
          } else {
            this.props.navigation.navigate('OtpScreen', { 'data': data })
          }
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log("Error ---->>>> \n", error);
        this.setState({ isLoading: false });
        showMessageAlert(error)
      });
    return
  }

  isValidMobileNumber = value => {
    let reg = /^([0-9]){8,12}$/;
    if (reg.test(value) === false) {
      return false;
    } else {
      return true;
    }
  };

}

const Styles = StyleSheet.create({
  container: {
    padding: 20,
    alignItems: 'center',
  },
  txtAppName: {
    fontSize: 24,
    fontFamily: 'Sansation-Bold'
  },
  txtMobileTitle: {
    fontSize: 14,
    marginTop: 50,
    fontFamily: 'Roboto-Regular',
  },
  txtCountryNumber: {
    marginEnd: 10,
    fontSize: 14,
    fontFamily: 'Roboto-Regular'
  },
  mobileContainer: {
    marginTop: 30,
    backgroundColor: MyColors.whiteColor,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  flagImage: {
    width: 30,
    height: 20,
    resizeMode: 'center',
    marginStart: 15,
    marginTop: 20,
    marginBottom: 20,
    marginEnd: 5
  },
  textInput: {
    paddingStart: 20,
    padding: 15,
    flex: 1,
    height: '100%',
    fontSize: 16,
    color: MyColors.blackColor,
    fontFamily: 'Roboto-Regular'
  }
});
