import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  Text,
  ImageBackground,
  ActivityIndicator,
} from 'react-native'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { MyColors } from '../../Theme'
import Global from '../../Utility/Global'
import strings from '../../Localization/string';
import { isEmpty, showMessageAlert } from '../../Utility/Utility'
import { postMethodAPI, getMethodAPI } from '../../API/APIClient';

import CustomButton from '../../customComponents/CustomButton'
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class OtpScreen extends Component {
  constructor(props) {
    super(props)
  }

  state = {
    data: {},
    isResendLoading: false,
    isLoading: false,
    otp: '',

    timer: 30,
  }

  componentDidMount() {
    if (this.props.navigation.state != null && this.props.navigation.state.params != undefined) {
      this.setState(
        {
          data: this.props.navigation.state.params.data
        },
        () => console.log('data == > ', this.state.data)
      )
    }

    this.interval = setInterval(
      () => this.setState((prevState) => ({ timer: prevState.timer - 1 })),
      1000
    );
  }

  componentDidUpdate() {
    if (this.state.timer === 1) {
      clearInterval(this.interval);
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, }}>
        <CustomStatusBarTheme />
        <ImageBackground
          source={images_path.login_background}
          style={Styles.topBgImage} />
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: MyColors.white }}>
            <View style={Styles.container}>
              <Image style={{ marginTop: 80 }} source={images_path.logo_bg} />
              <Text style={{ fontSize: 24, fontFamily: 'Sansation-Bold' }}>{strings.app_name}</Text>
              <Text style={Styles.txtVerifyTitle}>{strings.verify_otp}</Text>
              {this.renderOtpField()}
              {this.renderResend()}
              {this.renderButton()}
            </View>
          </KeyboardAwareScrollView>
      </SafeAreaView>
    )
  }

  renderOtpField() {
    return (
      <OTPInputView
        style={{ width: '100%', height: 50, marginTop: 15 }}
        pinCount={4}
        code={this.state.otp} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
        onCodeChanged={otp => { this.setState({ otp }) }}
        autoFocusOnLoad={true}
        codeInputFieldStyle={Styles.inputItem}
        // codeInputHighlightStyle={{
        //   borderColor: "#03DAC6",
        // }}
        onCodeFilled={code => {
          if (code.length == 4 && this.state.data.otp == code) {
            this.tappedOnButton()
          }
          // console.log(`Code is ${code}, you are good to go!`)
        }}
      />
    )
  }

  renderResend() {
    return (
      <TouchableOpacity
        style={Styles.resendOtpContainer}
        onPress={() => { this.state.timer == 1 ? this.callResendOTPAPI() : null }}>
        {this.state.isResendLoading ? <ActivityIndicator color="white" /> :
          <View style={Styles.resendSubContainer}>
            {this.state.timer == 1 ?
              <Image source={images_path.refresh}
                style={{ resizeMode: 'center', marginEnd: 10 }}
              /> : null
            }
            <Text
              style={{ color: this.state.timer == 1 ? MyColors.blackColor : MyColors.vwBorder, fontFamily: 'Roboto-Medium' }}
            >{this.state.timer == 1 ? strings.resend_otp : 'Resend in ' + this.state.timer + ' Sec'}</Text>
          </View>
        }
      </TouchableOpacity>
    )
  }

  renderButton() {
    return (
      <CustomButton
        title={strings.sign_in}
        isLoading={this.state.isLoading}
        buttonCustomStyle={{ marginTop: 30, paddingHorizontal: 20 }}
        onPress={() => this.tappedOnButton()}
      />
    )
  }

  tappedOnButton() {
    if (this.state.isLoading) {
      return
    }

    if (isEmpty(this.state.otp)) {
      showMessageAlert(strings.please_enter_otp)
      return
    }

    this.setState({ isLoading: true })

    var params = JSON.stringify({
      otp: this.state.otp,
      userType: '2',
    });

    const apifetcherObj = postMethodAPI("register/" + this.state.data.mobile, params, null);
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log("Response ---->>>>\n ", data);

        if (statusCode == 201) {
          if (data.accessToken != null && data.accessToken != undefined) {
            Global.saveCurrentToken(data.accessToken);
          }

          if (data.user != null && data.user != undefined) {
            Global.saveCurrentUser(data.user)
          }

          if (data.isFirstTime != null &&
            data.isFirstTime != undefined &&
            !data.isFirstTime) {
            this.props.navigation.navigate('VerifyPinScreen', { mobile: data.user.mobile })
          } else {
            this.props.navigation.navigate('CompleteProfileScreen', { isEdit: false, data: data.user })
          }
        }

        this.setState({ isLoading: false })

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log("Error ---->>>> \n", error);
        this.setState({ isLoading: false });
        showMessageAlert(error)
      });
    return
  }

  callResendOTPAPI = () => {
    if (this.state.isResendLoading) {
      return
    }

    this.setState({ isResendLoading: true })

    const apifetcherObj = getMethodAPI("register/" + this.state.data.mobile, null, null);
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log("Response ---->>>>\n ", data);

        this.setState({ isResendLoading: false })

        if (statusCode == 200) {

        } else if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        this.setState({ isResendLoading: false })
        console.log("Error ---->>>> \n", error);
        showMessageAlert(error)
      });
    return;
  }

}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
  },
  topBgImage: {
    width: '100%',
    height: '100%',
    position: 'absolute'
  },
  txtVerifyTitle: {
    fontSize: 14,
    marginTop: 50,
    fontFamily: 'Roboto-Regular',
  },
  resendOtpContainer: {
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: MyColors.whiteColor,
    borderRadius: 10,
    padding: 10,
    marginTop: 30,
  },
  resendSubContainer:{
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  
  inputItem: {
    backgroundColor: MyColors.whiteColor,
    borderRadius: 10,
    color: MyColors.blackColor,
    width: (Dimensions.get('window').width - 40) / 4 - 10
  }
})
