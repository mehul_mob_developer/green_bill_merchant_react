import React, { Component } from 'react'
import {
  View,
  TouchableOpacity,
  FlatList,
  Text,
  StyleSheet,
  TextInput,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient';

import { MyColors } from '../../Theme'
import Global from '../../Utility/Global';
import strings from '../../Localization/string'
import { getMethodAPI } from '../../API/APIClient';
import { isEmpty, showMessageAlert } from '../../Utility/Utility'
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class SearchInvoiceScreen extends Component {

  constructor() {
    super()
    this.state = {
      isLoading: true,
      token: '',
      user: {},
      merchant: {},

      isCid: false,
      isInvoiceNo: false,
      isConsumerName: false,

      keyword: '',
      list: [],
    }
  }

  componentDidMount() {
    Global.getCurrentToken().then(token => {
      Global.getCurrentUserPromise().then(user => {
        Global.getCurrentMerchantPromise().then(merchant => {
          this.setState({
            token: token,
            user: user,
            merchant: merchant,
          }, () => {
            console.log('user----------->>>>>>>', this.state.user)
            console.log('merchant------->>>>>>>', this.state.merchant)
            this.callInvoiceList()
          })
        })
      })
    })
  }

  callInvoiceList = () => {
    this.setState({ isLoading: true })

    var searchKey =
      this.state.isCid ? '&offerName=' :
        this.state.isInvoiceNo ? '&merchantBranchId=' :
          this.state.isConsumerName ? '&status=' :
          '&searchedValue='

    const apifetcherObj = getMethodAPI(
      'merchantDashboard/getInvoices/' + this.state.merchant.id + '?' + searchKey + this.state.keyword,
      // 'merchantDashboard/getInvoices/4',
      null,
      this.state.token
    )
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log('Success in API is >>> ', data)

        this.setState({ isLoading: false })

        if (statusCode == 200 && data.records != undefined) {
          this.setState({ list: data.records })
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log('Error in API is >>> ', error)
        this.setState({ isLoading: false })
      })
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: MyColors.whiteColor, }}>
        <CustomStatusBarTheme />

        <View style={Styles.container}>
          {this.renderSearch()}
          {this.renderFilter()}

          <View style={{ flex: 1 }} >
            {this.state.isLoading ? (
              <ActivityIndicator
                style={{ alignSelf: 'center' }}
                color={MyColors.themeColor}
                size='large'
              />
            ) : (
              this.state.list.length == 0 || this.state.list.length == undefined ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text>{strings.no_record_found}</Text>
                </View> :
                <View style={{ flex: 1 }}>
                  {this.renderList()}
                </View>
            )}
          </View>
        </View>
      </SafeAreaView>
    )
  }

  renderSearch() {
    return (
      <View style={Styles.searchContainer}>
        <TextInput
          style={{ padding: 10 }}
          clearButtonMode='always'
          returnKeyType='search'
          placeholder={strings.search}
          value={this.state.keyword}
          onChangeText={txt => { this.setState({ keyword: txt }) }}
          onSubmitEditing={(event) => { this.onChangeTextSearch() }}
        />
      </View>
    )
  }

  onChangeTextSearch = (key, value) => {
    console.log('onChangeText', key, value);
    this.setState({
      [key]: value,
      next_offset: 0,
      list: [],
    }, () => { this.callInvoiceList() });
  };

  renderFilter() {
    return (
      <View style={{ marginVertical: 10 }}>
        <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14 }}>{strings.search_by}</Text>
        <View style={{ flexDirection: 'row', marginTop: 10, }}>
          <TouchableOpacity style={[Styles.txtTagContainer,
          { backgroundColor: this.state.isCid ? MyColors.themeColor : null }]}
            onPress={() => {
              this.setState({
                isCid: true,
                isInvoiceNo: false,
                isConsumerName: false
              })
            }}>
            <Text style={[Styles.txtTag, { color: this.state.isCid ? MyColors.whiteColor : '#07321A' }]}>{strings.cid}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[Styles.txtTagContainer,
          { backgroundColor: this.state.isInvoiceNo ? MyColors.themeColor : null, marginHorizontal: 5 }]}
            onPress={() => {
              this.setState({
                isCid: false,
                isInvoiceNo: true,
                isConsumerName: false
              })
            }}>
            <Text style={[Styles.txtTag, { color: this.state.isInvoiceNo ? MyColors.whiteColor : '#07321A' }]}>{strings.invoice_no_dot}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[Styles.txtTagContainer,
          { backgroundColor: this.state.isConsumerName ? MyColors.themeColor : null }]}
            onPress={() => {
              this.setState({
                isCid: false,
                isInvoiceNo: false,
                isConsumerName: true
              })
            }}>
            <Text style={[Styles.txtTag, { color: this.state.isConsumerName ? MyColors.whiteColor : '#07321A' }]}>{strings.consumer_name}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderList() {
    return (
      <FlatList
        style={{ marginTop: 20 }}
        data={this.state.list}
        renderItem={this.renderItem}
        showsVerticalScrollIndicator={false}
      />
    )
  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => { }}>
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
          <Text style={Styles.txtInvoiceNo}>{'Invoice No. #' + item.InvoiceNo}</Text>
          <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14 }}>{item.DateTime}</Text>
        </View>
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 5, }}>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 18 }}>{item.ConsumerName}</Text>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 14, color: MyColors.themeColor }}>{item.CID}</Text>
        </View>
        <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14, marginTop: 5 }}>{item.ContactNo}</Text>
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 5, }}>
          <Text style={{ flex: 1, fontFamily: 'Roboto-Regular', fontSize: 14 }}>{item.branchName}</Text>
          <LinearGradient
            colors={['#19CB3F', '#83DA48']}
            style={{ borderRadius: 10, }}
            start={{ x: 0, y: 1 }}
            end={{ x: 1, y: 1 }}>
            <Text style={{ color: MyColors.whiteColor, fontFamily: 'Roboto-Medium', fontSize: 14, padding: 10 }}>{strings.update_resend}</Text>
          </LinearGradient>
        </View>
        <View style={Styles.vwSeparator} />
      </TouchableOpacity>
    )
  }

}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: MyColors.whiteColor,
    padding: 15
  },
  searchContainer: {
    justifyContent: 'center',
    borderRadius: 10,
    marginVertical: 10,
    backgroundColor: '#F1F1F4'
  },

  txtTagContainer: {
    borderWidth: 1,
    borderRadius: 10,
    borderColor: MyColors.themeColor,
  },
  txtTag: {
    fontFamily: 'Roboto-Regular',
    fontSize: 16,
    padding: 8,
  },

  vwSeparator: {
    flex: 1,
    height: 1,
    backgroundColor: MyColors.seperatorGreyColor,
    marginVertical: 15
  },

})
