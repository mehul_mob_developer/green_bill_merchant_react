import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  SafeAreaView,
  DeviceEventEmitter,
  Text,
  FlatList,
  ActivityIndicator,
  Alert,
  BackHandler,
  Dimensions,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler'
import moment from "moment";
// import BottomSheet from 'reanimated-bottom-sheet';
import BottomSheet from '@gorhom/bottom-sheet';
import LinearGradient from 'react-native-linear-gradient';
import { DatePickerModal } from 'react-native-paper-dates';
import SlidingUpPanel from 'rn-sliding-up-panel';


import { MyColors } from '../../Theme';
import images_path from '@Images/Images';
import Global from '../../Utility/Global';
import strings from '../../Localization/string';
import { showMessageAlert, isEmpty } from '../../Utility/Utility';

import CustomButton from '../../customComponents/CustomButton';
import { getMethodAPI, postMethodAPI } from '../../API/APIClient';
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class HomeScreen extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    isLoading: false,

    token: '',
    user: {},
    merchant: {},

    isDashboard: true,

    TotalBillsCount: 0,
    TotalTreesSaved: 0,
    TotalSaving: 0,

    list: [],

    branchName: strings.select_branch,
    selectedBranch: {},

    isMenu: false,
    menuType: strings.today,
    isPickDate: false,

    isBottomSheet: true

  };

  componentDidMount() {
    if (this.props.navigation.state != null && this.props.navigation.state.params != undefined) {
      this.setState({ isDashboard: this.props.navigation.state.params.isDashboard }, () => {
        console.log('isDashboard--------------------------------------', this.state.isDashboard)
      })
    }

    Global.getCurrentToken().then(token => {
      Global.getCurrentUserPromise().then(user => {
        Global.getCurrentMerchantPromise().then(merchant => {
          this.setState({
            token: token,
            user: user,
            merchant: merchant,
          }, () => {
            console.log('user----------->>>>>>>', this.state.user)
            console.log('merchant------->>>>>>>', this.state.merchant)
            this.callCount()
            this.callMeAPI()
          })
        })
      })
    })

    Global.getCurrentBranchPromise().then(branch => {
      this.setState({selectedBranch: branch, branchName: branch.branchName}, () => {
        console.log('selectedBranch----------->>>>>>>', this.state.selectedBranch)
      })
    })

    DeviceEventEmitter.addListener('updateUserMerchant', () => {
      this.callMeAPI()
    })
  }

  componentWillMount() {
    console.log("###### inside componentWillMount");
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    console.log("###### inside componentWillUnmount");
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log("#### handleBackButtonClick ", this.props.navigation);
    if (!this.props.navigation.isFocused()) {
      // The screen is not focused, so don't do anything
      return false;
    }

    if(this.state.isDashboard) {
      Alert.alert(
        strings.app_name,
        strings.are_you_sure_you_want_to_exit_app, [{
          text: strings.cancel,
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel'
        }, {
          text: strings.ok,
          onPress: () => BackHandler.exitApp()
        },], {
        cancelable: false
      })
    } else {
      this.props.navigation.goBack()
    }
    return true;
  }

  callCount = () => {
    this.setState({ isLoading: true })

    const apifetcherObj = getMethodAPI(
      'merchantDashboard/getMerchantDashboardCounts/' + this.state.merchant.id, null, this.state.token
    )
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log('Success in API is >>> ', data)

        if (statusCode == 200) {
          this.setState({
            TotalBillsCount: data.TotalBillsCount,
            TotalTreesSaved: data.TotalTreesSaved,
            TotalSaving: data.TotalSaving
          })
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }

        this.callInvoiceList()
      })
      .catch(error => {
        console.log('Error in API is >>> ', error)
        this.setState({ isLoading: false })
      })
  }

  callInvoiceList = () => {
    const apifetcherObj = getMethodAPI(
      'merchantDashboard/getInvoices/' + this.state.merchant.id + '?offset=0', null, this.state.token
      // 'merchantDashboard/getInvoices/' + 4 + '?offset=0', null, this.state.token
    )
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log('Success in API is >>> ', data)

        this.setState({ isLoading: false })

        if (statusCode == 200) {
          this.setState({ list: data.records })
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log('Error in API is >>> ', error)
        this.setState({ isLoading: false })
      })
  }

  callMeAPI() {
    const apifetcherObj = getMethodAPI('merchantDetails/getMerchantForApp/' + this.state.merchant.id, null, this.state.token);
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log("Response ---->>>>\n ", data)

        if (statusCode == 200) {
          if (data.user != null && data.user != undefined) {
            Global.saveCurrentUser(data.user)
          }

          if (data.merchant != null && data.merchant != undefined) {
            var merchant = data.merchant
            merchant.selectedCategory = data.category
            merchant.selectedSubCategory = data.subCategory
            merchant.selectedCountry = data.country
            merchant.selectedRegion = data.region
            merchant.selectedState = data.state
            merchant.selectedCity = data.city
            merchant.category = data.category.name
            merchant.subCategory = data.subCategory.name
            merchant.country = data.country.country
            merchant.region = data.region.region
            merchant.state = data.state.state
            merchant.city = data.city.city
            Global.saveCurrentMerchant(merchant)
          }

          this.setState({
            user: data.user,
            merchant: merchant
          })

          DeviceEventEmitter.emit('refreshUserMerchant')
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log("Error ---->>>> \n", error);
        showMessageAlert(error)
      });
    return
  }

  render() {
    console.log('this.state.list-----------------------------', this.state.list)
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: MyColors.whiteColor, }}>
        <CustomStatusBarTheme />
        <Image source={images_path.top_bg}
          style={{ width: '100%', position: 'absolute' }} />
        <ScrollView contentContainerStyle={{ flexGrow: 1, }} showsVerticalScrollIndicator={false} >
          <View style={{ flex: 1, }}>
            {this.renderHeader()}
            <View style={Styles.container}>
              {this.state.isLoading ? (
                <ActivityIndicator
                  style={{ alignSelf: 'center' }}
                  color={MyColors.themeColor}
                  size='large'
                />
              ) : (
                <View >
                  {this.renderTotal()}
                  {this.renderFilter()}
                  {this.renderCalendar()}
                  <Text style={Styles.txtTitle}>{strings.invoices}</Text>
                  {this.state.list == undefined || this.state.list.length == undefined || this.state.list.length == 0 ?
                      <View style={{ zIndex : 1,  flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={images_path.promotions_placeholder} />
                      </View> :
                      <View>
                        {this.renderList()}
                      </View>
                    }
                </View>
              )}
            </View>
          </View>
          
        </ScrollView>

        {!this.state.isDashboard ? this.renderBottomSheet() : null}

      </SafeAreaView >
    );
  }

  renderHeader() {
    return (
      <View style={Styles.cellHeader}>
        <View style={{ left : Dimensions.get("window").width - 85}}>
        <TouchableOpacity
          onPress={() => { this.props.navigation.toggleDrawer() }}>
          <Image style={{width : 35, height : 35}}  source={images_path.menu} />
        </TouchableOpacity>
        </View>
        <View style={{alignItems : "center"}}>
          <Image style={{height : 70, width : 70, borderRadius : 10, borderWidth : 3, borderColor : "white"}} source={{uri : this.state.merchant.businessLogo}} />
          <Text style={Styles.txtUserName}>{this.state.user.name}</Text>
        </View>
      </View>
    );
  }

  renderTotal() {
    return (
      <View style={{ flex: 1, flexDirection: 'row', }}>
        <View style={[Styles.vwRow, { marginEnd: 5 }]}>
          <Image style={{ width: 30 }} source={images_path.bill} resizeMode='contain' />
          <View style={{ marginStart: 5 }}>
            <Text style={Styles.txtCount}>{this.state.TotalBillsCount}</Text>
            <Text style={Styles.txtCountTitle}>{strings.bills_generated}</Text>
          </View>
        </View>
        {this.state.isDashboard ?
          <View style={[Styles.vwRow, { justifyContent: "center" }]}>
            <Image style={{ width: 30 }} source={images_path.tree} resizeMode='contain' />
            <View style={{ marginStart: 5 }}>
              <Text style={Styles.txtCount}>{this.state.TotalTreesSaved}</Text>
              <Text style={Styles.txtCountTitle}>{strings.trees_saved}</Text>
            </View>
          </View>
          : null}
        <View style={[Styles.vwRow, { marginStart: 5, justifyContent: "flex-end" }]}>
          <Image style={{ width: 30 }} source={images_path.wallet} resizeMode='contain' />
          <View style={{ marginStart: 5 }}>
            <Text style={Styles.txtCount}>{this.state.TotalSaving}</Text>
            <Text style={Styles.txtCountTitle}>{strings.total_savings}</Text>
          </View>
        </View>
      </View>
    )
  }

  renderFilter() {
    return (
      <View style={Styles.filterContainer}>
        <TouchableOpacity style={{ minWidth: 100 }}
          onPress={() => { this.onPressSelectBranch() }} >
          <LinearGradient
            colors={['#19CB3F', '#83DA48']}
            style={{ borderRadius: 10, }}
            start={{ x: 0, y: 1 }}
            end={{ x: 1, y: 1 }}>
            <View style={{ padding: 10, flexDirection: 'row', alignItems: 'center' }}>
              <Text style={Styles.txtBranch}>{this.state.branchName}</Text>
              <Image source={images_path.arrow_down_white} />
            </View>
          </LinearGradient>
        </TouchableOpacity>

        <View style={{ minWidth: 100 }}>
          <TouchableOpacity style={Styles.menuContainer}
            onPress={() => { this.setState({ isMenu: true })}}>
            <Text style={Styles.txtMenu}>{this.state.menuType}</Text>
            <Image source={images_path.arrow_down_green} />
          </TouchableOpacity>
          {this.state.isMenu ? 
          <View style={{ alignItems: 'center' }}>
            <View style={Styles.menuListContainer}>
              <View>
              <TouchableOpacity onPress={() => {this.setState({ isMenu: false, menuType: strings.today })}}>
                  <Text style={[Styles.txtMenuItem, { color: this.state.menuType == strings.today ? MyColors.themeColor : MyColors.blackColor }]}>{strings.today}</Text>
                </TouchableOpacity>
                <View style={{ height: 1, backgroundColor: MyColors.vwBorder }} />
                <TouchableOpacity onPress={() => {console.log("Hey stranger ")
                 this.setState({ isMenu: false, menuType: strings.this_week })}}>
                  <Text style={[Styles.txtMenuItem, { color: this.state.menuType == strings.this_week ? MyColors.themeColor : MyColors.blackColor }]}>{strings.this_week}</Text>
                </TouchableOpacity>
                <View style={{ height: 1, backgroundColor: MyColors.vwBorder }} />
                <TouchableOpacity onPressIn={() => {
                  console.log("Hey stranger two....")
                  this.setState({ isMenu: false, menuType: strings.this_month })}}>
                  <Text style={[Styles.txtMenuItem, { color: this.state.menuType == strings.this_month ? MyColors.themeColor : MyColors.blackColor }]}>{strings.this_month}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          : null }
        </View>

        <View style={{ flex: 1 }} />

        <TouchableOpacity onPress={() => { this.setState({ isPickDate: true }) }} >
          <Image source={images_path.calendar} style={{ marginHorizontal: 5, }} resizeMode='contain' />
        </TouchableOpacity>

        <TouchableOpacity style={Styles.searchContainer}
          onPress={() => { this.props.navigation.navigate('SearchInvoiceScreen') }}>
          <Image source={images_path.search} />
        </TouchableOpacity>

      </View>
    )
  }

  onPressSelectBranch = () => {
    this.props.navigation.navigate("SelectBranchListScreen", {
      returnData: this.refreshSelectedBranch.bind(this)
    })
  }

  refreshSelectedBranch(selectedBranch) {
    console.log("selectedBranch is >>> ", selectedBranch);
    this.setState({
      selectedBranch: selectedBranch,
      branchName: selectedBranch.branchName
    })
  }

  renderCalendar() {
    return (
      <DatePickerModal
        // locale={'en'} optional, default: automatic
        mode="single"
        visible={this.state.isPickDate}
        date={new Date()}
        onDismiss={this._hideDateCancel}
        onConfirm={this._handleDateConfirm}
        validRange={{
          // startDate: new Date(2021, 6, 18),  // optional
          endDate: new Date(), // optional
        }}
      // onChange={} // same props as onConfirm but triggered without confirmed by user
      // saveLabel="Save" // optional
      // label="Select date" // optional
      // animationType="slide" // optional, default is 'slide' on ios/android and 'none' on web
      />
    )
  }

  _hideDateCancel = () => this.setState({ isPickDate: false });

  _handleDateConfirm = (params) => {
    console.log('A date has been picked: ', params);
    this._hideDateCancel();
    // this.setState({ expiry_date: moment(params) });
  }

  renderList() {
    return (
      <FlatList
        style={{ flex: 1, marginTop: 20, }}
        data={this.state.list}
        renderItem={this.renderItem}
      />
    )
  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => { }}>
        <View style={Styles.itemSubContainer}>
          <Text style={Styles.txtInvoiceNo}>{'Invoice No. #' + item.InvoiceNo}</Text>
          <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14 }}>{item.DateTime}</Text>
        </View>
        <View style={[Styles.itemSubContainer, { marginTop: 5, }]}>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 18 }}>{item.ConsumerName}</Text>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 14, color: MyColors.themeColor }}>{item.CID}</Text>
        </View>
        <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14, marginTop: 5 }}>{item.ContactNo}</Text>
        <View style={[Styles.itemSubContainer, { marginTop: 5, }]}>
          <Text style={{ flex: 1, fontFamily: 'Roboto-Regular', fontSize: 14 }}>{item.branchName}</Text>
          <LinearGradient
            colors={['#19CB3F', '#83DA48']}
            style={{ borderRadius: 10, }}
            start={{ x: 0, y: 1 }}
            end={{ x: 1, y: 1 }}>
            <Text style={Styles.txtUpdateResend}>{strings.update_resend}</Text>
          </LinearGradient>
        </View>
        <View style={Styles.vwSeparator} />
      </TouchableOpacity>
    )
  }

  renderBottomSheet() {
    return (
      <SlidingUpPanel
          ref={c => (this._panel = c)}
          draggableRange={{top: Dimensions.get('window').height / 3, bottom: 120}}
          animatedValue={this._draggedValue}
          showBackdrop={false}
          allowMomentum={true}
          allowDragging={false}
          // onDragStart={(value, gestureState) => { console.log('onDragStart----------------------', value) }}
          // onDragEnd={(value, gestureState) => { console.log('onDragEnd----------------------', value) }}
          onMomentumDragStart={(value, gestureState) => { console.log('onMomentumDragStart----------------------', value) }}
          onMomentumDragEnd={(value, gestureState) => { console.log('onMomentumDragEnd----------------------', value) }}
          onBottomReached={() => { 
            console.log('onBottomReached----------------------')
            this.setState({ isBottomSheet: true })
          }}
      >
          <View style={Styles.panel}>
            <View style={Styles.panelHeader}>
              {/* <TouchableOpacity onPress={() => this._panel.show()}>
                <Text style={{color: '#FFF'}}>Bottom Sheet Peek</Text>
              </TouchableOpacity> */}
              {this.state.selectedBranch.branchName == undefined ?
                <View style={{ alignItems: 'center' }}>
                  <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 20 }}>{strings.no_selected_branch_found}</Text>
                </View>
                :
                <View>
                  <View style={{ flexDirection:'row', alignItems:'center' }}>
                    <Text style={{ flex:1, fontFamily:'Roboto-Bold', fontSize:24 }}>{this.state.selectedBranch.branchName}</Text>
                    <TouchableOpacity style={{ padding:10 }} 
                    onPress={()=>{
                      // this.sheetRef.current.snapTo(1)
                      if(this.state.isBottomSheet){
                        this.setState({ isBottomSheet: false }, () => { this._panel.show() })
                      } else {
                        this.setState({ isBottomSheet: true }, () => { this._panel.hide() })
                      }
                      console.log('onPress------------------')
                    }}>
                      <Image source={this.state.isBottomSheet ? images_path.arrow_up : images_path.arrow_down_black} />
                    </TouchableOpacity>
                  </View>
                  { this.state.selectedBranch.managerName == undefined ? null : <Text style={{ fontFamily:'Roboto-Medium', fontSize: 14, marginTop: 5 }}>{this.state.selectedBranch.managerName}</Text> }
                  <Text style={{ fontFamily:'Roboto-Regular', fontSize: 14, color:MyColors.themeColor, marginTop:5 }}>Manager</Text> 
                  
                </View>
              }
            </View>
            <View>
              {/* <TouchableOpacity onPress={() => this._panel.hide()}>
                <Text>Bottom Sheet Content</Text>
              </TouchableOpacity> */}
              <View style={{ flexDirection: 'row', marginTop: 5, alignItems: 'center' }}>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                      <Image source={images_path.phone} />
                      <Text style={{ fontFamily:'Roboto-Regular', fontSize:14, marginStart: 10 }}>{this.state.selectedBranch.contactNumber}</Text> 
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Text style={{ fontFamily:'Roboto-Bold', fontSize: 14, }}>{strings.pos_collon}</Text> 
                      <Text style={{ fontFamily:'Roboto-Regular', fontSize: 14, marginStart: 10, color:MyColors.themeColor }}>Active</Text> 
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 5, alignItems: 'center' }}>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                      <Image source={images_path.mail} />
                      <Text style={{ fontFamily:'Roboto-Regular', fontSize:14, marginStart: 10 }}>{this.state.selectedBranch.customerCareEmail}</Text> 
                    </View>
                    <TouchableOpacity
                      activeOpacity={0.7}
                      style={{
                        paddingHorizontal: 20, 
                        marginStart: 10,
                        paddingVertical: 10,
                        borderRadius: 15,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: MyColors.themeColor,
                  
                        shadowColor: "#000",
                        shadowOffset: {
                          width: 0,
                          height: 5,
                        },
                        shadowOpacity: 0.36,
                        shadowRadius: 6.68,
                  
                        elevation: 11, }}
                      onPress={() => this.props.navigation.navigate("PosScreen")}>
                      <Text style={{
                        fontSize: 14,
                        fontFamily: 'Roboto-Medium',
                        color: MyColors.whiteColor,
                      }} >{strings.more}</Text>
                    </TouchableOpacity>
                  </View>
            </View>
          </View>
        </SlidingUpPanel>
        
      // <SlidingUpPanel
      //     ref={sheet => (this.sheetRef = sheet)}
      //     snapPoints={['20%', '45%']}
      //     borderRadius={25}
      //     initialSnap={1}
      //     // initialSnap={this.state.isBottomSheet ? 1 : 0}
      //     // enabledInnerScrolling={false}
      //     renderContent={this.renderBottomSheetContent}
      //     onOpenEnd={()=>{
      //       console.log('onOpenEnd----------------------')
      //       this.setState({ isBottomSheet: false })
      //     }}
      //     onCloseEnd={()=>{
      //       console.log('onCloseEnd----------------------')
      //       this.setState({ isBottomSheet: true })
      //     }}
      //     // snapTo={this.state.isBottomSheet ? 1 : 0}
      //   />
    )
  }

  renderBottomSheetContent = () => (
    <View style={Styles.bottomSheetContainer}>
      {this.state.selectedBranch.branchName == undefined ?
        <View style={{ alignItems: 'center' }}>
          <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 20 }}>{strings.no_selected_branch_found}</Text>
        </View>
        :
        <View>
          <View style={{ flexDirection:'row', alignItems:'center' }}>
            <Text style={{ flex:1, fontFamily:'Roboto-Bold', fontSize:24 }}>{this.state.selectedBranch.branchName}</Text>
            <TouchableOpacity style={{ padding:10 }} 
            onPress={()=>{
              // this.setState({ isBottomSheet: !this.state.isBottomSheet })
              this.sheetRef.current.snapTo(1)
              console.log('onPress------------------')
            }}>
              <Image source={this.state.isBottomSheet ? images_path.arrow_up : images_path.arrow_down_black} />
            </TouchableOpacity>
          </View>
          <Text style={{ fontFamily:'Roboto-Medium', fontSize: 14, marginTop: 5 }}>{this.state.selectedBranch.managerName}</Text> 
          <Text style={{ fontFamily:'Roboto-Regular', fontSize: 14, color:MyColors.themeColor, marginTop:5 }}>Manager</Text> 
          <View style={{ flexDirection: 'row', marginTop: 5, alignItems: 'center' }}>
            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
              <Image source={images_path.phone} />
              <Text style={{ fontFamily:'Roboto-Regular', fontSize:14, marginStart: 10 }}>{this.state.selectedBranch.contactNumber}</Text> 
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ fontFamily:'Roboto-Bold', fontSize: 14, }}>{strings.pos_collon}</Text> 
              <Text style={{ fontFamily:'Roboto-Regular', fontSize: 14, marginStart: 10, color:MyColors.themeColor }}>Active</Text> 
            </View>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5, alignItems: 'center' }}>
            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
              <Image source={images_path.mail} />
              <Text style={{ fontFamily:'Roboto-Regular', fontSize:14, marginStart: 10 }}>{this.state.selectedBranch.customerCareEmail}</Text> 
            </View>
            <TouchableOpacity
              activeOpacity={0.7}
              style={{
                paddingHorizontal: 20, 
                marginStart: 10,
                paddingVertical: 10,
                borderRadius: 15,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: MyColors.themeColor,
          
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 5,
                },
                shadowOpacity: 0.36,
                shadowRadius: 6.68,
          
                elevation: 11, }}
              onPress={() => this.props.navigation.navigate("PosScreen")}>
              <Text style={{
                fontSize: 14,
                fontFamily: 'Roboto-Medium',
                color: MyColors.whiteColor,
              }} >{strings.more}</Text>
            </TouchableOpacity>
          </View>
        </View>
      }
    </View>
  );

}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    backgroundColor: MyColors.whiteColor,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25
  },

  // ---------------------------Top
  cellHeader: {
    margin: 25,
  },

  txtUserName: {
    fontSize: 18, 
    color: MyColors.whiteColor, 
    marginTop: 15, 
    fontFamily: 'Sansation-Bold'
  },

  txtCount: {
    fontSize: 22, 
    color: MyColors.themeColor, 
    fontFamily: 'Roboto-Medium'
  },
  txtCountTitle: {
    fontSize: 10, 
    fontFamily: 'Roboto-Regular'
  },

  filterContainer: {
    flex: 1, 
    flexDirection: 'row', 
    alignItems: 'center', 
    marginTop: 30,
    zIndex : 999
  },

  txtBranch: {
    flex: 1,
    color: MyColors.whiteColor, 
    fontFamily: 'Roboto-Medium', 
    marginEnd: 5, 
    fontSize: 12
  },

  menuContainer: {
    borderRadius: 10, 
    borderWidth: 1, 
    borderColor: MyColors.themeColor, 
    marginHorizontal: 5, 
    padding: 10, 
    flexDirection: 'row', 
    alignItems: 'center',
  },

  txtMenu: {
    flex: 1, 
    color: MyColors.themeColor, 
    fontFamily: 'Roboto-Regular', 
    marginEnd: 5, 
    fontSize: 11
  },

  menuListContainer: {
    position: 'absolute',
    backgroundColor: MyColors.whiteColor,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.00,
    elevation: 24,
  },

  txtMenuItem: {
    fontFamily: 'Roboto-Regular', 
    fontSize: 12, 
    padding: 10, 
    textAlign: 'center'
  },

  searchContainer: {
    borderRadius: 10, 
    backgroundColor: '#F1F1F4', 
    padding: 10, 
    marginStart: 5
  },

  txtTitle: {
    fontSize: 24, 
    marginTop: 20,
    fontFamily: 'Sansation-Bold',
  },

  itemSubContainer: {
    flex: 1, 
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'space-between'
  },

  txtUpdateResend: {
    color: MyColors.whiteColor, 
    fontFamily: 'Roboto-Medium', 
    fontSize: 14, 
    padding: 10
  },

  txtInvoiceNo: {
    fontFamily: 'Roboto-Medium',
    fontSize: 14,
  },
  vwSeparator: {
    flex: 1,
    height: 1,
    backgroundColor: MyColors.seperatorGreyColor,
    marginVertical: 15
  },
  vwRow: {
    flex: 1,
    flexDirection: 'row',
    overflow: 'hidden'
  },

  bottomSheetContainer: {
    padding: 20,
    height: '100%',
    marginTop: 20,
    backgroundColor: MyColors.whiteColor,
    borderTopEndRadius: 25,
    borderTopStartRadius: 25,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.00,
    elevation: 24,
  },



  panel: {
    flex: 1,
    position: 'relative',

    padding: 20,
    height: '100%',
    marginTop: 20,
    backgroundColor: MyColors.whiteColor,
    borderTopEndRadius: 25,
    borderTopStartRadius: 25,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.00,
    elevation: 24,
  },
  panelHeader: {
    // height: 120,
    
  },
  favoriteIcon: {
    position: 'absolute',
    top: -24,
    right: 24,
    backgroundColor: '#2b8a3e',
    width: 48,
    height: 48,
    padding: 8,
    borderRadius: 24,
    zIndex: 1
  }


});
