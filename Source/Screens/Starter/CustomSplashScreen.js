import React, { Component } from 'react';
import { View, StyleSheet, Image, SafeAreaView } from 'react-native';
import SplashScreen from 'react-native-splash-screen'

import { MyColors } from '../../Theme';
import images_path from '@Images/Images';
import Global from '../../Utility/Global';
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class CustomSplashScreen extends Component {
  constructor() {
    super()
    this.state = {
      isLoading: true,
      token: '',
      user: {},
      merchant: {},
    }
  }

  componentDidMount() {
    SplashScreen.hide()

    Global.getCurrentToken().then(token => {
      Global.getCurrentUserPromise().then(user => {
        Global.getCurrentMerchantPromise().then(merchant => {
          this.setState({
            isLoading: false,
            token: token,
            user: user,
            merchant: merchant,
          }, () => {
            console.log('token is >>', this.state.token)
            console.log('user is >>', this.state.user)
            console.log('merchant is >>', this.state.merchant)
            this.props.navigation.navigate('VerifyPinScreen', { mobile: this.state.user.mobile })
          })
        })
          .catch(error => {
            console.log('user error is >>> ', error)
            this.props.navigation.navigate('Auth')
          })
      })
        .catch(error => {
          console.log('user error is >>> ', error)
          this.props.navigation.navigate('Auth')
        })
    }).catch(error => {
      console.log('token error is >>> ', error)
      this.setState({ token: '', isLoading: false, })
      this.props.navigation.navigate('Auth')
    })
  }

  render() {
    return (
      <SafeAreaView style={Styles.viewSafeArea}>
        <CustomStatusBarTheme />
        <View style={Styles.viewContent}>
          {/* <Image
            style={{ flex: 1 }}
            resizeMode="cover"
            source={images_path.login_background}
          /> */}
          <Image
            style={{ position: 'absolute', }}
            source={images_path.name_logo}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const Styles = StyleSheet.create({
  viewSafeArea: {
    flex: 1,
    backgroundColor: MyColors.whiteColor
  },
  viewContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

});
