import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  SafeAreaView,
  Text,
  ImageBackground,
  BackHandler
} from 'react-native'
import CryptoJS from "react-native-crypto-js"
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { MyColors } from '../../Theme'
import Global from '../../Utility/Global'
import strings from '../../Localization/string';
import { isEmpty, showMessageAlert } from '../../Utility/Utility'
import { postMethodAPI } from '../../API/APIClient';

import CustomButton from '../../customComponents/CustomButton'
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class VerifyPinScreen extends Component {
  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  state = {
    isLoading: false,

    token: '',
    mobile: '',

    pin: '',
  }

  componentDidMount() {
    if (this.props.navigation.state != null && this.props.navigation.state.params != undefined) {
      this.setState({ mobile: this.props.navigation.state.params.mobile })
    }
    Global.getCurrentToken().then(token => {
      this.setState({ token: token })
    })

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  handleBackButtonClick() {
    BackHandler.exitApp()
    return true;
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, }}>
        <CustomStatusBarTheme />
          <ImageBackground source={images_path.login_background}
            style={{ width: '100%', height: '100%' }}>

            <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: MyColors.white }}>
              <View style={Styles.container}>

                <Image style={{ marginTop: 80 }} source={images_path.logo_bg} />
                <Text style={{ fontSize: 24, fontFamily: 'Sansation-Bold' }}>{strings.app_name}</Text>

                <Text style={Styles.txtVerifyTitle}>{strings.verify_pin}</Text>
                {this.renderPinField()}
                {this.renderButton()}
              </View>
            </KeyboardAwareScrollView>
          </ImageBackground>
      </SafeAreaView>
    )
  }

  renderPinField() {
    return (
      <OTPInputView
        style={{ width: '100%', height: 50, marginTop: 15 }}
        pinCount={6}
        code={this.state.pin} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
        onCodeChanged={pin => { this.setState({ pin }) }}
        autoFocusOnLoad={true}
        codeInputFieldStyle={Styles.inputItem}
        // codeInputHighlightStyle={{
        //   borderColor: "#03DAC6",
        // }}
        onCodeFilled={code => {
          if (code.length == 6) {
            this.tappedOnButton(code)
          }
          // console.log(`Code is ${code}, you are good to go!`)
        }}
      />
    )
  }

  renderButton() {
    return (
      <CustomButton
        title={strings.verify}
        isLoading={this.state.isLoading}
        buttonCustomStyle={{ marginTop: 30, paddingHorizontal: 20 }}
        onPress={() => this.tappedOnButton(this.state.pin)}
      />
    )
  }

  tappedOnButton(code) {
    if (this.state.isLoading) {
      return
    }

    if (isEmpty(this.state.pin)) {
      showMessageAlert(strings.please_enter_pin)
      return
    }

    let ciphertext = CryptoJS.AES.encrypt(code, 'GREENBILL').toString();

    this.setState({ isLoading: true })

    var params = JSON.stringify({
      pin: ciphertext,
      userType: '2',
    });

    const apifetcherObj = postMethodAPI('register/verifyPin/' + this.state.mobile, params, this.state.token);
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log("Response ---->>>>\n ", data);

        this.setState({ isLoading: false })

        if (statusCode == 200 || statusCode == 201) {
          if (data.accessToken != null && data.accessToken != undefined) {
            Global.saveCurrentToken(data.accessToken);
          }

          if (data.user != null && data.user != undefined) {
            Global.saveCurrentUser(data.user)
          }

          if (data.merchant != null && data.merchant != undefined) {
            Global.saveCurrentMerchant(data.merchant)
          }

          console.log('data.isFirstTime----------------------', data.isFirstTime)

          if (data.isFirstTime) {
            this.props.navigation.navigate('CompleteProfileScreen', { isEdit: false, data: data.user })
          } else {
            this.props.navigation.navigate('Dashboard')
          }
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
        }
      })
      .catch(error => {
        console.log("Error ---->>>> \n", error);
        this.setState({ isLoading: false });
        showMessageAlert(error)
      });
    return
  }

}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
  },
  txtVerifyTitle: {
    fontSize: 14,
    marginTop: 50,
    fontFamily: 'Roboto-Regular'
  },
  inputItem: {
    backgroundColor: MyColors.whiteColor,
    borderRadius: 10,
    color: MyColors.blackColor,
    width: (Dimensions.get('window').width - 40) / 6 - 10
  }
})
