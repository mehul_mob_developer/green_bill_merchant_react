import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  SafeAreaView,
  Text,
} from 'react-native'
import CryptoJS from "react-native-crypto-js"
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { MyColors } from '../../Theme'
import Global from '../../Utility/Global'
import strings from '../../Localization/string';
import { isEmpty, showMessageAlert } from '../../Utility/Utility'
import { putMethodAPI } from '../../API/APIClient';

import CustomButton from '../../customComponents/CustomButton'
import CustomStatusBarTheme from '../../customComponents/CustomStatusBarTheme'


export default class ChangePinScreen extends Component {
  constructor(props) {
    super(props)
  }

  state = {
    isLoading: false,

    token: '',
    user: {},
    merchant: {},

    pin: '',
    confirmPin: '',
  }

  componentDidMount() {
    Global.getCurrentToken().then(token => {
      Global.getCurrentUserPromise().then(user => {
        Global.getCurrentMerchantPromise().then(merchant => {
          this.setState({
            token: token,
            user: user,
            merchant: merchant,
          }, () => {
            console.log('user----------->>>>>>>', this.state.user)
            console.log('merchant------->>>>>>>', this.state.merchant)
          })
        })
      })
    })
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, }}>
          <CustomStatusBarTheme />

          <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: MyColors.white }}>
            <View style={Styles.container}>

              <Image source={images_path.logo_bg} style={{ alignSelf: 'center' }} />

              <Text style={Styles.txtVerifyTitle}>{strings.enter_pin}</Text>

              {this.renderPinField()}

              <Text style={Styles.txtVerifyTitle}>{strings.enter_confirm_pin}</Text>

              {this.renderConfirmPinField()}

              {this.renderButton()}
            </View>
          </KeyboardAwareScrollView>

      </SafeAreaView>
    )
  }

  renderPinField() {
    return (
      <OTPInputView
        style={{ width: '100%', height: 50, marginTop: 15 }}
        pinCount={6}
        code={this.state.pin} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
        onCodeChanged={pin => { this.setState({ pin }) }}
        autoFocusOnLoad={true}
        codeInputFieldStyle={Styles.inputItem}
        // codeInputHighlightStyle={{
        //   borderColor: "#03DAC6",
        // }}
        onCodeFilled={code => {
          // console.log(`Code is ${code}, you are good to go!`)
        }}
      />
    )
  }

  renderConfirmPinField() {
    return (
      <OTPInputView
        style={{ width: '100%', height: 50, marginTop: 15 }}
        pinCount={6}
        code={this.state.confirmPin} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
        onCodeChanged={confirmPin => { this.setState({ confirmPin }) }}
        autoFocusOnLoad={true}
        codeInputFieldStyle={Styles.inputItem}
        // codeInputHighlightStyle={{
        //   borderColor: "#03DAC6",
        // }}
        onCodeFilled={code => {
          // console.log(`Code is ${code}, you are good to go!`)
        }}
      />
    )
  }

  renderButton() {
    return (
      <CustomButton
        title={strings.verify}
        isLoading={this.state.isLoading}
        buttonCustomStyle={{ marginTop: 30, paddingHorizontal: 20 }}
        onPress={() => this.tappedOnButton()}
      />
    )
  }

  tappedOnButton() {
    if (this.state.isLoading) {
      return
    }

    if (isEmpty(this.state.pin)) {
      showMessageAlert(strings.please_enter_pin)
      return
    } else if (isEmpty(this.state.confirmPin)) {
      showMessageAlert(strings.please_enter_confirm_pin)
      return
    } else if (this.state.pin != this.state.confirmPin) {
      showMessageAlert(strings.pin_does_not_match)
      return
    }

    let ciphertext = CryptoJS.AES.encrypt(this.state.confirmPin, 'GREENBILL').toString();

    this.setState({ isLoading: true })

    var params = JSON.stringify({
      pin: ciphertext,
    });

    const apifetcherObj = putMethodAPI('register/updateMerchantPin/' + this.state.user.mobile, params, this.state.token);
    apifetcherObj
      .then(response => { return Promise.all([response.status, response.json()]) })
      .then(res => {
        let statusCode = res[0]
        let data = res[1]
        console.log("Response ---->>>>\n ", data);

        this.setState({ isLoading: false })

        if (statusCode == 200) {
          this.props.navigation.goBack()
        }

        if (data.error != undefined) {
          showMessageAlert(data.error)
          this.setState({ isLoading: false })
        }
      })
      .catch(error => {
        console.log("Error ---->>>> \n", error);
        this.setState({ isLoading: false })
        showMessageAlert(error)
      });
    return
  }

}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  txtVerifyTitle: {
    fontSize: 14,
    marginTop: 20,
    fontFamily: 'Roboto-Regular'
  },
  inputItem: {
    backgroundColor: MyColors.whiteColor,
    borderRadius: 10,
    color: MyColors.blackColor,
    width: (Dimensions.get('window').width - 40) / 6 - 10
  }
})
