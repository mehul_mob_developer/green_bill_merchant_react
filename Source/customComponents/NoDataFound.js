import React from "react";
import {
  View,
  Dimensions,
  StyleSheet, Image
} from "react-native";
import images_path from '@Images/Images';
import { BoldText } from "./CustomTexts";

export default class NoDataFound extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    var {message,textColor = '#324755'}= this.props
    return (
      <View
        style={Styles.vwMainContainer}
      >
        <Image source={images_path.no_data_found}/>
        <BoldText text={message} size={15} color={textColor} style={Styles.img}/>
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  vwMainContainer: {
    flex:1,
    justifyContent:'center',
    alignItems:'center',
  },
  img:{
      marginTop:10
  }
});
