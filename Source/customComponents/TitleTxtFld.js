import React, { Component } from 'react'
import { View, Text, TextInput, StyleSheet, TouchableOpacity, Image } from 'react-native'
import { MyColors } from '../Theme'

export default class TitleTxtFld extends Component {
  render() {
    const {
      placeholder,
      value,
      title,
      onChangeTxt,
      isSecured = false,
      isSelectable = false,
      onPress,
      autoCapitalize = false,
      keyboardType = 'default',
      multiline = false,
      rightImageSource } = this.props

    return (
      <TouchableOpacity activeOpacity={1} onPress={onPress}>
        <View style={Styles.container}>
          <Text style={Styles.txtLabel}>{title}</Text>
          <View style={{ flex: 1, flexDirection: 'row', marginTop: 5 }}>
            <TextInput
              style={Styles.txtInpt}
              placeholder={placeholder}
              editable={isSelectable ? false : true}
              value={value}
              onChangeText={onChangeTxt}
              autoCapitalize={autoCapitalize ? "none" : "words"}
              secureTextEntry={isSecured}
              pointerEvents={isSelectable ? "none" : "auto"}
              keyboardType={keyboardType}
              multiline={multiline}
            />
            {rightImageSource != undefined || rightImageSource != null ?
              <Image style={Styles.rightIamge} source={rightImageSource} /> : null}
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const Styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  txtLabel: {
    fontFamily: 'Roboto-Regular',
    fontSize: 14,
    color: '#5A607B',
  },
  txtInpt: {
    flex: 1,
    fontFamily: 'Roboto-Regular',
    fontSize: 14,
    padding: 10,
    color: MyColors.darkTitleColor,
    backgroundColor: '#F1F1F4',
    borderRadius: 10,
  },
  rightIamge: {
    resizeMode: 'center',
    position: 'absolute',
    right: 15,
    alignSelf: 'center',
  },
})
