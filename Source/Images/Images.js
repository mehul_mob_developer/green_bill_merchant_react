export default images_path = {

  india: require('./india.png'),
  login_background: require('./login_background.png'),
  verify_otp_background: require('./verify_otp_background.png'),
  top_bg: require('./top_bg.png'),
  arrow_down: require('./arrow_down.png'),
  camera: require('./camera.png'),
  refresh: require('./refresh.png'),

  close: require('./close.png'),
  nilgiri: require('./nilgiri.png'),
  menu: require('./menu.png'),
  dashboard: require('./dashboard.png'),
  branches: require('./branches.png'),
  invoice: require('./invoice.png'),
  subscription: require('./subscription.png'),
  pos_request: require('./pos_request.png'),
  promotions_offers: require('./promotions_offers.png'),
  menu_footer: require('./menu_footer.png'),
  ticket_yellow: require('./ticket_yellow.png'),
  ticket_grey: require('./ticket_grey.png'),

  back: require('./back.png'),
  back_white: require('./back_white.png'),
  bill: require('./bill.png'),
  wallet: require('./wallet.png'),
  search: require('./search.png'),
  tree: require('./tree.png'),
  calendar: require('./calendar.png'),
  arrow_down_white: require('./arrow_down_white.png'),
  arrow_down_green: require('./arrow_down_green.png'),
  camera_green: require('./camera_green.png'),
  
  logo_bg: require('./logo_bg.png'),
  pos_placeholder: require('./pos_placeholder.png'),
  lock: require('./lock.png'),
  subscription_placeholder: require('./subscription_placeholder.png'),
  gift: require('./gift.png'),
  user_plus: require('./user_plus.png'),
  promotions_placeholder: require('./promotions_placeholder.png'),
  arrow_up: require('./arrow_up.png'),
  mail: require('./mail.png'),
  phone: require('./phone.png'),
  arrow_down_black: require('./arrow_down_black.png'),

  log_out: require('./log_out.png'),
  name_logo: require('./name_logo.png'),

  branch_placeholder: require('./branch_placeholder.png'),
  

};
