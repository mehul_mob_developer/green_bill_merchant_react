const MyColors = {
  
  themeColor: '#19CB3F',
  whiteColor: "#FFFFFF",
  
  seperatorGreyColor: 'rgba(151,151,151,0.18)',
  blackColor: "#000000",

  vwBorder: "#C3D3D4",

};

export { MyColors };

