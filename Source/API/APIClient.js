// import { hex_md5 } from "react-native-md5";
import { Platform, NetInfo } from "react-native";
export const BASE_URL = "https://backend.digiliving.in/api/v1/";

export const APIConstants = {
  AuthenticationType: {
    BASIC: "Basic",
    BEARER: "Bearer",
    TOKEN: "Token"
  },
  ContentType: {
    JSON: "application/json",
    URLEncoded: "application/x-www-form-urlencoded"
  },
  HTTPMethod: {
    GET: "GET",
    DELETE: "DELETE",
    POST: "POST",
    PUT: "PUT"
  },
  StatusCode: {
    SUCCESS: 200,
    REDIRECTION: 300,
    CLIENT_ERROR: 400,
    SERVER_ERROR: 500
  }
};

export const postMethodAPI = (url, params, token) => {
  var final_url = `${BASE_URL}${url}`;
  const config = {
    method: `${APIConstants.HTTPMethod.POST}`,
    headers: {
      "Accept": APIConstants.ContentType.JSON,
      "Content-Type": APIConstants.ContentType.JSON,
      "app_version": 1.0,
      "app_locale": "en_US",
      "platform": Platform.OS,
      "Authorization": token == undefined ? "" : ("Bearer " + token),
    },
    body: params
  };
  console.log("config ", config, final_url);
  return fetch(final_url, config);
};

export const postMethodUploadImageAPI = (url, params, token) => {
  var final_url = `${BASE_URL}${url}`;
  console.log(final_url);
  const config = {
    method: `${APIConstants.HTTPMethod.POST}`,
    headers: {
      Accept: APIConstants.ContentType.JSON,
      "Content-Type": "multipart/form-data",
      "app_version": 1.0,
      "app_locale": "en_US",
      "platform": Platform.OS,
      "Authorization": token == undefined ? "" : ("Bearer " + token),
    },
    body: params
  };
  console.log("config ", config);
  return fetch(final_url, config);
};

export const getMethodAPI = (url, params, token) => {
  var final_url = `${BASE_URL}${url}`;

  console.log(final_url);
  const config = {
    method: `${APIConstants.HTTPMethod.GET}`,
    headers: {
      Accept: APIConstants.ContentType.JSON,
      "Content-Type": APIConstants.ContentType.JSON,
      "app_version": 1.0,
      "app_locale": "en_US",
      "platform": Platform.OS,
      "Authorization": token == undefined ? "" : ("Bearer " + token),
    },
  };
  console.log("config ", config);
  return fetch(final_url, config);
};

export const putMethodAPI = (url, params, token) => {
  var final_url = `${BASE_URL}${url}`;
  const config = {
    method: `${APIConstants.HTTPMethod.PUT}`,
    headers: {
      "Accept": APIConstants.ContentType.JSON,
      "Content-Type": APIConstants.ContentType.JSON,
      "app_version": 1.0,
      "app_locale": "en_US",
      "platform": Platform.OS,
      "Authorization": token == undefined ? "" : ("Bearer " + token),
    },
    body: params
  };
  console.log("config ", config, final_url);
  return fetch(final_url, config);
};

export const deleteMethodAPI = (url, params, token) => {
  var final_url = `${BASE_URL}${url}`;
  const config = {
    method: `${APIConstants.HTTPMethod.DELETE}`,
    headers: {
      "Accept": APIConstants.ContentType.JSON,
      "Content-Type": APIConstants.ContentType.JSON,
      "app_version": 1.0,
      "app_locale": "en_US",
      "platform": Platform.OS,
      "Authorization": token == undefined ? "" : ("Bearer " + token),
    },
    body: params
  };
  console.log("config ", config, final_url);
  return fetch(final_url, config);
};

export const isInternetAvailable = async () => {
  return new Promise(function (resolve, reject) {
    NetInfo.isConnected.fetch().then(isConnected => {
      resolve(isConnected)
    })
      .catch((error) => {
        reject(error)
      })
  })
}