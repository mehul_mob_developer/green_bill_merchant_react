import React from "react";
import { Alert, Dimensions } from "react-native";
//import { APP_NAME } from "..";
import moment from "moment"; // for date format
import Toast from 'react-native-simple-toast';

const APP_NAME = "DigiBill"

export const showMessageAlert = message => {

  Toast.showWithGravity(message, Toast.SHORT, Toast.BOTTOM)

  // Alert.alert(
  //   APP_NAME,
  //   message,
  //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
  //   { cancelable: false }
  // );
};

export const showNativeMessageAlert = message => {

  Alert.alert(
    APP_NAME,
    message,
    [{ text: "Okay", onPress: () => console.log("OK Pressed") }],
    { cancelable: false }
  );
};

export const convertUTCDateToLocalDate = dateString => {
  console.log("dateString is >>> ",dateString);
  
  var localDate = new Date(dateString);
  return localDate 
};

export const parseUTCDate = dateString => {
  console.log("dateString is >>>> ",dateString);
  
  const dateParams = dateString.replace(/ UTC/, "").split(/[\s-:]/);
  dateParams[1] = (parseInt(dateParams[1], 10) - 1).toString();

  return new Date(Date.UTC(...dateParams));
};

export const printOnConsole = text => {
  console.log(text);
}

export const showServerMessageAlert = response => {
  // console.log("showServerMessageAlert ", response);
  // const json = JSON.parse(response);
  // console.log("showServerMessageAlert json", json);

  Alert.alert(
    APP_NAME,
    response,
    [{ text: "OK", onPress: () => console.log("OK Pressed") }],
    { cancelable: false }
  );
};
export const isEmpty = value => {
  if (value == null) {
    return true
  }
  value = value.trim()
  if (value == null || value == undefined) {
    return true;
  } else if (value == "") {
    return true;
  }
  return false;
};

export const isValidEmail = value => {
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (reg.test(value) === false) {
    //console.log("Email is Not Correct");
    return false;
  } else {
    //console.log("Email is Correct");
    return true;
  }
};

export const getHexStringFromTimestamp = () => {
  var timestamp = new Date().getTime()
  hexString = number.toString(16);
  return hexString;
};

export const getMonthYearfromData = (month , day , title) => {
  switch (month) {
    case 1 :
    return "Jan " + day + ", " + title
    case 2 :
    return "Feb " + day + ", "  +  title
    case 3 :
    return "Mar " + day + ", "  + title
    case 4 :
    return "Apr " + day + ", "  + title
    case 5 :
    return "May " + day + ", "  + title
    case 6 :
    return "Jun " + day + ", "  + title
    case 7 :
    return "Jul " + day + ", "  + title
    case 8 :
    return "Aug " + day + ", "  + title
    case 9 :
    return "Sep " + day + ", "  + title
    case 10 :
    return "Oct " + day + ", "  + title
    case 11 :
    return "Nov " + day + ", "  + title
    case 12 :
    return "Dec " + day + ", "  + title
  }
}

export const getOnlyMonthAndYearFromData = (month , title) => {
  switch (month) {
    case 1 :
    return "Jan " + ", " + title
    case 2 :
    return "Feb " + ", "  +  title
    case 3 :
    return "Mar " + ", "  + title
    case 4 :
    return "Apr " + ", "  + title
    case 5 :
    return "May " + ", "  + title
    case 6 :
    return "Jun " + ", "  + title
    case 7 :
    return "Jul " + ", "  + title
    case 8 :
    return "Aug " + ", "  + title
    case 9 :
    return "Sep " + ", "  + title
    case 10 :
    return "Oct " + ", "  + title
    case 11 :
    return "Nov " + ", "  + title
    case 12 :
    return "Dec " + ", "  + title
  }
}



export const timeSince = (date) => {

  var seconds = Math.floor((new Date() - date) / 1000);

  var interval = Math.floor(seconds / 31536000);

  if (interval > 1) {
    return interval + " years";
  }
  interval = Math.floor(seconds / 2592000);
  if (interval > 1) {
    return interval + " months";
  }
  interval = Math.floor(seconds / 86400);
  if (interval > 1) {
    return interval + " days";
  }
  interval = Math.floor(seconds / 3600);
  if (interval > 1) {
    return interval + " hours";
  }
  interval = Math.floor(seconds / 60);
  if (interval > 1) {
    return interval + " minutes";
  }
  return Math.floor(seconds) + " seconds";
}

export const getOperationModeFromCode = (value) => {
  if (value == 0) {
    return "OFF";
  }else if (value == 1) {
    return "Stand by(Low Power Consumption)";
  }else if (value == 2) {
    return "ON (Battery Saving Mode)";
  }
  else if (value == 3) {
    return "ON (Normal Operating State)";
  }else {
    return "";
  }
}

export const getRunLoopMode = (value) => {
  if (value == "OFF") {
    return "Off";
  }else if (value == "STAND_BY") {
    return "Stand by";
  }else if (value == "RESERVED") {
    return "Reserved";
  }
  else if (value == "ON") {
    return "On";
  }else {
    return "";
  }
}

export const formatDate = (date, format) => {
  console.log("format - ", format);
  return moment(date).format(format);
};

export const isiPhoneX = () => {
  const height = Dimensions.get('window').height
  const width = Dimensions.get('window').width
  if (Platform.OS === 'android') {
    return false
  } else {

    // iPhone X height/width is 812
    // iPhone XR, XS, & XS Max height/width is 896

    if (height == 812 || width == 812 || height == 896 || width == 896) {
      return true
    } else {
      return false
    }
  }
}