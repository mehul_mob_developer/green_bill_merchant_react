import 'react-native-gesture-handler'
import * as React from 'react'
import { LogBox, } from 'react-native'


import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import { createSwitchNavigator } from '@react-navigation/compat'
import { createCompatNavigatorFactory } from '@react-navigation/compat'


// Starter
import CustomSplashScreen from "./Source/Screens/Starter/CustomSplashScreen";
import VerifyPinScreen from "./Source/Screens/Starter/VerifyPinScreen";

// Auth
import MobileScreen from "./Source/Screens/Auth/MobileScreen";
import OtpScreen from "./Source/Screens/Auth/OtpScreen";
import CompleteProfileScreen from "./Source/Screens/Auth/CompleteProfileScreen";

// Dashboard
import DrawerNavigator from "./Source/Screens/Dashboard/DrawerNavigator";

//Home
import HomeScreen from "./Source/Screens/Home/HomeScreen";
import SearchInvoiceScreen from "./Source/Screens/Home/SearchInvoiceScreen";

//Branch
import BranchScreen from "./Source/Screens/Branch/BranchScreen";
import AddGeneralInfoScreen from "./Source/Screens/Branch/AddGeneralInfoScreen";
import AddTaxInfoScreen from "./Source/Screens/Branch/AddTaxInfoScreen";
import AddContactInfoScreen from "./Source/Screens/Branch/AddContactInfoScreen";
import AddBranchAddressScreen from "./Source/Screens/Branch/AddBranchAddressScreen";
import SearchBranchScreen from "./Source/Screens/Branch/SearchBranchScreen";

//Subscriptions
import SubscriptionsScreen from "./Source/Screens/Subscriptions/SubscriptionsScreen";

//POS
import PosScreen from "./Source/Screens/Pos/PosScreen";
import AddPosScreen from "./Source/Screens/Pos/AddPosScreen";

//Promotions
import PromotionsScreen from "./Source/Screens/Promotions/PromotionsScreen";
import AddPromotionScreen from "./Source/Screens/Promotions/AddPromotionScreen";
import SearchPromotionScreen from "./Source/Screens/Promotions/SearchPromotionScreen";

// Select
import CategoryListScreen from "./Source/Screens/Select/CategoryListScreen";
import SubCategoryListScreen from "./Source/Screens/Select/SubCategoryListScreen";
import CountryListScreen from "./Source/Screens/Select/CountryListScreen";
import RegionListScreen from "./Source/Screens/Select/RegionListScreen";
import StateListScreen from "./Source/Screens/Select/StateListScreen";
import CityListScreen from "./Source/Screens/Select/CityListScreen";
import SelectBranchListScreen from "./Source/Screens/Select/SelectBranchListScreen";
import OwnershipListScreen from "./Source/Screens/Select/OwnershipListScreen";

import ChangePinScreen from "./Source/Screens/Starter/ChangePinScreen";

// Other
import WebViewScreen from "./Source/Screens/Other/WebViewScreen";


// import Global from './Source/Utility/Global'

import MainApp from "./MainApp";
// import { MyColors } from './Source/Theme'
import strings from "./Source/Localization/string";
// import { isiPhoneX } from './Source/Utility/Utility'


// const StackMain = createStackNavigator()
// const StackNavigatorHome = createStackNavigator()


const StarterStack = createCompatNavigatorFactory(createStackNavigator)({
  CustomSplashScreen: {
    screen: CustomSplashScreen,
    navigationOptions: { headerShown: false, allowFontScaling: false }
  },
  VerifyPinScreen: {
    screen: VerifyPinScreen,
    navigationOptions: { headerShown: false, allowFontScaling: false }
  },
})

const AuthStack = createCompatNavigatorFactory(createStackNavigator)({
  MobileScreen: {
    screen: MobileScreen,
    navigationOptions: { headerShown: false, allowFontScaling: false }
  },
  OtpScreen: {
    screen: OtpScreen,
    navigationOptions: { headerShown: false, allowFontScaling: false }
  },
  CompleteProfileScreen: {
    screen: CompleteProfileScreen,
    navigationOptions: { headerShown: false, allowFontScaling: false },
    params: { isEdit: false }
  },
  VerifyPinScreen: {
    screen: VerifyPinScreen,
    navigationOptions: { headerShown: false, allowFontScaling: false }
  },
  CategoryListScreen: {
    screen: CategoryListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_business_category)
  },
  SubCategoryListScreen: {
    screen: SubCategoryListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_business_sub_category)
  },
  CountryListScreen: {
    screen: CountryListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_country)
  },
  RegionListScreen: {
    screen: RegionListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_region)
  },
  StateListScreen: {
    screen: StateListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_state)
  },
  CityListScreen: {
    screen: CityListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_city)
  },
})

const DashboardStack = createCompatNavigatorFactory(createStackNavigator)({
  DrawerNavigator: {
    screen: DrawerNavigator,
    navigationOptions: { headerShown: false, allowFontScaling: false }
  },
  CompleteProfileScreen: {
    screen: CompleteProfileScreen,
    navigationOptions: { headerShown: false, allowFontScaling: false },
    params: { isEdit: true }
  },
  CategoryListScreen: {
    screen: CategoryListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_business_category)
  },
  SubCategoryListScreen: {
    screen: SubCategoryListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_business_sub_category)
  },
  CountryListScreen: {
    screen: CountryListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_country)
  },
  RegionListScreen: {
    screen: RegionListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_region)
  },
  StateListScreen: {
    screen: StateListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_state)
  },
  CityListScreen: {
    screen: CityListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_city)
  },
  ChangePinScreen: {
    screen: ChangePinScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.change_pin)
  },
})

const HomeStack = createCompatNavigatorFactory(createStackNavigator)({
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: { headerShown: false, allowFontScaling: false },
    params: { isDashboard: true }
  },
  SelectBranchListScreen: {
    screen: SelectBranchListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_branch)
  },
  SearchInvoiceScreen: {
    screen: SearchInvoiceScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.search_invoices)
  },
})

const BranchStack = createCompatNavigatorFactory(createStackNavigator)({
  BranchScreen: {
    screen: BranchScreen,
    navigationOptions: { headerShown: false, allowFontScaling: false }
  },
  AddGeneralInfoScreen: {
    screen: AddGeneralInfoScreen,
    // navigationOptions: { headerShown: false, allowFontScaling: false }
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.general_information)
  },
  AddTaxInfoScreen: {
    screen: AddTaxInfoScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.tax_information)
  },
  AddContactInfoScreen: {
    screen: AddContactInfoScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.contact_informations)
  },
  AddBranchAddressScreen: {
    screen: AddBranchAddressScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.branch_address)
  },
  OwnershipListScreen: {
    screen: OwnershipListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_ownership_type)
  },
  SearchBranchScreen: {
    screen: SearchBranchScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.search_branches)
  },
  CountryListScreen: {
    screen: CountryListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_country)
  },
  RegionListScreen: {
    screen: RegionListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_region)
  },
  StateListScreen: {
    screen: StateListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_state)
  },
  CityListScreen: {
    screen: CityListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_city)
  },
})

const InvoiceStack = createCompatNavigatorFactory(createStackNavigator)({
  InvoiceScreen: {
    screen: HomeScreen,
    navigationOptions: { headerShown: false, allowFontScaling: false },
    params: { isDashboard: false }
  },
  SelectBranchListScreen: {
    screen: SelectBranchListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_branch)
  },
  SearchInvoiceScreen: {
    screen: SearchInvoiceScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.search_invoices)
  },
})

const SubscriptionsStack = createCompatNavigatorFactory(createStackNavigator)({
  SubscriptionsScreen: {
    screen: SubscriptionsScreen,
    navigationOptions: { headerShown: false, allowFontScaling: false }
  },
  WebViewScreen: {
    screen: WebViewScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.app_name)
  },
})

const PosStack = createCompatNavigatorFactory(createStackNavigator)({
  PosScreen: {
    screen: PosScreen,
    navigationOptions: { headerShown: false, allowFontScaling: false }
  },
  AddPosScreen: {
    screen: AddPosScreen,
    navigationOptions: { headerShown: false, allowFontScaling: false }
  },
  SelectBranchListScreen: {
    screen: SelectBranchListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_branch)
  },
})

const PromotionsStack = createCompatNavigatorFactory(createStackNavigator)({
  PromotionsScreen: {
    screen: PromotionsScreen,
    navigationOptions: { headerShown: false, allowFontScaling: false }
  },
  AddPromotionScreen: {
    screen: AddPromotionScreen,
    navigationOptions: { headerShown: false, allowFontScaling: false }
  },
  SelectBranchListScreen: {
    screen: SelectBranchListScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.select_branch)
  },
  SearchPromotionScreen: {
    screen: SearchPromotionScreen,
    navigationOptions: MainApp.customizeNavigationBarWithWhiteColor(strings.search_promotions)
  },
})


const SwitchNavigator = createSwitchNavigator(
  {
    Starter: StarterStack,
    Auth: AuthStack,
    Dashboard: DashboardStack,
    Home: HomeStack,
    Branches: BranchStack,
    Invoice: InvoiceStack,
    Subscriptions: SubscriptionsStack,
    Pos: PosStack,
    Promotions: PromotionsStack,
  },
  {
    initialRouteName: 'Starter',
  }
)

export {
  HomeStack,
  BranchStack,
  InvoiceStack,
  SubscriptionsStack,
  PosStack,
  PromotionsStack,
};

export default function MainAppWithSwitchNavigator() {
  LogBox.ignoreAllLogs()
  return (
    <NavigationContainer>
      <SwitchNavigator />
    </NavigationContainer>
  )
}
