import 'react-native-gesture-handler'
import React from 'react';
import MainAppWithSwitchNavigator from "./MainAppWithSwitchNavigator"

const App = () => {
  return (
    <MainAppWithSwitchNavigator />
  );
};

export default App;
